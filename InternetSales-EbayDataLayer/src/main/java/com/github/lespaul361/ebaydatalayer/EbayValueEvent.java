/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.ebaydatalayer;

import java.util.EventObject;

/**
 * 
 * @author Charles Hamilton
 */
public class EbayValueEvent extends EventObject {

    public final static int RETURN_TYPE_SESSION_ID = 0;
    public final static int RETURN_TYPE_TOKEN = 1;
    public final static int RETURN_TYPE_ACCOUNT_NAME = 2;
    public final static int RETURN_TYPE_ACCOUNT_ADDED = 3;
    public final static int RETURN_TYPE_CANCEL = 4;

    private final String ret;
    private final Object apiContext;
    private final int retType;

    public EbayValueEvent(String ret, Object apiContext, int returnType, Object source) {
	super(source);
	if (returnType > 2 || returnType < 0) {
	    RuntimeException re = new IllegalArgumentException("Invalid returnType argument");
	    throw re;
	}
	this.ret = ret;
	this.apiContext = apiContext;
	this.retType = returnType;
    }

    /**
     * @return the ret
     */
    public String getValue() {
	return ret;
    }

    /**
     * @return the apiContext
     */
    public Object getApiContext() {
	return apiContext;
    }

    /**
     * @return the retType
     */
    public int getReturnType() {
	return retType;
    }

}
