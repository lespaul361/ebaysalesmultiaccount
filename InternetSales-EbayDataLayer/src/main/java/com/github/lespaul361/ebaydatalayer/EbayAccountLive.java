/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.ebaydatalayer;

import com.ebay.sdk.ApiAccount;
import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.ApiException;
import com.ebay.sdk.ApiLogging;
import com.ebay.sdk.SdkException;
import com.ebay.sdk.call.GetOrdersCall;
import com.ebay.sdk.call.GetTokenStatusCall;
import com.ebay.sdk.call.GetUserCall;
import com.ebay.soap.eBLBaseComponents.DetailLevelCodeType;
import com.ebay.soap.eBLBaseComponents.OrderType;
import com.ebay.soap.eBLBaseComponents.PaginationType;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import com.ebay.soap.eBLBaseComponents.TokenStatusType;
import com.ebay.soap.eBLBaseComponents.UserType;
import com.github.lespaul361.internetsales.configurationfile.AbstractLogError;
import java.io.IOException;
import com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo;
import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 */
public class EbayAccountLive extends AbstractLogError {

    private final static String SOAP_URL = "https://api.ebay.com/wsapi";
    private final static String SANDBOX_URL = "https://api.sandbox.ebay.com/wsapi";
    private ApiContext apiContext = null;
    private final EbayApplicationInfo appInfo;
    private final EbayAccount ebayAccount;
    private final Image logo;

    public EbayAccountLive(EbayApplicationInfo appInfo, EbayAccount ebayAccount,
            Logger logger, Image logo) {
        super(logger);
        this.appInfo = appInfo;
        this.ebayAccount = ebayAccount;
        this.logo = logo;
    }

    ApiContext getApiContext() throws IOException, Exception {
        if (this.apiContext == null) {
            ApiContext apiContext = new ApiContext();
            // set Api Token to access EbayApi Server
            ApiCredential cred = apiContext.getApiCredential();
            ApiAccount account = new ApiAccount();
            account.setApplication(appInfo.getAppId());
            account.setCertificate(appInfo.getCertificateId());
            account.setDeveloper(appInfo.getDeveloperName());
            cred.setApiAccount(account);
            cred.seteBayToken(ebayAccount.getSecurityToken());
            apiContext.setSite(SiteCodeType.US);
            // set Api Server Url
            apiContext.setApiServerUrl(SOAP_URL);
            this.apiContext = apiContext;
        }
        return this.apiContext;
    }

    public boolean isTokenExpiring() throws Exception {
        try {

            ApiContext apiContext = getApiContext();
            GetTokenStatusCall call = new GetTokenStatusCall(apiContext);

            TokenStatusType status = call.getTokenStatus();
            Calendar expCalendar = status.getExpirationTime();
            Calendar cur = Calendar.getInstance();
            cur.add(Calendar.DATE, -7);
            if (expCalendar.getTimeInMillis() < cur.getTimeInMillis()) {
                return true;
            }

            return false;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw e;
        }

    }

    public List<OrderType> getOrders(Calendar from) throws ApiException, SdkException, Exception {
        Calendar creationDate = Calendar.getInstance();
        creationDate.setTime(from.getTime());
        OrderType[] orders = getOrders(from, Calendar.getInstance());
        return new ArrayList<OrderType>(Arrays.asList(orders));
    }

    public OrderType[] getOrders(Calendar from, Calendar to) throws ApiException, SdkException, Exception {
        ApiContext apiContext = getApiContext();
        ApiLogging apiLogging = new ApiLogging();
        apiContext.setApiLogging(apiLogging);
        // Set the site based on the seller - for UK sellers, set it to SiteCodeType.UK
        // for US, set to SiteCodeType.US
        apiContext.setSite(SiteCodeType.US);

        // set detail level to ReturnAll
        DetailLevelCodeType[] detailLevels = new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_ALL};

        GetOrdersCall getOrders = new GetOrdersCall(apiContext);
        getOrders.setDetailLevel(detailLevels);
        getOrders.setCreateTimeFrom(from);
        getOrders.setCreateTimeTo(to);
        getOrders.setIncludeFinalValueFee(true);

        PaginationType page = new PaginationType();
        page.setEntriesPerPage(100);
        int pageIndex = 1;
        page.setPageNumber(pageIndex);
        getOrders.setPagination(page);
        List<OrderType> returnedOrders = new ArrayList<OrderType>();
        while (getOrders.getReturnedHasMoreOrders() == null || getOrders.getReturnedHasMoreOrders()) {
            OrderType[] ordersOrderTypes = getOrders.getOrders();
            returnedOrders.addAll(Arrays.asList(ordersOrderTypes));
            pageIndex++;
            page.setPageNumber(pageIndex);
        }
        return returnedOrders.toArray(new OrderType[returnedOrders.size()]);
    }

    public String getNewSecurityToken() throws ApiException, SdkException, Exception {
        return null;
    }

    public static String getUserID(ApiContext apiContext, String token) throws
            ApiException, SdkException, Exception {

        ApiCredential credential = apiContext.getApiCredential();
        credential.seteBayToken(token);
        GetUserCall getUserCall = new GetUserCall(apiContext);
        getUserCall.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_ALL});
        UserType ut = getUserCall.getUser();
        String ret = ut.getUserID();

        return ret;
    }
}
