/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.ebaydatalayer.Exceptions;

/**
 * 
 * @author Charles Hamilton
 */
public class EbayConfigurationException extends Exception {

    public static final int ERROR_APPLICATION_ID = 0;
    public static final int ERROR_DEVELOPER_ID = 1;
    public static final int ERROR_RUNAME = 2;
    public static final int ERROR_CERTIFICATE_ID = 3;

    private int errorNumber = -1;

    public EbayConfigurationException(String message) {
	super(message);
    }

    public EbayConfigurationException(String message, Throwable cause) {
	super(message, cause);
    }

    public EbayConfigurationException(Throwable cause) {
	super(cause);
    }

    public int getErrorNumber() {
	return errorNumber;
    }
}
