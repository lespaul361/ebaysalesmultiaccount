/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.ebaydatalayer;

import com.ebay.sdk.ApiAccount;
import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.ApiException;
import com.ebay.sdk.SdkException;
import com.ebay.sdk.call.FetchTokenCall;
import com.ebay.sdk.call.GetSessionIDCall;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import com.github.lespaul361.commons.commonroutines.utilities.DesktopApi;
import com.github.lespaul361.ebaydatalayer.Exceptions.EbayConfigurationException;
import com.github.lespaul361.internetsales.configurationfile.AbstractLogError;

import java.awt.Frame;
import java.awt.Image;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.github.lespaul361.internetsales.gui.forms.ProgressForm;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.SecondaryLoop;
import java.awt.Toolkit;
import java.awt.Window;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

/**
 * 
 * Helps encapsulate the code for creating and basic account information from,
 * the <code>Ebaysdk</code>
 */
public class EbayLogInHelper extends AbstractLogError {

    private final static String SOAP_URL = "https://api.ebay.com/wsapi";
    private final static String SANDBOX_URL = "https://api.sandbox.ebay.com/wsapi";
    private final SiteCodeType siteCodeType;
    final private Image logo;
    private ProgressForm progressForm;
    private Window parentWindow = null;
    private Frame parentFrame = null;
    private final String url;
    private String sessionID = "";
    private ApiContext apiContext = null;
    private String token = "";
    final private String certificateId;
    final private String appId;
    final private String developerName;
    final private String ruName;
    private final EventListenerList listeners = new EventListenerList();
    private String accountName;
    private boolean showProgressForm;
    private volatile boolean cancel = false;
    private Toolkit tkit = Toolkit.getDefaultToolkit();
    private final boolean isSandBox;
    private Thread thr = null;

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL and shows a progress
     * form
     * 
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param logger
     *            the base {@link Logger}
     */
    public EbayLogInHelper(String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Logger logger) {

	this(null, certificateId, appId, developerName, ruName, siteCodeType, logo, null, logger);

    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL and shows a progress
     * form
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param logger
     *            the base {@link Logger}
     */
    public EbayLogInHelper(Frame parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, Logger logger) {

	this(parent, certificateId, appId, developerName, ruName, siteCodeType, logo, null, logger, true);
    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param logger
     *            the base {@link Logger}
     * @param showProgressForm
     *            show a progress form or not
     */
    public EbayLogInHelper(Frame parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, Logger logger, boolean showProgressForm) {

	this(parent, certificateId, appId, developerName, ruName, siteCodeType, logo, apiContext, false, logger,
		showProgressForm);

    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL and shows a progress
     * form
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param logger
     *            the base {@link Logger}
     */
    public EbayLogInHelper(Window parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, Logger logger) {
	this(parent, certificateId, appId, developerName, ruName, siteCodeType, logo, null, logger, true);
    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param logger
     *            the base {@link Logger}
     * @param showProgressForm
     *            show a progress form or not
     */
    public EbayLogInHelper(Window parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, Logger logger, boolean showProgressForm) {

	this(parent, certificateId, appId, developerName, ruName, siteCodeType, logo, apiContext, false, logger,
		showProgressForm);

    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL and shows a progress
     * form
     * 
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param isSandBox
     *            to use the sand box or not
     * @param logger
     *            the base {@link Logger}
     */
    public EbayLogInHelper(String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, boolean isSandBox, Logger logger) {

	super(logger);
	this.siteCodeType = siteCodeType;
	this.logo = logo;
	this.appId = appId;
	this.certificateId = certificateId;
	this.developerName = developerName;
	this.ruName = ruName;
	if (apiContext != null) {
	    this.apiContext = (ApiContext) this.apiContext;
	}
	this.showProgressForm = showProgressForm;
	this.parentFrame = new DummyFrame("Ebay Log In", logo);
	this.isSandBox = isSandBox;
	this.url = (isSandBox) ? SANDBOX_URL : SOAP_URL;
	init();

    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL and shows a progress
     * form
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param isSandBox
     *            to use the sand box or not
     * @param logger
     *            the base {@link Logger}
     */
    public EbayLogInHelper(Frame parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, boolean isSandBox, Logger logger) {

	this(parent, certificateId, appId, developerName, ruName, siteCodeType, logo, null, logger, true);
    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param isSandBox
     *            to use the sand box or not
     * @param logger
     *            the base {@link Logger}
     * @param showProgressForm
     *            show a progress form or not
     */
    public EbayLogInHelper(Frame parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, boolean isSandBox, Logger logger,
	    boolean showProgressForm) {
	super(logger);
	this.siteCodeType = siteCodeType;
	this.logo = logo;
	this.appId = appId;
	this.certificateId = certificateId;
	this.developerName = developerName;
	this.ruName = ruName;
	if (apiContext != null) {
	    this.apiContext = (ApiContext) this.apiContext;
	}
	this.showProgressForm = showProgressForm;
	this.parentFrame = parent;
	this.isSandBox = isSandBox;
	this.url = (isSandBox) ? SANDBOX_URL : SOAP_URL;
	init();
    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL and shows a progress
     * form
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param isSandBox
     *            to use the sand box or not
     * @param logger
     *            the base {@link Logger}
     */
    public EbayLogInHelper(Window parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, boolean isSandBox, Logger logger) {
	this(parent, certificateId, appId, developerName, ruName, siteCodeType, logo, null, logger, true);
    }

    /**
     * Constructs a new login helper for getting account information and
     * creating new accounts using the production Ebay URL
     * 
     * @param parent
     *            the parent to use for {@link JFrame}s made in this class
     * @param certificateId
     *            the application certificate ID
     * @param appId
     *            the application ID
     * @param developerName
     *            the application developer ID
     * @param ruName
     *            the application RU name
     * @param siteCodeType
     *            the country code as a {@link SiteCodeType}
     * @param logo
     *            the company logo
     * @param apiContext
     *            the {link ApiContext} passed as an Object
     * @param isSandBox
     *            to use the sand box or not
     * @param logger
     *            the base {@link Logger}
     * @param showProgressForm
     *            show a progress form or not
     */
    public EbayLogInHelper(Window parent, String certificateId, String appId, String developerName, String ruName,
	    SiteCodeType siteCodeType, Image logo, Object apiContext, boolean isSandBox, Logger logger,
	    boolean showProgressForm) {
	super(logger);
	this.siteCodeType = siteCodeType;
	this.logo = logo;
	this.appId = appId;
	this.certificateId = certificateId;
	this.developerName = developerName;
	this.ruName = ruName;
	if (apiContext != null) {
	    this.apiContext = (ApiContext) this.apiContext;
	}
	this.showProgressForm = showProgressForm;
	this.parentWindow = parent;
	this.isSandBox = isSandBox;
	this.url = (isSandBox) ? SANDBOX_URL : SOAP_URL;
	init();
    }

    public void getEbaySessionID(final ApiContext apiContext) throws Exception {
        if (isShowProgressForm()) {
            progressForm.setVisible(true);
            progressForm.updateLabel("Getting Session ID."
                    + "\n This may take a couple of minutes");
        }
        Toolkit kit = Toolkit.getDefaultToolkit();
        final SecondaryLoop loop
                = kit.getSystemEventQueue().createSecondaryLoop();

        class GetIDRunnable extends AbstractRunnableWithException {

            @Override
            public void run() {
                GetSessionIDCall call = new GetSessionIDCall(getApiContext());
                call.setRuName(ruName);
                try {
                    sessionID = call.getSessionID();

                } catch (com.ebay.sdk.SdkSoapException eSoap) {
                    call = null;
                    String message = eSoap.getMessage();
                    if (message.toLowerCase().contains("application")) {
                        Exception sdkException = new EbayConfigurationException(eSoap);
                        this.exception = sdkException;
                    }
                } catch (Exception ex) {
                    call = null;
                    this.exception = ex;
                    loop.exit();
                }
                progressForm.getBaseDialog().setVisible(false);
                loop.exit();
                
            }
        }

        GetIDRunnable runnable = new GetIDRunnable();
        thr = new Thread(runnable);
        thr.start();

        loop.enter();
        if (thr.isAlive()) {
            try {
                thr = null;
            } catch (Exception e) {
            }
        }
        if (runnable.isException()) {
            throw runnable.getException();
        } else {
            EbayValueEvent evt = new EbayValueEvent(getSessionID(),
                    apiContext, EbayValueEvent.RETURN_TYPE_SESSION_ID,
                    this);
            fireEvent(evt);
        }
    }

    public void getSecurityTokenFromLogIn() throws ApiException, SdkException,
            Exception {
        if (isShowProgressForm()) {
            progressForm.setVisible(true);
            progressForm.updateLabel("Please wait for browser and log in");
            Thread.sleep(2000);
            progressForm.getBaseDialog().setVisible(false);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = getURL(ruName, getSessionID());
                loadPage(url);
                Object o = null;
                try {
                    o = waitForToken();
                } catch (Exception ex) {
                    logError(Level.SEVERE, ex.getMessage(), ex);
                }

                token = (String) o;
                EbayValueEvent evt = new EbayValueEvent(getToken(), getApiContext(),
                        EbayValueEvent.RETURN_TYPE_TOKEN, this);
                fireEvent(evt);
            }
        }).start();
    }

    public void getAccountNameFromToken() {
        if (isShowProgressForm()) {
            progressForm.getBaseDialog().setVisible(true);
            progressForm.updateLabel("Getting Account Name");
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    accountName = EbayAccountLive.getUserID(getApiContext(), getToken());
                    EbayValueEvent evt = new EbayValueEvent(getToken(), getApiContext(),
                            EbayValueEvent.RETURN_TYPE_ACCOUNT_NAME, this);
                    fireEvent(evt);
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }
            }
        }).start();
    }

    public ApiContext getApiContext() {
	if (this.apiContext != null) {
	    return this.apiContext;
	}
	ApiContext apiContext = new ApiContext();
	String url = null;
	// set Api Token to access EbayApi Server
	ApiCredential cred = apiContext.getApiCredential();
	ApiAccount account = new ApiAccount();
	account.setApplication(this.appId);
	account.setCertificate(this.certificateId);
	account.setDeveloper(this.developerName);
	cred.setApiAccount(account);
	apiContext.setApiServerUrl(this.url);
	apiContext.setApiCredential(cred);
	apiContext.setSite(siteCodeType);

	return apiContext;
    }

    /**
     * @return the sessionID
     */
    public String getSessionID() {
	return sessionID;
    }

    /**
     * @return the token
     */
    public String getToken() {
	return token;
    }

    public void addEbayValueListener(EbayValueListener l) {
	this.listeners.add(EbayValueListener.class, l);
    }

    public void removeEbayValueListener(EbayValueListener l) {
	this.listeners.remove(EbayValueListener.class, l);
    }

    /**
     * @return the accountName
     */
    public String getAccountName() {
	return accountName;
    }

    /**
     * @return the showProgressForm
     */
    public boolean isShowProgressForm() {
	return showProgressForm;
    }

    /**
     * Sets the visibility of the progress form
     * 
     * @param b
     *            the visibility of the progress form
     */
    public void setProgressFormVisible(boolean b) {
	this.progressForm.getBaseDialog().setVisible(b);
    }

    /**
     * Disposes of the base {@link Dialog} of the progress form
     */
    public void disposeProgressForm() {
	this.progressForm.getBaseDialog().dispose();
    }

    /**
     * @param showProgressForm
     *            the showProgressForm to set
     */
    public void setShowProgressForm(boolean showProgressForm) {
	this.showProgressForm = showProgressForm;
    }

    private void fireEvent(EbayValueEvent evt) {
	EbayValueListener[] listeners = this.listeners.getListeners(EbayValueListener.class);
	for (int i = 0; i < listeners.length; i++) {
	    listeners[i].valueReceived(evt);
	}
    }

    private void init() {
	if (showProgressForm) {
	    initProgressForm();
	}
    }

    private void initProgressForm() {
	Runnable runnable = new Runnable() {
	    public void run() {
		progressForm = new ProgressForm((parentFrame == null) ? parentWindow : parentFrame, true, logo,
			getLogger());
		progressForm.getProgress().setIndeterminate(true);
		progressForm.updateLabel("Getting Session ID." + "\n This may " + "take a couple of minutes");
		progressForm.getBaseDialog().setVisible(false);
	    }
	};
	if (SwingUtilities.isEventDispatchThread()) {
	    runnable.run();
	} else {
	    SwingUtilities.invokeLater(runnable);
	}
    }

    private void loadPage(String url) {
	try {
	    DesktopApi.openBrowserWindow(url);
	} catch (Exception e) {
	    logError(Level.SEVERE, e.getMessage(), e);
	}
    }

    protected String getURL(final String ruName, final String sessionID) {
	String url2 = this.url.concat("?SignIn&RUName={0}&SessID={1}");
	String urlConsent = null;
	urlConsent = MessageFormat.format(url2, ruName, sessionID);

	return urlConsent;
    }

    private Object waitForToken() throws ApiException, SdkException, Exception {
        int milliToSec = 1000;
        int secToMin = 60;
        int minTimeOut = 30;
        int timeOutInMilli = (milliToSec * secToMin) * minTimeOut;
        long maxTimeOut = System.currentTimeMillis() + timeOutInMilli;//kill 
        token = null;
        final EventQueue eq = this.tkit.getSystemEventQueue();
        final SecondaryLoop loop = eq.createSecondaryLoop();

        // Spawn a new thread to do the work
        Runnable runnable = () -> {
            FetchTokenCall tokenCall = new FetchTokenCall(getApiContext());
            tokenCall.setSessionID(getSessionID());
            while (true) {
                if (cancel) {
                    break;
                }
                try {
                    token = tokenCall.fetchToken();
                } catch (Exception e) {
                    if (e.getMessage().contains("not completed Auth & Auth")) {
                        continue;
                    } else {
                        e.printStackTrace(System.err);
                        logError(Level.SEVERE, "Error getting token", e);
                        break;
                    }
                }
                if (token != null && !token.trim().equalsIgnoreCase("")) {
                    System.out.println("Token: " + token);
                    break;
                }
                if (System.currentTimeMillis() > maxTimeOut) {
                    System.err.println("get log in time out");
                    break;
                }
            }
            loop.exit();
        };

        Thread thr = new Thread(runnable);
        thr.start();

        // Enter the loop to block the current event
        // handler, but leave UI responsive
        if (!loop.enter()) {
            System.err.println("Error starting secondloop");
        }

        return token;
    }

    public void cancel() {
	this.cancel = true;

	try {
	    this.progressForm.getBaseDialog().getOwner().dispose();
	} catch (Exception e) {
	}
	try {
	    this.progressForm.getBaseDialog().dispose();
	} catch (Exception e) {
	}
    }
}
