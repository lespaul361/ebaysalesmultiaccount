/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.github.lespaul361.internetsales.launcher;

import java.io.File;

/**
 *
 * @author David Hamilton
 */
 class InstallJRE {
private final boolean is64Bit;

    public InstallJRE(boolean is64Bit) {
        this.is64Bit=is64Bit;
        File workingDir = new File(System.getProperty("user.dir"));
        File jreDir=new File(workingDir.getAbsolutePath()+"\\jre");
        
    }
    
    private File downloadToTemp(){
        return null;
    }

}
