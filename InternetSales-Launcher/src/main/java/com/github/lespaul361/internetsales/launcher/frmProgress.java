/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.launcher;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 *
 * @author David Hamilton
 */
public class frmProgress {

    private final JDialog dialog;
    private final JProgressBar progressBar;
    private final JLabel labelText;
    private final JLabel labelFileCount;

    public frmProgress(String text) {
        dialog = new JDialog();
        progressBar = new JProgressBar();
        labelText = new JLabel();
        labelFileCount = new JLabel();
        init(text);
    }

    private void init(String text) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        labelText.setText(text);
        panel.add(Box.createVerticalStrut(15));
        Box boxH = Box.createHorizontalBox();
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(labelText);
        boxH.add(Box.createHorizontalGlue());
        panel.add(boxH);
        panel.add(Box.createVerticalStrut(10));
        boxH = Box.createHorizontalBox();
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(progressBar);
        boxH.add(Box.createHorizontalStrut(10));
        panel.add(boxH);
        panel.add(Box.createVerticalStrut(5));
        boxH = Box.createHorizontalBox();
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(labelFileCount);
        panel.add(boxH);
        panel.add(Box.createVerticalStrut(10));
        dialog.getContentPane().add(panel);
        dialog.pack();
        dialog.setLocationRelativeTo(null);

    }
}
