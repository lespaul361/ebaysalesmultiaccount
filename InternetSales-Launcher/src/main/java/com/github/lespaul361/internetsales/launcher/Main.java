/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.launcher;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author David Hamilton
 */
public class Main {

    public static void main(String[] args) {
        Main m = new Main();
        m.start();
    }

    private void start() {
        if (!vmExists()) {
            if (isWindows()) {
                if (is64Bit()) {

                } else {

                }
            }
        }
    }

    private static void installJRE() {

    }

    private static void deleteJREFolder() {
        File workingDir = new File(System.getProperty("user.dir"));
        File[] files = workingDir.listFiles();
        File jreFolder = null;
        for (int i = 0; i < files.length; i++) {
            File fle = files[i];
            if (fle.isDirectory()) {
                if (fle.getName().toLowerCase().contains("jre")) {
                    jreFolder = fle;
                    if (fle.getName() != "jre") {
                        recurseDeleteFile(fle);
                    } else {
                        break;
                    }
                    break;
                }
            }
        }

    }

    private static void recurseDeleteFile(File dir) {
        List<File> files = Arrays.asList(dir.listFiles());
        for (File file : files) {
            if (file.isDirectory()) {
                recurseDeleteFile(file);
            } else {
                file.delete();
            }
        }
    }

    private static boolean vmExists() {
        try {
            File workingDir = new File(System.getProperty("user.dir"));
            JOptionPane.showMessageDialog(null, workingDir);
            File[] files = workingDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                File fle = files[i];
                if (fle.isDirectory()) {
                    if (fle.getName().toLowerCase().contains("jre")) {
                        if (fle.getName().toLowerCase().equalsIgnoreCase("java")) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        return false;
    }

    private static boolean is64Bit() {
        boolean ret = System.getProperty("sun.arch.data.model").contains("64");
        return ret;
    }

    private static boolean isWindows() {
        boolean ret = System.getProperty("os.name").toLowerCase().contains("win");
        return ret;
    }

    private static void installJRE(String path) {

    }
}
