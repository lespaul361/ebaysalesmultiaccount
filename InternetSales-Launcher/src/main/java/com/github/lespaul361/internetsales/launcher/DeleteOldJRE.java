/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.launcher;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author David Hamilton
 */
public class DeleteOldJRE {

    public static List<File> getJREFolderFiles(File folder) {
        List<File> files = recurseDirectories(folder);
        return files;
    }

    public static void deleteFiles(List<File> files) {

    }

    private static List<File> recurseDirectories(File folder) {
        List<File> returnFiles = new ArrayList<>(500);
        List<File> files = new ArrayList<>(Arrays.asList(folder.listFiles()));
        files.forEach((file) -> {
            if (file.isDirectory()) {
                returnFiles.addAll(recurseDirectories(file));
            } else {
                returnFiles.add(file);
            }
        });
        return returnFiles;
    }
}
