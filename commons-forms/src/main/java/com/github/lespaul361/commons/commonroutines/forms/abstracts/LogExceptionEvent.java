/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.forms.abstracts;

import java.awt.Frame;
import java.util.EventObject;
import java.util.logging.Level;

/**
 * 
 * @author Charles Hamilton
 */
public class LogExceptionEvent extends EventObject {

    private final Throwable ex;
    private final Level level;
    private final int exitCode;
    private final boolean showMessage;
    private final Frame parent;

    public LogExceptionEvent(Throwable ex, Level level, Object source) {
	this(ex, level, false, source);
    }

    public LogExceptionEvent(Throwable ex, Level level, boolean exit, Object source) {
	this(ex, level, exit, 0, source);
    }

    public LogExceptionEvent(Throwable ex, Level level, boolean exit, int exitCode, Object source) {
	this(ex, level, exit, exitCode, true, source);
    }

    public LogExceptionEvent(Throwable ex, Level level, boolean exit, int exitCode, boolean showMessage, Object source) {
	this(ex, level, exit, exitCode, showMessage, null, source);
    }

    public LogExceptionEvent(Throwable ex, Level level, boolean showMessage, Frame parent, Object source) {
	this(ex, level, false, 0, showMessage, parent, source);
    }

    public LogExceptionEvent(Throwable ex, Level level, boolean exit, int exitCode, boolean showMessage, Frame parent,
	    Object source) {

	super(source);
	this.ex = ex;
	this.level = level;
	this.exitCode = exitCode;
	this.showMessage = showMessage;
	this.parent = parent;
    }

    /**
     * @return the ex
     */
    public Throwable getEx() {
	return ex;
    }

    /**
     * @return the level
     */
    public Level getLevel() {
	return level;
    }

    /**
     * @return the exitCode
     */
    public int getExitCode() {
	return exitCode;
    }

    /**
     * @return the showMessage
     */
    public boolean isShowMessage() {
	return showMessage;
    }

    /**
     * @return the parent
     */
    public Frame getParent() {
	return parent;
    }

}
