/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.forms;

import java.awt.Dimension;
import java.awt.Frame;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * A simple progress form
 */
public class ProgressForm extends JDialog {
    private javax.swing.JLabel jLabel1;
    private javax.swing.JProgressBar jProgressBar1;
    
    /**
     * Creates a modeless dialog without a title and without a specified
     * {@code Frame} owner. A shared, hidden frame will be set as the owner of
     * the dialog.
     * <p>
     * This constructor sets the component's locale property to the value
     * returned by {@code JComponent.getDefaultLocale}.
     * <p>
     * NOTE: This constructor does not allow you to create an unowned
     * {@code JDialog}. To create an unowned {@code JDialog} you must use either
     * the {@code JDialog(Window)} or {@code JDialog(Dialog)} constructor with
     * an argument of {@code null}.
     *
     * @throws HeadlessException if {@code GraphicsEnvironment.isHeadless()}
     * returns {@code true}.
     * @see java.awt.GraphicsEnvironment#isHeadless
     * @see JComponent#getDefaultLocale
     */
    public ProgressForm() {
        super();
    }

    /**
     * Creates a dialog with an empty title and the specified modality and
     * {@code Frame} as its owner. If {@code owner} is {@code null}, a shared,
     * hidden frame will be set as the owner of the dialog.
     * <p>
     * This constructor sets the component's locale property to the value
     * returned by {@code JComponent.getDefaultLocale}.
     * <p>
     * NOTE: This constructor does not allow you to create an unowned
     * {@code JDialog}. To create an unowned {@code JDialog} you must use either
     * the {@code JDialog(Window)} or {@code JDialog(Dialog)} constructor with
     * an argument of {@code null}.
     *
     * @param owner the {@code Frame} from which the dialog is displayed
     * @param modal specifies whether dialog blocks user input to other
     * top-level windows when shown. If {@code true}, the modality type property
     * is set to {@code DEFAULT_MODALITY_TYPE}, otherwise the dialog is
     * modeless.
     * @throws HeadlessException if {@code GraphicsEnvironment.isHeadless()}
     * returns {@code true}.
     * @see java.awt.GraphicsEnvironment#isHeadless
     * @see JComponent#getDefaultLocale
     */
    public ProgressForm(Frame owner, boolean modal) {
        super(owner, modal);
    }

    /**
     * Creates a modeless dialog with the specified title and owner
     * {@code Window}.
     * <p>
     * This constructor sets the component's locale property to the value
     * returned by {@code JComponent.getDefaultLocale}.
     *
     * @param owner the {@code Window} from which the dialog is displayed or
     * {@code null} if this dialog has no owner
     * @param title the {@code String} to display in the dialog's title bar or
     * {@code null} if the dialog has no title
     *
     * @throws IllegalArgumentException if the {@code owner} is not an instance
     * of {@link java.awt.Dialog Dialog} or {@link java.awt.Frame Frame}
     * @throws IllegalArgumentException if the {@code owner}'s
     * {@code GraphicsConfiguration} is not from a screen device
     * @throws HeadlessException when {@code GraphicsEnvironment.isHeadless()}
     * returns {@code true}
     *
     * @see java.awt.GraphicsEnvironment#isHeadless
     * @see JComponent#getDefaultLocale
     *
     * @since 1.6
     */
    public ProgressForm(Frame owner, String title) {
        super(owner, title);
    }

    /**
     * Creates a dialog with the specified title, owner {@code Frame} and
     * modality. If {@code owner} is {@code null}, a shared, hidden frame will
     * be set as the owner of this dialog.
     * <p>
     * This constructor sets the component's locale property to the value
     * returned by {@code JComponent.getDefaultLocale}.
     * <p>
     * NOTE: Any popup components ({@code JComboBox}, {@code JPopupMenu},
     * {@code JMenuBar}) created within a modal dialog will be forced to be
     * lightweight.
     * <p>
     * NOTE: This constructor does not allow you to create an unowned
     * {@code JDialog}. To create an unowned {@code JDialog} you must use either
     * the {@code JDialog(Window)} or {@code JDialog(Dialog)} constructor with
     * an argument of {@code null}.
     *
     * @param owner the {@code Frame} from which the dialog is displayed
     * @param title the {@code String} to display in the dialog's title bar
     * @param modal specifies whether dialog blocks user input to other
     * top-level windows when shown. If {@code true}, the modality type property
     * is set to {@code DEFAULT_MODALITY_TYPE} otherwise the dialog is modeless
     * @throws HeadlessException if {@code GraphicsEnvironment.isHeadless()}
     * returns {@code true}.
     *
     * @see java.awt.Dialog.ModalityType
     * @see java.awt.Dialog.ModalityType#MODELESS
     * @see java.awt.Dialog#DEFAULT_MODALITY_TYPE
     * @see java.awt.Dialog#setModal
     * @see java.awt.Dialog#setModalityType
     * @see java.awt.GraphicsEnvironment#isHeadless
     * @see JComponent#getDefaultLocale
     */
    public ProgressForm(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
    }
@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Init Code">
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1.setText("Ready");

        JPanel pnl = new JPanel();
        pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
        pnl.add(Box.createVerticalStrut(15));
        Box boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(15));
        boxh.add(jLabel1);
        boxh.add(Box.createHorizontalStrut(15));
        boxh.add(Box.createHorizontalGlue());
        pnl.add(boxh);
        pnl.add(Box.createHorizontalStrut(10));
        jProgressBar1.setPreferredSize(new Dimension(300, 20));
        boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(15));
        boxh.add(jProgressBar1);
        boxh.add(Box.createHorizontalStrut(15));
        pnl.add(boxh);
        pnl.add(Box.createVerticalStrut(5));
        getContentPane().add(pnl);
        pack();
    }
}
