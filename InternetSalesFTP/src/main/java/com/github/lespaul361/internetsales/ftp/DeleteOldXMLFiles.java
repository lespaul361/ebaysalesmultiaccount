/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.ftp;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.commons.net.ftp.FTPFile;

/**
 *
 * @author Charles Hamilton
 */
public class DeleteOldXMLFiles {

    protected final String iP;
    protected final String user;
    protected final String passWord;
    protected LocalDate lastRan = null;
    protected final FTPMethods ftp;

    public DeleteOldXMLFiles(String iP, String user, String passWord) {
        this.iP = iP;
        this.user = user;
        this.passWord = passWord;
        FTPMethods f = null;
        try {
            f = new FTPMethods(user, passWord, iP);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        ftp = f;
    }

    public boolean delete(byte weeks) {
        Calendar start = Calendar.getInstance();
        start.add(Calendar.DATE, (weeks * 7) * -1);
        return delete(start);
    }

    public boolean delete(Calendar since) {
        return delete(convertToLocalDate(since));
    }

    public boolean delete(LocalDate since) {
        boolean ret = deleteOldFiles(since);
        ftp.CloseConnection();
        System.gc();
        return ret;
    }

    private boolean deleteOldFiles(LocalDate since) {
        if (!isRun()) {
            return false;
        }
        try {
            ftp.logIn(iP, user, passWord);
            List<FTPFile> files = ftp.getEbaySales();
            Map<FTPFile, Long> fileMap = getFileTimesMili(files);
            List<FTPFile> oldFiles = getOldFiles(since, fileMap);
            deleteFilesFromServer(oldFiles);
            this.lastRan = LocalDate.now();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return true;
    }

    private boolean isRun() {
        if (lastRan == null) {
            return true;
        }

        LocalDate now = LocalDate.now();
        int compare = now.compareTo(lastRan);

        return compare > 0;
    }

    private LocalDate convertToLocalDate(Calendar calendar) {
        TimeZone tz = calendar.getTimeZone();
        ZoneId zid = (tz == null) ? ZoneId.systemDefault() : tz.toZoneId();
        LocalDate localDate = LocalDateTime
                .ofInstant(calendar.toInstant(), zid)
                .toLocalDate();

        return localDate;
    }

    private long getFileTime(String name) {
        int len = name.lastIndexOf(".");
        name = name.substring(0, len);
        return Long.parseLong(name);
    }

    private Map<FTPFile, Long> getFileTimesMili(List<FTPFile> files) {
        Map<FTPFile, Long> ret = new HashMap<>(200);

        for (FTPFile file : files) {
            long dateTime = getFileTime(file.getName());
            ret.put(file, dateTime);
        }

        return ret;
    }

    private List<FTPFile> getOldFiles(LocalDate localDate, Map<FTPFile, Long> fileMap) {
        LocalDateTime dateTime = localDate.atStartOfDay();
        ZonedDateTime zdt = dateTime.atZone(ZoneId.of("America/Los_Angeles"));
        long millis = zdt.toInstant().toEpochMilli();
        Iterator<FTPFile> iterator = fileMap.keySet().iterator();
        List<FTPFile> ret = new ArrayList<>(100);
        int compare = 0;

        while (iterator.hasNext()) {
            FTPFile file = iterator.next();
            long dateTimeMili = (long) fileMap.get(file);
            compare = Long.compare(millis, dateTimeMili);
            if (compare > 0 || compare == 0) {
                ret.add(file);
            }
        }

        return ret;
    }

    private void deleteFilesFromServer(List<FTPFile> oldFiles) {
        for (FTPFile file : oldFiles) {
            try {
                ftp.Delete(file.getName());
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }
        }
    }
}
