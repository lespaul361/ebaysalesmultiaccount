/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.internetsales.ftp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import static org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPCmd;
import org.apache.commons.net.ftp.FTPFile;

/**
 * Wrapper for common FTP operations for this application
 *
 * @author David Hamilton
 * @version $Id: $Id
 */
public class FTPMethods implements AutoCloseable {

    FTPClient client = null;
    private final String USER_NAME;
    private final String PASS_WORD;
    private final String HOST;
    public final String EBAY_SALES_PATH = "ebaysales";
    public final String ARCHIVE_PATH = EBAY_SALES_PATH + "\\archive";

    /**
     * Constructs a new FTPMethods class and connects to the server
     *
     * @param USER_NAME the user name
     * @param PASS_WORD the password
     * @param HOST the host address
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public FTPMethods(String USER_NAME, String PASS_WORD, String HOST)
            throws SocketException, IOException {
        this.USER_NAME = USER_NAME;
        this.PASS_WORD = PASS_WORD;
        this.HOST = HOST;
        logIn(HOST, USER_NAME, PASS_WORD);
    }

    /**
     * Closes the connection property with the FTP server
     */
    public void CloseConnection() {
        try {
            client.sendCommand(FTPCmd.QUIT);
        } catch (Exception ex) {
        }
        client = null;
    }

    /**
     * Gets all the XML file info for the eBay sales
     *
     * @return a <code>List</code> of <code>FTPFile</code>
     * @see FTPFile
     * @see List
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public List<FTPFile> getEbaySales() throws SocketException, IOException {
        List<FTPFile> xmlFiles = new ArrayList<FTPFile>();
        changeDirectoryWorker(client, "ebaysales");
        try {
            String nm = client.printWorkingDirectory();
            String ext = "";

            FTPFile[] fles = client.listFiles(nm);
            List<FTPFile> allFiles = new ArrayList<>(Arrays.asList(fles));

            xmlFiles = allFiles.stream()
                    .filter((file) -> {
                        return file.getName().toLowerCase().endsWith("xml");
                    })
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            return null;
        }

        return xmlFiles;

    }

    /**
     * Deletes the specified file from the FTP server
     *
     * @param fileName the name of the file to delete
     * @return a boolean
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any. @ throws java.lang.Exception if any.
     */
    public boolean Delete(String fileName) throws SocketException, IOException,
            Exception {
        return client.deleteFile(fileName);
    }

    /**
     * Gets a <code>List</code> wit the <code>FTPFile</code> information of the
     * saved archive files
     *
     * @return a <code>List</code> wit the <code>FTPFile</code> information of
     * the saved archive files
     * @see List
     * @see FTPFile
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public List<FTPFile> getArchives() throws SocketException, IOException {
        return listFiles(this.ARCHIVE_PATH, null);
    }

    /**
     * Changes the working directory for the FTP client
     *
     * @param name name of the directory to change to
     * @throws java.io.IOException if any.
     */
    public void changeDir(String name) throws IOException {
        if (name.contains("\\")) {
            String[] paths = name.split("\\\\");
            for (int i = 0; i < paths.length; i++) {
                String path = paths[i];
                if (!path.trim().isEmpty()) {
                    client.changeWorkingDirectory(path);
                    String tmp = client.printWorkingDirectory();
                    if (tmp == null || !tmp.contains(path)) {
                        throw new IOException("Invalid Directory");
                    }
                }
            }
        } else {
            client.changeWorkingDirectory(name);
            String tmp = client.printWorkingDirectory();
            if (tmp == null || !tmp.contains(name)) {
                throw new IOException("Invalid Directory");
            }
        }
    }

    /**
     * Uploads a file to the FTP server
     *
     * @param file a <code>FTPFile</code> with the destination file name
     * @throws java.net.SocketException
     * @throws java.io.IOException
     * @see FTPFile
     * @param stream an <code>InputStream</code> with the data to upload
     * @see InputStream
     * @see org.apache.commons.net.ftp.FTPFile
     */
    public void uploadFile(FTPFile file, InputStream stream) 
            throws SocketException, IOException {
        client.setFileType(FTPClient.BINARY_FILE_TYPE);
        client.storeFile(file.getName(), stream);
        int reply = client.getReplyCode();
        if (reply == 550) {
            throw new SocketException("Access To Server Denied");
        }

    }

    /**
     * Uploads a file to the server in the current path
     *
     * @param name the server name of the file
     * @param file the file containing the data
     * @throws java.net.SocketException
     * @throws IOException any file read or write exceptions @ throws Exception
     * if any.
     */
    public void uploadFile(String name, File file) 
            throws SocketException, IOException {
        if (!file.exists()) {
            throw new IOException("file " + file.getAbsolutePath()
                    + " does not exist");
        }
        FTPFile ftpFile = new FTPFile();
        ftpFile.setName(name);
        FileInputStream fis = new FileInputStream(file);
        uploadFile(ftpFile, fis);
    }

    /**
     * Uploads a file to the server in the provided path
     *
     * @param serverDir the path to the server's folder separated by "\"
     * @param name the server name of the file
     * @param file the file containing the data
     * @throws java.net.SocketException
     * @throws IOException any file read or write exceptions @ throws Exception
     * if any.
     */
    public void uploadFile(String serverDir, String name, File file)
            throws SocketException, IOException {
        client.changeWorkingDirectory("/");
        changeDir(serverDir);
        uploadFile(name, file);
    }

    /**
     * Uploads a file to the server in the current path
     *
     * @param name the server name of the file
     * @param data a byte array of the data to upload
     * @throws java.net.SocketException
     * @throws IOException any file read or write exceptions @ throws Exception
     * if any.
     */
    public void uploadFile(String name, byte[] data) 
            throws SocketException, IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        FTPFile ftpFile = new FTPFile();
        ftpFile.setName(name);
        uploadFile(ftpFile, bais);
    }

    /**
     * Uploads a file to the server in the provided path
     *
     * @param serverDir the path to the server's folder separated by "\"
     * @param name the server name of the file
     * @param data a byte array of the data to upload
     * @throws java.net.SocketException
     * @throws IOException any file read or write exceptions @ throws Exception
     * if any.
     */
    public void uploadFile(String serverDir, String name, byte[] data)
            throws SocketException, IOException {
        client.changeWorkingDirectory("/");
        changeDir(serverDir);
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        FTPFile ftpFile = new FTPFile();
        ftpFile.setName(name);
        uploadFile(ftpFile, bais);
    }

    /**
     * Gets the <code>InputStream</code> of the file
     *
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @param file FTPfile of the the file to download
     * @return the <code>InputStream</code> of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public InputStream getFile(String workingDirectory, FTPFile file)
            throws SocketException, IOException {
        return getFile(workingDirectory, file.getName());
    }

    /**
     * Gets the <code>InputStream</code> of the file
     *
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @param fileName the name of the file to download
     * @return the <code>InputStream</code> of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public InputStream getFile(String workingDirectory, String fileName)
            throws SocketException, IOException {
        if (!client.isConnected()) {
            logIn(HOST, HOST, PASS_WORD);
        }
        try {
            client.setFileType(BINARY_FILE_TYPE);
            if (workingDirectory != null) {
                String[] paths = workingDirectory.split("\\\\");
                for (String s : paths) {
                    client.changeWorkingDirectory(s);
                }
            }
            String path = client.printWorkingDirectory();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (client.retrieveFile(fileName, baos)) {
                baos.flush();
                byte[] buffer = baos.toByteArray();
                InputStream in = new ByteArrayInputStream(buffer);
                buffer = null;
                return in;
            }
            int i = client.getReply();
            throw new IOException("File did not download properly");
        } catch (SocketException e) {
            if (e.getMessage().toLowerCase().contains("connection reset")) {
                logIn(HOST, HOST, PASS_WORD);
                System.err.println(this.getClass().getName()
                        + " line 304 reset and attempting reconnect");
                return getFile(workingDirectory, fileName);
            } else {
                throw e;
            }
        }
    }

    /**
     * Get a byte array with the data
     *
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @param file FTPfile of the the file to download
     * @return a byte array of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public byte[] getFileBytes(String workingDirectory, FTPFile file)
            throws SocketException, IOException {
        return getFileBytes(workingDirectory, file.getName());
    }

    /**
     * Gets a byte array wit the data
     *
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @param fileName the name of the file to download
     * @return the <code>InputStream</code> of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public byte[] getFileBytes(String workingDirectory, String fileName)
            throws SocketException, IOException {
        ByteArrayOutputStream baos;
        byte[] ret;
        try (InputStream in = getFile(workingDirectory, fileName)) {
            baos = new ByteArrayOutputStream();
            org.apache.commons.io.IOUtils.copy(in, baos);
            ret = baos.toByteArray();
        }
        baos.close();
        baos = null;
        return ret;
    }

    /**
     * Gets a list of the files in the given directory that match the extensions
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @param extentions a list of extensions not including the ".". Null will
     * return all files
     * @return a list of FTPFile that fit the search
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static List<FTPFile> listFiles(String host, String userName,
            String password, String workingDirectory, List<String> extentions)
            throws SocketException, IOException {
        return listFiles(null, host, userName, password, workingDirectory, extentions);
    }

    /**
     * Gets a list of the files in the given directory that match the extensions
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for root directory
     * @return a list of FTPFile that fit the search
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static List<FTPFile> listFiles(String host, String userName,
            String password, String workingDirectory) throws SocketException,
            IOException {
        FTPClient client = getStaticClient(host, userName, password);
        return listFiles(client, host, userName, password, workingDirectory);
    }

    /**
     * Gets a list of the files in the given directory that match the extensions
     *
     * @param client the ftp client for this connection
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @param extentions a list of extensions not including the ".". Null will
     * return all files
     * @return a list of FTPFile that fit the search
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static List<FTPFile> listFiles(FTPClient client, String host,
            String userName, String password, String workingDirectory,
            List<String> extentions) throws SocketException, IOException {
        List<FTPFile> ret = listFilesWorker(workingDirectory, extentions, client);
        client.quit();
        return ret;
    }

    /**
     * Gets a list of the files in the given directory that match the extensions
     *
     * @param client the ftp client for this connection
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for root directory
     * @return a list of FTPFile that fit the search
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static List<FTPFile> listFiles(FTPClient client, String host,
            String userName, String password, String workingDirectory)
            throws SocketException, IOException {
        List<FTPFile> ret = new ArrayList<>();
        ret = listFilesWorker(workingDirectory, null, client);
        client.quit();
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws Exception {
        this.CloseConnection();
    }

    /**
     * Gets a list of the files in the given directory that match the extensions
     *
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @param extentions a list of extensions not including the ".". Null will
     * return all files
     * @return a list of FTPFile that fit the search
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public List<FTPFile> listFiles(String workingDirectory, List<String> extentions)
            throws SocketException, IOException {
        List<FTPFile> fileList = listFilesWorker(workingDirectory, extentions, client);
        return filterExtensionsWorker(fileList, extentions);
    }

    /**
     * Gets a list of the files in the given directory that match the extensions
     *
     * @param workingDirectory the working directory full path. Separate
     * directories with "\". Use null for current directory.
     * @return a list of FTPFile that fit the search
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public List<FTPFile> listFiles(String workingDirectory) throws SocketException,
            IOException {
        return listFilesWorker(workingDirectory, null, client);
    }

    private static List<FTPFile> listFilesWorker(String workingDirectory,
            List<String> extentions, FTPClient client) throws IOException {
        List<FTPFile> fileList = null;
        List<FTPFile> ret = null;
        client = changeDirectoryWorker(client, workingDirectory);
        fileList = Arrays.asList(client.listFiles());
        ret = filterExtensionsWorker(fileList, extentions);
        return ret;
    }

    private static FTPClient changeDirectoryWorker(
            FTPClient client, String workingDirectory) throws IOException {
        if (workingDirectory != null) {
            client.changeWorkingDirectory("/");
            String[] paths = workingDirectory.split("\\\\");
            System.out.println(client.printWorkingDirectory());

            for (String s : paths) {
                client.changeWorkingDirectory(s);
                System.out.println(client.printWorkingDirectory());
                if (client.printWorkingDirectory().equals("/")) {
                    client.changeWorkingDirectory("home");
                    client.changeWorkingDirectory("reusetek");
                    List<FTPFile> files = Arrays.asList(client.listFiles());
                    //files.forEach(file -> System.out.println(file.getName()));
                    client.changeWorkingDirectory(s);
                    System.out.println(client.printWorkingDirectory());
                }
            }
        }
        return client;
    }

    private static FTPClient getStaticClient(String host, String userName,
            String password) throws SocketException, IOException {
        FTPClient client = new FTPClient();
        logIn(client, host, userName, password);
        return client;
    }

    private static List<FTPFile> filterExtensionsWorker(List<FTPFile> fileList,
            List<String> extensions) {
        List<FTPFile> ret = new ArrayList<>();
        if (extensions != null && !extensions.isEmpty()) {
            for (FTPFile ftpFile : fileList) {
                if (ftpFile.isFile()) {
                    String extensionOfFile = ftpFile
                            .getName()
                            .substring(ftpFile.getName()
                                    .lastIndexOf("."));
                    for (String extension : extensions) {
                        if (extensionOfFile.equalsIgnoreCase(extension)) {
                            ret.add(ftpFile);
                            break;
                        }
                    }
                }
            }
        } else {
            ret = fileList;
        }

        return ret;
    }

    /**
     * Gets the <code>InputStream</code> of the file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param file FTPfile of the the file to download
     * @return the <code>InputStream</code> of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static InputStream getFile(String host, String userName,
            String password, String workingDirectory, FTPFile file)
            throws SocketException, IOException {
        return getFile(host, userName, password, workingDirectory, file.getName());
    }

    /**
     * <p>
     * getFileBytes.
     * </p>
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param file FTPfile of the the file to download
     * @return a byte array of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static byte[] getFileBytes(String host, String userName,
            String password, String workingDirectory, FTPFile file)
            throws SocketException, IOException {
        ByteArrayOutputStream baos;
        byte[] ret;
        try (InputStream in = getFile(host, userName, password, workingDirectory, file)) {
            baos = new ByteArrayOutputStream();
            org.apache.commons.io.IOUtils.copy(in, baos);
            ret = baos.toByteArray();
        }
        baos.close();
        baos = null;
        return ret;
    }

    /**
     * Gets the <code>InputStream</code> of the file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * @param fileName the name of the file to download directories with "\"
     * @return the <code>InputStream</code> of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static byte[] getFileBytes(String host, String userName, String password,
            String workingDirectory, String fileName)
            throws SocketException, IOException {
        ByteArrayOutputStream baos;
        byte[] ret;
        try (InputStream in = getFile(host, userName, password, workingDirectory, fileName)) {
            baos = new ByteArrayOutputStream();
            org.apache.commons.io.IOUtils.copy(in, baos);
            ret = baos.toByteArray();
        }
        baos.close();
        baos = null;
        return ret;
    }

    /**
     * Gets the <code>InputStream</code> of the file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param fileName the name of the file to download
     * @return the <code>InputStream</code> of the file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static InputStream getFile(String host, String userName,
            String password, String workingDirectory, String fileName)
            throws SocketException, IOException {
        FTPClient client = new FTPClient();
        client.connect(host);
        client.enterLocalPassiveMode();
        client.login(userName, password);
        String[] paths = workingDirectory.split("\\\\");
        for (String s : paths) {
            client.changeWorkingDirectory(s);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream in = client.retrieveFileStream(fileName);
        org.apache.commons.io.IOUtils.copy(in, baos);
        closeConnection(client);
        return new ByteArrayInputStream(baos.toByteArray());
    }

    /**
     * Closes the connection to the FTP server
     *
     * @param client the FTP client
     * @throws java.io.IOException if any.
     */
    public static void closeConnection(FTPClient client) throws IOException {
        client.logout();
        client.disconnect();
    }

    /**
     * Downloads the FTPfile to a local file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param ftpFileName the name of the file to download from the server
     * @param fileName the name of the file to save it to
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static void getFile(String host, String userName, String password,
            String workingDirectory, String ftpFileName, String fileName)
            throws SocketException, IOException {

        OutputStream out;
        try (InputStream in = getFile(host, userName, password, workingDirectory, ftpFileName)) {
            out = new FileOutputStream(fileName);
            org.apache.commons.io.IOUtils.copy(in, out);
            out.flush();
            out.close();
        }
        out = null;
    }

    /**
     * Downloads the FTPfile to a local file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param ftpFileName the name of the file to download from the server
     * @param saveFile the file to save it to
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static void getFile(String host, String userName, String password,
            String workingDirectory, String ftpFileName, File saveFile)
            throws SocketException, IOException {
        getFile(host, userName, password, workingDirectory, ftpFileName,
                saveFile.getAbsolutePath());

    }

    /**
     * Downloads the FTPfile to a local file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param ftpFile the FTPFile to download
     * @param saveFile the name of the file to save it to
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static void getFile(String host, String userName, String password,
            String workingDirectory, FTPFile ftpFile, File saveFile)
            throws SocketException, IOException {
        getFile(host, userName, password, workingDirectory, ftpFile.getName(), saveFile);
    }

    /**
     * Downloads the FTPfile to a local file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param ftpFile the FTPFile to download
     * @return the temp file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static File getFileToTempFile(String host, String userName,
            String password, String workingDirectory, FTPFile ftpFile)
            throws SocketException, IOException {
        return getFileToTempFile(host, userName, password, workingDirectory,
                ftpFile.getName());

    }

    /**
     * Downloads the FTPfile to a local file
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @param workingDirectory the working directory full path. Separate
     * directories with "\"
     * @param ftpFileName the FTPFile to download
     * @return the temp file
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static File getFileToTempFile(String host, String userName,
            String password, String workingDirectory, String ftpFileName)
            throws SocketException, IOException {
        String tmpDirName = System.getProperty("java.io.tmpdir");
        File fleTmp = File.createTempFile("ebs", null, new File(tmpDirName));
        fleTmp.deleteOnExit();
        getFile(host, userName, password, workingDirectory, ftpFileName, fleTmp);
        return fleTmp;
    }

    /**
     * Gets A list of <code>FTPFile</code> with the archive file info
     *
     * @param host the host name
     * @param userName the user name
     * @param password the password
     * @return A list of <code>FTPFile</code> with the archive file info
     * @see FTPFile
     * @throws java.net.SocketException if any. @ throws java.io.IOException if
     * any.
     */
    public static List<FTPFile> getArchives(String host, String userName, String password)
            throws SocketException, IOException {
        List<String> ext = new ArrayList<>();
        ext.add("mrc");
        return listFiles(host, userName, password, "ebaysales\\archive", ext);

    }

    private static void closeFTPConnection(FTPClient client) {
        try {
            client.sendCommand(FTPCmd.QUIT);
        } catch (IOException e) {
        }
        client = null;
    }

    private boolean isGood(int code) {
        String tmp = Integer.toString(code);
        tmp = tmp.substring(0, 1);
        int tmpI = Integer.parseInt(tmp);
        return tmpI == 1 || tmpI == 2 || tmpI == 3;
    }

    public boolean logIn(String host, String user, String password)
            throws SocketException, IOException {
        client = new FTPClient();

        logIn(client, HOST, USER_NAME, PASS_WORD);
        return true;
    }

    private static FTPClient logIn(FTPClient client, String host, String user,
            String password) throws SocketException, IOException {
        if (client == null) {
            client = new FTPClient();
        }
        if (client.isConnected() == false) {
            client.connect(host);
            client.setConnectTimeout(30000);
            client.setDefaultTimeout(30000);
            client.connect(host);
            client.setKeepAlive(true);
            client.setControlKeepAliveReplyTimeout(1000);
            client.setControlKeepAliveTimeout(5);
            client.setBufferSize(1024 * 1024);
            client.login(user, password);
        }
        return client;
    }

    /**
     * <p>
     * Getter for the field <code>client</code>.
     * </p>
     *
     * @return a {@link org.apache.commons.net.ftp.FTPClient} object.
     */
    public FTPClient getclient() {
        return client;
    }

    private static boolean isConnectionReset(Exception e) {
        if (!(e instanceof SocketException)) {
            return false;
        }
        return e.getMessage().equalsIgnoreCase("Connection reset");
    }

}
