/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.ftp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.net.ftp.FTPFile;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.StAXEventBuilder;

/**
 *
 * @author Charles Hamilton
 */
public class FTPOrders {

    private final String USER_NAME;
    private final String PASS_WORD;
    private final String HOST;
    private FTPMethods ftp;
    private List<FTPFile> ftpFiles = null;

    public FTPOrders(String userName, String password, String host)
            throws SocketException, IOException {
        this.USER_NAME = userName;
        this.PASS_WORD = password;
        this.HOST = host;
        ftp = new FTPMethods(USER_NAME, PASS_WORD, HOST);
        refreshFTPFiles();
    }

    public Map<String, Document> getDocuments() throws SocketException, IOException {
        Map<String, Document> retMap = null;
        List<FTPFile> files = getFtp().getEbaySales();
        retMap = getDocuments(files);
        closeFTP();
        return retMap;
    }

    public Map<String, Document> getDocuments(Map<String, Document> savedDocuments)
            throws SocketException, IOException {
        if (savedDocuments == null) {
            return getDocuments();
        }

        List<FTPFile> files = getFtp().getEbaySales();
        List<String> newFTPFileNames = getNewFileNames(savedDocuments, files);
        Map<String, Document> ret = getDocuments(fileNamesToFTPFiles(newFTPFileNames));

        Iterator<String> iterator = ret.keySet().iterator();
        String fileName = null;
        while (iterator.hasNext()) {
            fileName = iterator.next();
            savedDocuments.put(fileName, ret.get(fileName));
        }

        return savedDocuments;

    }

    public Map<String, Document> cleanUpOrderMap(Map<String, Document> savedOrders) throws IOException {
        List<FTPFile> files = getFtp().getEbaySales();
        return cleanUpOrderMap(savedOrders, files);
    }

    public void refreshFTPFiles() throws IOException {
        ftpFiles = getFtp().getEbaySales();
    }

    /**
     * @return the ftp
     */
    public FTPMethods getFtp() {
        return ftp;
    }

    /**
     * @param ftp the ftp to set
     */
    public void setFtp(FTPMethods ftp) {
        this.ftp = ftp;
    }

    public Map<String, Document> cleanUpOrderMap(Map<String, Document> savedOrders,
            List<FTPFile> files) {
        final Map<String, Document> retMap = new HashMap<>(300);
        List<String> savedFileNames = new ArrayList<>(savedOrders.keySet());
        List<String> ftpFileNames = getFTPFileNames(files);

        final List<String> currentNames = savedFileNames.stream()
                .filter((fileName) -> ftpFileNames.contains(fileName))
                .collect(Collectors.toList());

        currentNames.forEach((fileName) -> {
            retMap.put(fileName, savedOrders.get(fileName));
        });

        return retMap;
    }

    public Map<String, Document> cleanUpOrderMap(List<String> fileNames, Map<String, Document> savedOrders) {
        List<FTPFile> files = fileNamesToFTPFiles(fileNames);
        return cleanUpOrderMap(savedOrders, files);
    }

    /**
     * @return the ftpFiles
     */
    public List<FTPFile> getFtpFiles() {
        return ftpFiles;
    }

    public long getLastFTPUploadInMili() {
        List<FTPFile> ftpFiles = getFtpFiles();
        List<String> fileNames = getFTPFileNames(ftpFiles);
        final List<Long> fileTimesInMili = new ArrayList<>(ftpFiles.size());

        fileNames.forEach((fileName) -> {
            String tmp = fileName.substring(0, fileName.lastIndexOf("."));
            fileTimesInMili.add(Long.parseLong(tmp));
        });

        fileTimesInMili.sort((Long o1, Long o2) -> {
            return o1.compareTo(o2);
        });

        Long time = fileTimesInMili.get(fileTimesInMili.size() - 1);

        return time;
    }

    public void uploadOrders(String fileName, byte[] data)
            throws SocketException, IOException {
        getFtp().changeDir("ebaysales");
        getFtp().uploadFile(fileName, data);
    }

    private Map<String, Document> getDocuments(List<FTPFile> files)
            throws IOException {
        Map<String, Document> retMap = new HashMap<>(300);
        int counter = 0;

        for (FTPFile file : files) {
            if (counter % 10 == 0) {
                getFtp().CloseConnection();
                getFtp().logIn(HOST, USER_NAME, PASS_WORD);
                getFtp().changeDir("ebaysales");
                System.out.println("Reconnected");
            }
            retMap.put(file.getName(), readFile(file));
            counter++;
            System.out.println("Downloaded " + counter + " of " + files.size());
        }

        return retMap;
    }

    private Document readFile(String fileName) throws IOException {
        FTPFile f = new FTPFile();
        f.setName(fileName);
        return readFile(f);
    }

    private Document readFile(FTPFile file) throws SocketException,
            IOException {
        byte[] buffer = null;
        try {
            buffer = readFileToBuffer(getFtp().getFile(null, file));
            return byteArrayToXMLFile(buffer);
        } catch (SocketException se) {
            if (se.getMessage().equalsIgnoreCase("Connection reset")) {
                try {
                    getFtp().logIn(HOST, USER_NAME, PASS_WORD);
                    getFtp().changeDir("ebaysales");
                    System.out.println("Reconnected");
                    return readFile(file);
                } catch (Exception e) {
                }

            } else {
                throw se;
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return null;
    }

    private List<String> getNewFileNames(Map<String, Document> savedDocuments,
            List<FTPFile> ftpFiles) {
        List<String> savedFileNames = new ArrayList<>(savedDocuments.keySet());
        List<String> newFileNames = null;
        List<String> ftpFileNames = getFTPFileNames(ftpFiles);

        newFileNames = ftpFileNames.stream()
                .filter((fileName) -> !savedFileNames.contains(fileName))
                .collect(Collectors.toList());

        return newFileNames;
    }

    private List<String> getFTPFileNames(List<FTPFile> files) {
        final List<String> ftpFileNames = new ArrayList<>(files.size());
        files.forEach((file) -> {
            ftpFileNames.add(file.getName());
        });

        return ftpFileNames;
    }

    private byte[] readFileToBuffer(InputStream in) {
        try {
            byte[] buffer = new byte[1000];
            int retCount = -1;
            ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
            while ((retCount = in.read(buffer)) > -1) {
                baos.write(buffer, 0, retCount);
            }
            return baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return null;
    }

    private Document byteArrayToXMLFile(byte[] buffer)
            throws XMLStreamException, JDOMException {
        Document doc = null;
        XMLInputFactory factory = XMLInputFactory.newFactory();
        XMLEventReader reader = factory.createXMLEventReader(new ByteArrayInputStream(buffer));
        StAXEventBuilder builder = new StAXEventBuilder();
        doc = builder.build(reader);
        return doc;
    }

    private void closeFTP() {
        if (getFtp() == null) {
            return;
        }
        try {
            getFtp().close();
        } catch (Exception e) {
        }
    }

    private FTPFile fileNameToFTPFile(String FileName) {
        FTPFile retFile = new FTPFile();
        retFile.setName(FileName);
        return retFile;
    }

    private List<FTPFile> fileNamesToFTPFiles(List<String> fileNames) {
        final List<FTPFile> retFTPFiles = new ArrayList<>(fileNames.size());

        fileNames.forEach((file) -> {
            retFTPFiles.add(fileNameToFTPFile(file));
        });

        return retFTPFiles;
    }

}
