/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.utilities;

import java.util.prefs.Preferences;

/**
 * Helper class to store and retrieve saved settings
 * 
 * @author David
 */
public class SaveSettings {

    /**
     * Saves a setting
     * 
     * @param rootNodeName
     *            path of the setting
     * @param nodeName
     *            name of the setting
     * @param nodeValue
     *            value of the setting
     */
    public static void saveSetting(String rootNodeName, String nodeName, String nodeValue) {
	Preferences prefs = Preferences.userRoot().node(rootNodeName);
	prefs.put(nodeName, nodeValue);
    }

    /**
     * Gets a saved setting
     * 
     * @param rootNodeName
     *            the path of the setting
     * @param nodeName
     *            the name of the setting
     * @return the value of the setting
     */
    public static String getSetting(String rootNodeName, String nodeName) {
	Preferences prefs = Preferences.userRoot().node(rootNodeName);
	String ret = prefs.get(nodeName, "");
	return ret;
    }
}
