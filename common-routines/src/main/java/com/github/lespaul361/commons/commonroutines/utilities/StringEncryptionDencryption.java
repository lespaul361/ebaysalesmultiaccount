/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.utilities;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Methods to encrypt and decrypt strings
 */
public class StringEncryptionDencryption {

    private static SecretKeySpec secretKey;
    private static byte[] key;

    /**
     * Sets the encryption key
     * 
     * @param myKey
     *            the encryption key
     */
    public static void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Encrypts the string
     * 
     * @param strToEncrypt
     *            the String to encrypt
     * @param secret
     *            the encryption key
     * @return The encrypted string
     */
    public static String encrypt(String strToEncrypt, String secret) {
	try {
	    setKey(secret);
	    Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	    cipher.init(Cipher.ENCRYPT_MODE, secretKey);
	    return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
	} catch (Exception e) {
	    System.out.println("Error while encrypting: " + e.toString());
	}
	return null;
    }

    /**
     * Decrypts the string
     * 
     * @param strToDecrypt
     *            the String to decrypt
     * @param secret
     *            the encryption key
     * @return The decrypted string
     */
    public static String decrypt(String strToDecrypt, String secret) {
	try {
	    setKey(secret);
	    Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
	    cipher.init(Cipher.DECRYPT_MODE, secretKey);
	    return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
	} catch (Exception e) {
	    System.out.println("Error while decrypting: " + e.toString());
	}
	return null;
    }
}
