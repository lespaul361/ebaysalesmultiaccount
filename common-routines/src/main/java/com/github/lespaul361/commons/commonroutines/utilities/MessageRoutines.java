/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.utilities;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Helper class for simple message routines
 * 
 * @author Charles Hamilton
 */
public class MessageRoutines {

    /**
     * Shows a message box with 1 button that says OK
     * 
     * @param message
     *            the message to show the user
     * @param title
     *            the title of the message box
     * @param frame
     *            the parent frame
     */
    public static void showOkOnly(String message, String title, JFrame frame) {
	Object[] options = { "   OK   " };
	int n = JOptionPane.showOptionDialog(frame, message, title, JOptionPane.PLAIN_MESSAGE,
		JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
    }

    /**
     * Shows a message box with 1 button that says OK
     * 
     * @param message
     *            the message to show the user
     * @param title
     *            the title of the message box
     * @param messageType
     *            determines the icon to be shown in the message box
     * @param frame
     *            the parent frame
     */
    public static void showOkOnly(String message, String title, int messageType, JFrame frame) {
	Object[] options = { "   OK   " };
	int n = JOptionPane.showOptionDialog(frame, message, title, JOptionPane.PLAIN_MESSAGE, messageType, null,
		options, options[0]);
    }
}
