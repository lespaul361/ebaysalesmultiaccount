/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.email;

/**
 * 
 * @author David
 */
public class SenderInfo {

    private String senderAddress;
    private String senderAuthenticationID;
    private String senderAuthenticationPassword;

    private int serverEmailPort;
    private String SMTPHostName;
    private String hostName;

    /**
     * Constructs an empty <code>SenderInfo</code> class
     */
    public SenderInfo() {
    }

    /**
     * Constructs an new <code>SenderInfo</code> class
     * 
     * @param senderAddress
     *            the email address of the sender
     * @param senderAuthenticationID
     *            the login id of the sender
     * @param senderAuthenticationPassword
     *            the login password of the sender
     * @param SMTPHostName
     *            the server address
     * @param port
     *            the port used to send email
     */
    public SenderInfo(String senderAddress, String senderAuthenticationID, String senderAuthenticationPassword,
	    String SMTPHostName, int port) {
	this.senderAddress = senderAddress;
	this.senderAuthenticationID = senderAuthenticationID;
	this.senderAuthenticationPassword = senderAuthenticationPassword;
	this.SMTPHostName = SMTPHostName;
	this.serverEmailPort = port;
    }

    /**
     * @return the senderAddress
     */
    public String getSenderAddress() {
	return senderAddress;
    }

    /**
     * @param senderAddress
     *            the senderAddress to set
     */
    public void setSenderAddress(String senderAddress) {
	this.senderAddress = senderAddress;
    }

    /**
     * @return the senderAuthenticationID
     */
    public String getSenderAuthenticationID() {
	return senderAuthenticationID;
    }

    /**
     * @param senderAuthenticationID
     *            the senderAuthenticationID to set
     */
    public void setSenderAuthenticationID(String senderAuthenticationID) {
	this.senderAuthenticationID = senderAuthenticationID;
    }

    /**
     * @return the senderAuthenticationPassword
     */
    public String getSenderAuthenticationPassword() {
	return senderAuthenticationPassword;
    }

    /**
     * @param senderAuthenticationPassword
     *            the senderAuthenticationPassword to set
     */
    public void setSenderAuthenticationPassword(String senderAuthenticationPassword) {
	this.senderAuthenticationPassword = senderAuthenticationPassword;
    }

    /**
     * @return the serverEmailPort
     */
    public int getServerEmailPort() {
	return serverEmailPort;
    }

    /**
     * @param serverEmailPort
     *            the serverEmailPort to set
     */
    public void setServerEmailPort(int serverEmailPort) {
	this.serverEmailPort = serverEmailPort;
    }

    /**
     * @return the SMTPHostName
     */
    public String getSMTPHostName() {
	return SMTPHostName;
    }

    /**
     * @param SMTPHostName
     *            the SMTPHostName to set
     */
    public void setSMTPHostName(String SMTPHostName) {
	this.SMTPHostName = SMTPHostName;
    }

    /**
     * @return the hostName
     */
    public String getHostName() {
	return hostName;
    }

    /**
     * @param hostName
     *            the hostName to set
     */
    public void setHostName(String hostName) {
	this.hostName = hostName;
    }
}
