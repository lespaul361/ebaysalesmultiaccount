/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.utilities;

import java.util.Comparator;

/**
 * 
 * @author Charles Hamilton
 */
public class DefaultComparators {

    /**
     * Gets a <code>String</code> <code>Comparator</code>
     * 
     * @param isCaseSensative
     *            if case is used to determine sorting
     * @return a <code>String</code> <code>Comparator</code>
     */
    public static Comparator getDefaultStringComparator(boolean isCaseSensative) {
        class c implements Comparator<String> {

            private final boolean checkCase;

            public c(boolean caseSensative) {
                checkCase = caseSensative;
            }

            @Override
            public int compare(String obj1, String obj2) {
                if (obj1 == null) {
                    return -1;
                }
                if (obj2 == null) {
                    return 1;
                }
                if (!checkCase) {
                    return obj1.compareTo(obj2);
                }
                return obj1.compareToIgnoreCase(obj2);
            }

        }
        return new c(isCaseSensative);
    }

    /**
     * Gets a <code>double</code> <code>Comparator</code>
     * 
     * @return a <code>double</code> <code>Comparator</code>
     */
    public static Comparator getDefaultDoubleComparator() {
        class DoubleComparator implements Comparator<Double> {

            @Override
            public int compare(Double d1, Double d2) {
                return Double.compare(d1, d2);
            }

        }
        return new DoubleComparator();
    }

    /**
     * Gets a <code>int</code> <code>Comparator</code>
     * 
     * @return a <code>int</code> <code>Comparator</code>
     */
    public static Comparator getDefaultIntegerComparator() {
        class IntegerComparator implements Comparator<Integer> {

            @Override
            public int compare(Integer i1, Integer i2) {
                return Integer.compare(i1, i2);
            }

        }
        return new IntegerComparator();
    }
}
