/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.utilities;

import java.io.Serializable;

/**
 *
 * @author David Hamilton
 * @param <T> the data type this class uses
 */
public class NameValuePair<T> implements Serializable {

    private String name = "";
    private T value = null;

    /**
     * Constructs an empty <code>NameValuePair</code>
     */
    public NameValuePair() {
        this(null, null);
    }

    /**
     * Constructs a <code>NameValuePair</code> with a name
     *
     * @param name the name of the pair
     */
    public NameValuePair(String name) {
        this(name, null);
    }

    /**
     * Constructs a <code>NameValuePair</code> with name and value
     *
     * @param name the name of the pair
     * @param value the value of the pair
     */
    public NameValuePair(String name, T value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Gets the name of the <code>NameValuePair</code>
     *
     * @return the name the name of the <code>NameValuePair</code>
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the <code>NameValuePair</code>
     *
     * @param name the name of the <code>NameValuePair</code>
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of the <code>NameValuePair</code>
     *
     * @return the value the value of the <code>NameValuePair</code>
     */
    public T getValue() {
        return value;
    }

    /**
     * Sets the value of the <code>NameValuePair</code>
     *
     * @param value the value of the <code>NameValuePair</code>
     */
    public void setValue(T value) {
        this.value = value;
    }

}
