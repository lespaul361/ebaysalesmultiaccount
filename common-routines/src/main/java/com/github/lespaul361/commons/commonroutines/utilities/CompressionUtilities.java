/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.commons.commonroutines.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

/**
 * Utilities to help with Compressing data
 */
public class CompressionUtilities {

    /**
     * Compresses an Object
     * 
     * @param obj
     *            the {@link List} to compress
     * @return A {@link byte} array
     */
    public static byte[] deflateList(List obj) {
	return deflateList(obj, Deflater.DEFAULT_COMPRESSION);
    }

    /**
     * Compresses data to a {@link byte} array
     * 
     * @param obj
     *            {@link List} to compress
     * @param level
     *            level of compression. 0 is none at 9 is maximum
     * @return the {@link byte} array
     */
    public static byte[] deflateList(List obj, int level) {
	if (level < 0 || level > 9) {
	    new RuntimeException("Level needs to be between 0 and 9");
	}
	try {
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    ObjectOutputStream oos = new ObjectOutputStream(bos);
	    oos.writeObject(obj);
	    byte[] data = bos.toByteArray();
	    Deflater deflater = new Deflater(level);
	    deflater.setInput(data);
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
	    deflater.finish();
	    byte[] buffer = new byte[1024];
	    while (!deflater.finished()) {
		int count = deflater.deflate(buffer); // returns the generated
		// code... index
		outputStream.write(buffer, 0, count);
	    }
	    byte[] output = outputStream.toByteArray();
	    outputStream.close();
	    oos.close();
	    bos.close();
	    // Object test=inflateList(output);
	    return output;
	} catch (Exception e) {
	}
	return null;
    }

    /**
     * Decompresses a {@link List}
     * 
     * @param data
     *            the compressed data of the {@link List}
     * @return a {@link List}
     */
    public static List inflateList(byte[] data) {
	try {
	    byte[] buffer = inflateStream(data);

	    ByteArrayInputStream bis = new ByteArrayInputStream(buffer);
	    ObjectInputStream objectInputStream = new ObjectInputStream(bis);
	    Object ret = objectInputStream.readObject();
	    bis.close();
	    objectInputStream.close();
	    return (List) ret;

	} catch (Exception e) {
	    e.printStackTrace(System.out);
	}
	return null;
    }

    /**
     * Decompresses a {byte array
     * 
     * @param data
     *            the byte array
     * @return decompressed byte array
     */
    public static byte[] inflateStream(byte[] data) {
	try {
	    Inflater inflater = new Inflater();
	    inflater.setInput(data);
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
	    byte[] buffer = new byte[1024];
	    while (!inflater.finished()) {
		int count = inflater.inflate(buffer);
		outputStream.write(buffer, 0, count);
		if (count == 0) {
		    break;
		}
	    }
	    outputStream.close();
	    byte[] output = outputStream.toByteArray();
	    return output;
	} catch (Exception e) {
	}
	return null;
    }

    /**
     * Compresses data to a {@link byte} array
     * 
     * @param stream
     *            the data array
     * @param level
     *            level of compression. 0 is none at 9 is maximum
     * @return the {@link byte} array
     */
    public static byte[] deflateStream(byte[] stream, int level) {
	Deflater deflater = new Deflater(level);
	deflater.setInput(stream);
	ByteArrayOutputStream outputStream = new ByteArrayOutputStream(stream.length);
	deflater.finish();
	byte[] buffer = new byte[1024];
	while (!deflater.finished()) {
	    int count = deflater.deflate(buffer); // returns the generated
	    // code... index
	    outputStream.write(buffer, 0, count);
	}
	byte[] output = outputStream.toByteArray();
	return output;
    }

    /**
     * Compresses data to a {@link byte} array
     * 
     * @param stream
     *            the data array
     * @return the {@link byte} array
     */
    public static byte[] deflateStream(byte[] stream) {
	return deflateStream(stream, Deflater.DEFAULT_COMPRESSION);
    }
}
