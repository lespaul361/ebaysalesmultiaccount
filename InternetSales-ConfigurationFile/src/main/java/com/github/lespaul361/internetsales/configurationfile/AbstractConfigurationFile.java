/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Charles Hamilton
 */
public abstract class AbstractConfigurationFile extends AbstractLogError
        implements ConfigurationFile {

    protected ConfigurationFileInfo configurationFileInfo;

    /**
     * Constructor for the abstract class
     *
     * @param logger the logger
     */
    public AbstractConfigurationFile(Logger logger) {
        super(logger);
    }

    /**
     * Constructor for the abstract class and reads in the configuration
     * information
     *
     * @param logger the logger
     * @param file the file for the configuration information
     */
    public AbstractConfigurationFile(Logger logger, File file) {
        this(logger);
        readFile(file);
    }

    @Override
    public ConfigurationFileInfo getConfigurationFileInfo() {
        return this.configurationFileInfo;
    }

    @Override
    public void setConfigurationFileInfo(ConfigurationFileInfo info) {
        this.configurationFileInfo = info;
    }

    @Override
    public void saveConfigurationFile(File file) throws FileNotFoundException {
        FileOutputStream fos = new FileOutputStream(file);
        try {
            saveConfigurationFile(fos);
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        } finally {
            try {
                fos.flush();
                fos.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void saveConfigurationFile(OutputStream stream) {
        if (this.configurationFileInfo == null) {
            throw new NullPointerException("ConfigurationFileInfo is null");
        }
        DataOutputStream dos = new DataOutputStream(stream);
        this.write(dos);
        try {
            dos.flush();
            dos.close();
        } catch (Exception e) {
            logError(Level.WARNING, e.getMessage(), e);
        }
        try {
            stream.flush();
            stream.close();
        } catch (Exception e) {
            logError(Level.WARNING, e.getMessage(), e);
        }

    }

    @Override
    public void readConfigurationFile(File file) throws FileNotFoundException {
        try {
            FileInputStream fis = new FileInputStream(file);
            DataInputStream dis = new DataInputStream(fis);
            readConfigurationFile(dis);
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void readConfigurationFile(InputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        DataInputStream dis = new DataInputStream(stream);
        this.configurationFileInfo = (ConfigurationFileInfo) this.read(dis);
        try {
            dis.close();
        } catch (Exception e) {
        }

    }

    protected void readFile(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            DataInputStream dis = new DataInputStream(fis);
            this.configurationFileInfo = (ConfigurationFileInfo) read(dis);
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        }
    }    
    
}
