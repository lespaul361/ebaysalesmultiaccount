/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.util.logging.Logger;

/**
 * Abstract class to handle EbayApplication Info
 */
public abstract class AbstractEbayApplicationInfo extends AbstractLogError
        implements EbayApplicationInfo, CustomReadWrite {

    protected String certificateId = null;
    protected String appId = null;
    protected String developerName = null;
    protected String ruName = null;

    public AbstractEbayApplicationInfo(Logger logger) {
        super(logger);
    }

    @Override
    public String getCertificateId() {
        return certificateId;
    }

    @Override
    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    @Override
    public String getAppId() {
        return appId;
    }

    @Override
    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Override
    public String getDeveloperName() {
        return developerName;
    }

    @Override
    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    @Override
    public String getRuName() {
        return ruName;
    }

    @Override
    public void setRuName(String ruName) {
        this.ruName = ruName;
    }

    @Override
    public String toString() {
        return "AbstractEbayApplicationInfo{" + "certificateId=" + certificateId
                + ", appId=" + appId + ", developerName=" + developerName
                + ", ruName=" + ruName + '}';
    }

}
