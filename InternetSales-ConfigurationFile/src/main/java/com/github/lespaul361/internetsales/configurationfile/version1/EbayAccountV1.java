/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AbstractEbayAccount;
import com.github.lespaul361.internetsales.configurationfile.Account;
import com.github.lespaul361.internetsales.configurationfile.AccountTypes;
import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * Holds information for an Ebayaccount
 * </p>
 * The data is saved as:<br>
 * <table summary="The data is saved as:">
 * <tr><th>Byte</th><th>Value</th></tr>
 * <tr><td>0</td><td>Type of account</td></tr>
 * <tr><td>1-4</td><td>Length of account name</td></tr>
 * <tr><td>5-length of account name</td><td>Account name string</td></tr>
 * <tr><td>last + 4</td><td>Length of token</td></tr>
 * <tr><td>last + length</td><td>Token string</td></tr>
 * </table>
 *
 */
public class EbayAccountV1 extends AbstractEbayAccount implements EbayAccount {

    private static final long serialVersionUID = 782913647581571165L;

    protected String accountName;
    protected String securityToken;
    transient private final byte Account_Type = AccountTypes.EBAY;

    public EbayAccountV1(Logger logger) {
        super(logger);
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    EbayAccountV1(String accountName, String securityToken, Logger logger) {
        this(logger);
        setAccountName(accountName);
        setSecurityToken(securityToken);
    }

    @Override
    public String getAccountName() {
        return accountName;
    }

    @Override
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Override
    public String getSecurityToken() {
        return securityToken;
    }

    @Override
    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.accountName);
        hash = 59 * hash + Objects.hashCode(this.securityToken);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EbayAccountV1 other = (EbayAccountV1) obj;
        if (!Objects.equals(this.accountName, other.accountName)) {
            return false;
        }
        if (!Objects.equals(this.securityToken, other.securityToken)) {
            return false;
        }
        return true;
    }

    @Override
    public Object read(DataInputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            int len;
            byte type = (byte) stream.read();
            if (this.Account_Type != type) {
                throw new RuntimeException("File is corrupt");
            }
            len = stream.readInt();
            byte[] buffer = new byte[len];
            stream.read(buffer);
            this.accountName = new String(buffer, "UTF-16");
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            this.securityToken = new String(buffer, "UTF-8");
            return this;
        } catch (Exception e) {
            throw new RuntimeException("Could not read from stream");
        }
    }

    @Override
    public void write(DataOutputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            byte[] buffer = new byte[1];
            buffer[0] = AccountTypes.EBAY;
            stream.write(buffer);
            buffer = this.accountName.getBytes("UTF-16");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            buffer = this.securityToken.getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return "EbayAccountV1{" + "accountName=" + accountName + ", securityToken=" + securityToken + '}';
    }

    public EbayAccount copy() {
        return new EbayAccountV1(accountName, securityToken, getLogger());
    }

    protected boolean equalsObject(EbayAccount object) {
        return equalsObject(object, false);
    }

    protected boolean equalsObject(EbayAccount object, boolean ignoreStringCase) {
        try {
            if (ignoreStringCase) {
                if (!this.accountName.equalsIgnoreCase(object.getAccountName())) {
                    return false;
                }
                if (!this.securityToken.equalsIgnoreCase(object.getSecurityToken())) {
                    return false;
                }

                return true;
            }

            if (!this.accountName.equals(object.getAccountName())) {
                return false;
            }
            if (!this.securityToken.equals(object.getSecurityToken())) {
                return false;
            }

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean equalsObject(Account object) {
        return equalsObject(object, false);
    }

    @Override
    public boolean equalsObject(Account object, boolean ignoreStringCase) {
        if (!(object instanceof EbayAccount)) {
            return false;
        } else {
            return equalsObject((EbayAccount) this, ignoreStringCase);
        }
    }

}
