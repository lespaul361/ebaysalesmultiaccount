/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.time.LocalTime;

/**
 * An interface for getting the run information for the application
 */
public interface ApplicationRunInfo extends CustomReadWrite,
        Copy<ApplicationRunInfo>, Equals<ApplicationRunInfo> {

    /**
     * Get sales that are paid
     */
    public static final int COMPLETED_TYPE_PAID = 0;

    /**
     * Get sales that are shipped
     */
    public static final int COMPLETED_TYPE_SHIPPED = 1;

    /**
     * Gets the start time
     *
     * @return a LocalTime with the start time
     */
    LocalTime getStartTime();

    /**
     * Sets the start time
     *
     * @param time a LocaTime with the start time
     */
    void setStartTime(LocalTime time);

    /**
     * Gets the end time
     *
     * @return a LocalTime with the end time
     */
    LocalTime getTimeEnd();

    /**
     * Gets the end time
     *
     * @param time a LocaTime with the end time
     */
    void setEndTime(LocalTime time);

    /**
     * Gets the interval time for checking orders
     *
     * @return an int with the interval time for checking orders
     */
    int getIntervalTime();

    /**
     * Gets the interval time for checking orders
     *
     * @param value an int with the interval time for checking orders
     */
    void setIntervalTime(int value);

    void setSalesFetchType(int salesFetchType);

    int getSalesFetchType();
}
