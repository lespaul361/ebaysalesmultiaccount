/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AbstractConfigurationFileInfo;
import com.github.lespaul361.internetsales.configurationfile.AdminOverride;
import com.github.lespaul361.internetsales.configurationfile.AmazonAccount;
import com.github.lespaul361.internetsales.configurationfile.ApplicationRunInfo;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFileInfo;
import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo;
import com.github.lespaul361.internetsales.configurationfile.FTPInfo;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * The data for the accounts and connection to the FTP server<p>
 * The data is saved in the order of:<br>
 * Version (the first byte)<br>
 * FTPInfo<br>
 * number of Ebay accounts (as 1 byte)<br>
 * Ebay accounts[n]<br>
 * number of Amazon accounts (as 1 byte)<br>
 * Amazon accounts[n]<br>
 * EbayApplication Info
 * </p>
 *
 */
public class ConfigurationFileInfoV1 extends AbstractConfigurationFileInfo {

    public ConfigurationFileInfoV1(Logger logger) {
        super(logger);
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    ConfigurationFileInfoV1(FTPInfo ftpInfo, List<EbayAccount> ebayAcccounts,
            List<AmazonAccount> amazonAccounts, EbayApplicationInfo ebayApplicationInfo,
            List<AdminOverride> admins, Logger logger) {
        this(logger);
        setAmazonAccounts(amazonAccounts);
        setEbayAccounts(ebayAcccounts);
        setEbayApplicationInfo(ebayApplicationInfo);
        setFTPInfo(ftpInfo);
        setAdminOverrides(admins);
    }

    @Override
    public Object read(DataInputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            byte len = 0;
            this.ebayAcccounts = new ArrayList<>(5);
            this.amazonAccounts = new ArrayList<>(5);
            this.ftpInfo = new FTPInfoV1(super.getLogger());
            this.ebayApplicationInfo = new EbayApplicationInfoV1(super.getLogger());
            this.applicationRunInfo = new ApplicationRunInfoV1();

            this.ftpInfo.read(stream);
            len = stream.readByte();//number of Ebayaccounts
            for (int i = 0; i < len; i++) {
                EbayAccount account = new EbayAccountV1(super.getLogger());
                account.read(stream);
                this.ebayAcccounts.add(account);
            }
            len = stream.readByte();//number of amazon accounts
            for (int i = 0; i < len; i++) {
                AmazonAccount account = new AmazonAccountV1(super.getLogger());
                account.read(stream);
                this.amazonAccounts.add(account);
            }
            this.ebayApplicationInfo = (EbayApplicationInfo) this.ebayApplicationInfo.read(stream);

            try {
                this.applicationRunInfo = (ApplicationRunInfo) this.applicationRunInfo.read(stream);
            } catch (Exception e) {
            }

            len = stream.readByte();
            List<AdminOverride> admins = new ArrayList<>(len);
            for (int i = 0; i < len; i++) {
                AdminOverride admin = new AdminOverrideV1(getLogger());
                admin.read(stream);
                admins.add(admin);
            }
            this.admins = admins;

            return this;
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void write(DataOutputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }

        try {
            this.ftpInfo.write(stream);
            byte len = (byte) (this.ebayAcccounts == null ? 0 : this.ebayAcccounts.size());
            stream.writeByte(len);
            for (int i = 0; i < len; i++) {
                this.ebayAcccounts.get(i).write(stream);
            }
            len = (byte) (this.amazonAccounts == null ? 0 : this.amazonAccounts.size());
            stream.writeByte(len);
            for (int i = 0; i < len; i++) {
                this.amazonAccounts.get(i).write(stream);
            }
            this.ebayApplicationInfo.write(stream);
            this.applicationRunInfo.write(stream);
            stream.write((byte) admins.size());
            for (int i = 0; i < admins.size(); i++) {
                admins.get(i).write(stream);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public String toString() {
        return "ConfigurationFileInfoV1{" + "FTPs: " + (ftpInfo == null ? 0 : 1)
                + " EbayAccounts: " + (this.ebayAcccounts == null ? 0 : this.ebayAcccounts.size())
                + " Amazon Accounts: " + (this.amazonAccounts == null ? 0 : this.amazonAccounts.size())
                + "}";
    }

    @Override
    public ConfigurationFileInfo copy() {
        List<EbayAccount> ea;
        if (ebayAcccounts == null) {
            ea = new ArrayList<>(0);
        } else {
            ea = new ArrayList<>(ebayAcccounts.size());
            for (int i = 0; i < ebayAcccounts.size(); i++) {
                ea.add(ebayAcccounts.get(i).copy());
            }
        }

        List<AmazonAccount> aa;
        if (amazonAccounts == null) {
            aa = new ArrayList<>(0);
        } else {
            aa = new ArrayList<>(amazonAccounts.size());
            for (int i = 0; i < amazonAccounts.size(); i++) {
                aa.add(amazonAccounts.get(i).copy());
            }
        }

        List<AdminOverride> admins;
        if (this.admins == null) {
            admins = new ArrayList<>(0);
        } else {
            admins = new ArrayList<>(this.admins.size());
            for (int i = 0; i < this.admins.size(); i++) {
                admins.add(this.admins.get(i).copy());
            }
        }
        return new ConfigurationFileInfoV1(ftpInfo.copy(), ea, aa,
                ebayApplicationInfo.copy(), admins, getLogger());
    }

    @Override
    public boolean equalsObject(ConfigurationFileInfo object) {
        return equalsObject(object, false);
    }

    @Override
    public boolean equalsObject(ConfigurationFileInfo object, boolean ignoreStringCase) {
        return super.equalsObject(object, ignoreStringCase);
    }
}
