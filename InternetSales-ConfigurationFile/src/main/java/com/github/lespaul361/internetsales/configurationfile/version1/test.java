/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AdminOverride;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFile;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFileInfo;
import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo;
import com.github.lespaul361.internetsales.configurationfile.FTPInfo;
import java.io.File;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Charles Hamilton
 */
public class test {

    public static void main(String[] args) {
        ConfigurationFileInfo info = new ConfigurationFileInfoV1(null);
        FTPInfo ftp = new FTPInfoV1(null);
        ftp.setFTPIP("35.165.178.214");
        ftp.setFTPPassword("AN8l4vIDt3h6");
        ftp.setFTPUserName("reusetek");
        info.setFTPInfo(ftp);
        EbayAccount ebayAccount = new EbayAccountV1(null);
        ebayAccount.setAccountName("frankiesfastship");
        ebayAccount.setSecurityToken("AgAAAA**AQAAAA**aAAAAA**wD7fXA**nY+sHZ2Pr"
                + "Bmdj6wVnY+sEZ2PrA2dj6AHkIKpCpaCpwWdj6x9nY+seQ**C88CAA**AAMAAA"
                + "**yuZZPFEW/ve5BNxKkLbx7JK3l7E5RAjzquLoH2u+ovx1iUVRdXfM7NbpTYK"
                + "Wyc7x4IQL3A6j8uL21Ie+ZBXULV+txVEsZG9a57+mMkVcjNIlSXf5QP06YlOw"
                + "Gn66E5txlU2PBlhprjgjBqvuxxI4JiL+SYaKU8GHeqkPIBLmf3UkAM8S4EYy5"
                + "ktqU8F2gYjJQf/XZG5LZVFZEH7dbHbHU79b4aiie5DfSavfLka+XQtiXznz33"
                + "xuAEjfONXxpOPQvtpv/YvfFaNVfF/Bcy4OZDI/gnyn9ChmPkpxVeT7hv5RKq9"
                + "sZzWsduqreg1E9d2YqSPHNO+BYOxgeiUzTrLlH2xp14p8Tn1Q/hfjXWslDgYT"
                + "h+3pSyOQObTP4xhwBvVq8gUQh7Guf6DthrqrpO5UtTvfq7Xt0Jwrw12BzjiHx"
                + "+5esYv8b5wifBvQglDG+nz3R4MQQYTgmex44BKKZV/IqiA71N76I3eTVMZiB+"
                + "cQgep/52TY4dC1lpCwcr8I4rK6tnUv/RRZWx9rsOcPLBU/h+aTRupRtr2uf+A"
                + "pEBJOCb5X5Afm4JEIQpvmxBZoq9PLvz8Wr58NLT77w0lP1xWWElf/5tLpW74N"
                + "AluIBKcVHmrsc1QODT4385jc07he0J4b+sWUYeU9nge2w7DC2Px5ugz1OWUNw"
                + "heOYgw2FAAY0vsEiCC3G16iTd/EXdkDWuEAliAXbkXP58N3G4lOXSn8/g0gXB"
                + "015tlrdaxUTnY1N94lyxdf8z0dZRZYS5p4FaFt");
        info.addEbayAccount(ebayAccount);
        ebayAccount = new EbayAccountV1(null);
        ebayAccount.setAccountName("Affordable_solutions");
        ebayAccount.setSecurityToken("AgAAAA**AQAAAA**aAAAAA**tdXcXA**nY+sHZ2Pr"
                + "Bmdj6wVnY+sEZ2PrA2dj6AClYWpCJKGqQ6dj6x9nY+seQ**C88CAA**AAMAAA"
                + "**2BylQSE1wiemTj7dwUtfl8M/GWnNTVoCeqzvlpNFrEN5Z2hi1qZIaap2+Vr"
                + "Lguiju+MbZmbmtj9NePomSWmuCIyFWakmJOIyJy1KmuOs187xCKFt0NmNNpHz"
                + "4S0F09oPaDk++e0ms4GuIeXqqx7CljG0pQz+jFN1GHEqWm0xeSO4QYZjSs0LN"
                + "LaNeOTDbLFR17s6RWd1lV4EHI1UjuT2Q4TvA/zxN8KQ4P/Jx55fnOhssPtZtj"
                + "NSI1DtGZDDqp2VDgkNXf1YrQBXG8Og/t/cGLTKHM2l5ogdF+Hi39HI3hsMRyx"
                + "tbhTZ5E/uJbEcEpmxaUYsr8fZSPuRqmor30loQhKuT40gEKCnSjFEeCzYccXR"
                + "2vs6ycrVFFvX8kMwX9m/JEAwJnKxwFlLB5tQFXTo2J+iT5GzmZU4BUrbZUi1j"
                + "H0fcjx68l7mD8obsKPqJo9ca2+cU1SI7JoowhSpOgUempURinVQTLNPNKA1Lx"
                + "inWEN3msNcLWXmkUi5N3BuKZyoQ6BbsCNHJM+90zQ9b5pf2O2oVLNeS24SotG"
                + "onu/R6aztQad/epLPvQCWNpq4eMdCxg5dp5s58ivg9b6UQ39W5ARK3SvTfEaX"
                + "9EkEoTKI4C44v5FChg1YdheXVZdTX/AR5FFXMl+VXoOV+8WeU3S5vlgcm1cnx"
                + "CE5a2BqUvQaZFCL3/UBQlos2ezHUR441w+Uj7Vfd2oRDjEnDMZTVjgz6KLyxn"
                + "VeOIUT5dJuyuNSaPiur8FX9k4bGL8lVhP9lnBx");
        info.addEbayAccount(ebayAccount);
        EbayApplicationInfo appInfo = new EbayApplicationInfoV1(null);
        appInfo.setAppId("CharlesH-ade2-4e54-9988-b3668b47c768");
        appInfo.setCertificateId("8fee490d-230d-47c5-8fb9-4ddafdc98d78");
        appInfo.setDeveloperName("6f9f74b7-aec8-4601-af29-4c21204f4f91");
        appInfo.setRuName("Charles_Hamilto-CharlesH-ade2-4-jvbpr");
        info.setEbayApplicationInfo(appInfo);
        AdminOverride admin = new AdminOverrideV1(null);
        admin.setAdminName("Lespaul36");
        admin.setAdminPassword("Noregrets6614?1");
        admin.setVisible(false);
        List<AdminOverride> admins = new ArrayList<>();
        admins.add(admin);
        info.setAdminOverrides(admins);
        ApplicationRunInfoV1 runInfo=new ApplicationRunInfoV1();
        runInfo.setIntervalTime(30);
        runInfo.setEndTime(LocalTime.of(18, 0));
        runInfo.setStartTime(LocalTime.of(6, 0));
        runInfo.setSalesFetchType(ApplicationRunInfoV1.COMPLETED_TYPE_PAID);
        info.setApplicationRunInfo(runInfo);
        ConfigurationFile configFile = new ConfigurationFileV1(null);
        configFile.setConfigurationFileInfo(info);
        
        
        configFile.getConfigurationFileInfo().setEbayApplicationInfo(appInfo);
        configFile.getConfigurationFileInfo().setAdminOverrides(admins);
        try {
            configFile.saveConfigurationFile(new File("config.dat"));
        } catch (Exception e) {
        }
        try {
            configFile = new ConfigurationFileV1(null);
            configFile.readConfigurationFile(new File("config.dat"));
        } catch (Exception e) {
        }
    }
}
