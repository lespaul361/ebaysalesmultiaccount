/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.beans.PropertyChangeSupport;
import java.util.logging.Logger;

/**
 *
 * @author David Hamilton
 */
public abstract class AbstractAdminOverride extends AbstractLogError implements AdminOverride {

    private String adminName;
    private String adminPassword;
    private boolean visible;
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    public static final String PROP_ADMINNAME = "adminName";
    public static final String PROP_ADMINPASSWORD = "adminPassword";
    public static final String PROP_VISIBLE = "visible";

    public AbstractAdminOverride(Logger logger) {
        super(logger);
    }
    
    @Override
    public String getAdminName() {
        return adminName;
    }

    @Override
    public void setAdminName(String adminName) {
        java.lang.String oldAdminName = this.adminName;
        this.adminName = adminName;
        propertyChangeSupport.firePropertyChange(PROP_ADMINNAME, oldAdminName, adminName);
    }

    @Override
    public String getAdminPassword() {
        return adminPassword;
    }

    @Override
    public void setAdminPassword(String adminPassword) {
        java.lang.String oldAdminPassword = this.adminPassword;
        this.adminPassword = adminPassword;
        propertyChangeSupport.firePropertyChange(PROP_ADMINPASSWORD, oldAdminPassword, adminPassword);
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public void setVisible(boolean visible) {
        boolean oldVisible = this.visible;
        this.visible = visible;
        propertyChangeSupport.firePropertyChange(PROP_VISIBLE, oldVisible, visible);
    }


}
