/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

/**
 *
 * @author Charles Hamilton
 * <table summary="The data is saved as:">
 * <tr>
 * <th>Bytes</th>
 * <th>Value</th>
 * </tr>
 * <tr>
 * <td>0</td>
 * <td>Account Type</td>
 * </tr>
 * <tr>
 * <td>1-5</td>
 * <td>Length of the account name</td>
 * </tr>
 * <tr>
 * <td>5-n</td>
 * <td>The account name in UTF-16</td>
 * </tr>
 * <tr>
 * <td>n+1-n+5</td>
 * <td>The length of the key</td>
 * </tr>
 * <tr>
 * <td>last n-length of key</td>
 * <td>The key</td>
 * </tr>
 * </table>
 */
public interface EbayAccount extends CustomReadWrite, Account, Copy<EbayAccount> {

    /**
     * Gets the account name
     *
     * @return the account name
     */
    String getAccountName();

    /**
     * Sets the account name
     *
     * @param accountName the accountName to set
     */
    void setAccountName(String accountName);

    /**
     * gets the security token
     *
     * @return the securityToken
     */
    String getSecurityToken();

    /**
     * Sets the security token
     *
     * @param securityToken the securityToken to set
     */
    void setSecurityToken(String securityToken);

}
