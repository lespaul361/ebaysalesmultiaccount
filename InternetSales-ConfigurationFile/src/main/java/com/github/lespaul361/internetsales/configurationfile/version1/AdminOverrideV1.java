/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AbstractAdminOverride;
import com.github.lespaul361.internetsales.configurationfile.AdminOverride;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David Hamilton
 */
public class AdminOverrideV1 extends AbstractAdminOverride {

    public AdminOverrideV1(Logger logger) {
        super(logger);
    }

    @Override
    public void write(DataOutputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }

        try {
            byte[] buffer = null;
            buffer = getAdminName().getBytes("UTF-16");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            buffer = getAdminPassword().getBytes("UTF-16");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            stream.writeBoolean(isVisible());
        } catch (Exception e) {
        }

    }

    @Override
    public Object read(DataInputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            int len = stream.readInt();
            byte[] buffer = new byte[len];
            this.setAdminName(new String(buffer, "UTF-16"));
            len = stream.readInt();
            buffer = new byte[len];
            this.setAdminPassword(new String(buffer, "UTF-16"));
            this.setVisible(stream.readBoolean());
            return this;
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public AdminOverride copy() {
        AdminOverride admin = new AdminOverrideV1(getLogger());
        admin.setAdminName(getAdminName());
        admin.setAdminPassword(getAdminPassword());
        admin.setVisible(isVisible());

        return admin;
    }

    @Override
    public boolean equalsObject(AdminOverride object) {
        return equalsObject(object, false);
    }

    @Override
    public boolean equalsObject(AdminOverride object, boolean ignoreStringCase) {
        boolean ret = object.getAdminName().equals(getAdminName());
        ret = !ret ? false : object.getAdminPassword().equals(this.getAdminPassword());
        ret = !ret ? false : object.isVisible() == isVisible();

        return ret;

    }

}
