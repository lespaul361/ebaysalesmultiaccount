/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AbstractApplicationRunInfo;
import com.github.lespaul361.internetsales.configurationfile.ApplicationRunInfo;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Charles Hamilton
 */
public class ApplicationRunInfoV1 extends AbstractApplicationRunInfo {

    public ApplicationRunInfoV1() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm a");
        this.startTime = LocalTime.parse("6:00 AM", dtf);
        this.endTime = LocalTime.parse("6:00 PM", dtf);
    }

    @Override
    public void write(DataOutputStream stream) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm a");

            String tmp = this.startTime.format(dtf);
            byte[] buffer = tmp.getBytes("UTF-8");
            int len = buffer.length;
            stream.writeInt(len);
            stream.write(buffer);

            tmp = this.endTime.format(dtf);
            buffer = tmp.getBytes("UTF-8");
            len = buffer.length;
            stream.writeInt(len);
            stream.write(buffer);

            stream.writeInt(interval);
            stream.writeInt(this.fetchType);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

    }

    @Override
    public Object read(DataInputStream stream) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm a");

        int len;
        byte[] buffer;
        String tmp;

        try {
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            tmp = new String(buffer, "UTF-8");
            this.startTime = LocalTime.parse(tmp, dtf);

            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            tmp = new String(buffer, "UTF-8");
            this.endTime = LocalTime.parse(tmp, dtf);

            this.interval = stream.readInt();
            this.fetchType = stream.readInt();
            return this;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public ApplicationRunInfo copy() {
        ApplicationRunInfo ret = new ApplicationRunInfoV1();
        ret.setSalesFetchType(fetchType);
        ret.setEndTime(endTime);
        ret.setIntervalTime(interval);
        ret.setStartTime(startTime);

        return ret;
    }

    @Override
    public boolean equalsObject(ApplicationRunInfo object) {
        return equalsObject(object, true);
    }

    @Override
    public boolean equalsObject(ApplicationRunInfo object, boolean ignoreStringCase) {
        if (!this.endTime.equals(object.getTimeEnd())) {
            return false;
        }

        if (!this.startTime.equals(object.getStartTime())) {
            return false;
        }

        if (this.interval != object.getIntervalTime()) {
            return false;
        }

        if (this.fetchType != object.getSalesFetchType()) {
            return false;
        }

        return true;
    }

}
