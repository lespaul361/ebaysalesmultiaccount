/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AbstractEbayApplicationInfo;
import com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * Holds information for the Ebayapplication information
 * </p>
 * The data is saved as:<br>
 * <table summary="The data is saved as:">
 * <tr><th>Byte</th><th>Value</th></tr>
 * <tr><td>0-3</td><td>Length of Application ID</td></tr>
 * <tr><td>length</td><td>Application ID</td></tr>
 * <tr><td>length + 4</td><td>Length of certificate ID </td></tr>
 * <tr><td>last + length</td><td>Certificate ID string</td></tr>
 * <tr><td>last + 4</td><td>Length of developer name</td></tr>
 * <tr><td>last + length</td><td>developer name string</td></tr>
 * <tr><td>last + 4</td><td>RU Name length</td></tr>
 * <tr><td>last + length</td><td>RU Name string</td></tr>
 * </table>
 *
 */
public class EbayApplicationInfoV1 extends AbstractEbayApplicationInfo {

    public EbayApplicationInfoV1(Logger logger) {
        super(logger);
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    EbayApplicationInfoV1(String certificateId, String appId, String developerName,
            String ruName, Logger logger) {
        this(logger);
        setCertificateId(certificateId);
        setAppId(appId);
        setDeveloperName(developerName);
        setRuName(ruName);
    }

    @Override
    public void write(DataOutputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            byte buffer[];
            buffer = this.getAppId().getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            buffer = this.getCertificateId().getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            buffer = this.getDeveloperName().getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            buffer = this.getRuName().getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public Object read(DataInputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            int len = 0;
            byte[] buffer;
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            this.setAppId(new String(buffer, "UTF-8"));
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            this.setCertificateId(new String(buffer, "UTF-8"));
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            this.setDeveloperName(new String(buffer, "UTF-8"));
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            this.setRuName(new String(buffer, "UTF-8"));
            return this;
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    public EbayApplicationInfo copy() {
        return new EbayApplicationInfoV1(certificateId, appId, developerName,
                ruName, getLogger());

    }

    @Override
    public boolean equalsObject(EbayApplicationInfo object) {
        return equalsObject(object, false);
    }

    @Override
    public boolean equalsObject(EbayApplicationInfo object, boolean ignoreStringCase) {
        try {
            if (ignoreStringCase) {
                if (!this.appId.equalsIgnoreCase(object.getAppId())) {
                    return false;
                }
                if (!this.certificateId.equalsIgnoreCase(object.getCertificateId())) {
                    return false;
                }
                if (!this.developerName.equalsIgnoreCase(object.getDeveloperName())) {
                    return false;
                }
                if (!this.ruName.equalsIgnoreCase(object.getRuName())) {
                    return false;
                }

                return true;
            }

            if (!this.appId.equals(object.getAppId())) {
                return false;
            }
            if (!this.certificateId.equals(object.getCertificateId())) {
                return false;
            }
            if (!this.developerName.equals(object.getDeveloperName())) {
                return false;
            }
            if (!this.ruName.equals(object.getRuName())) {
                return false;
            }

            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
