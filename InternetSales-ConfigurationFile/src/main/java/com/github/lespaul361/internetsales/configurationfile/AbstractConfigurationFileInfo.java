/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * An abstract class for the ConfigurationFileInfo interface
 *
 * @author Charles Hamilton
 */
public abstract class AbstractConfigurationFileInfo extends AbstractLogError
        implements ConfigurationFileInfo {

    protected FTPInfo ftpInfo = null;
    protected List<EbayAccount> ebayAcccounts = null;
    protected List<AmazonAccount> amazonAccounts = null;
    protected byte version;
    protected EbayApplicationInfo ebayApplicationInfo = null;
    protected ApplicationRunInfo applicationRunInfo = null;
    protected List<AdminOverride> admins = null;

    /**
     * Constructor for the abstract class
     *
     * @param logger the logger
     */
    public AbstractConfigurationFileInfo(Logger logger) {
        super(logger);
    }

    @Override
    public FTPInfo getFTPInfo() {
        return ftpInfo;
    }

    @Override
    public void setFTPInfo(FTPInfo info) {
        this.ftpInfo = info;
    }

    @Override
    public List<EbayAccount> getEbayAccounts() {
        return ebayAcccounts;
    }

    @Override
    public void addEbayAccount(EbayAccount account) {
        if (this.ebayAcccounts == null) {
            this.ebayAcccounts = new ArrayList<>(5);
        }
        this.ebayAcccounts.add(account);
    }

    @Override
    public void setEbayAccounts(List<EbayAccount> accounts) {
        this.ebayAcccounts = accounts;
    }

    @Override
    public byte getVersion() {
        return this.version;
    }

    @Override
    public List<AmazonAccount> getAmazonAccounts() {
        return this.amazonAccounts;
    }

    @Override
    public void addAmazonAccount(AmazonAccount account) {
        if (this.amazonAccounts == null) {
            this.amazonAccounts = new ArrayList<>(5);
        }
        this.amazonAccounts.add(account);
    }

    @Override
    public void setAmazonAccounts(List<AmazonAccount> accounts) {
        this.amazonAccounts = accounts;
    }

    /**
     * @return the ebayApplicationInfo
     */
    @Override
    public EbayApplicationInfo getEbayApplicationInfo() {
        return ebayApplicationInfo;
    }

    /**
     * @param ebayApplicationInfo the ebayApplicationInfo to set
     */
    @Override
    public void setEbayApplicationInfo(EbayApplicationInfo ebayApplicationInfo) {
        this.ebayApplicationInfo = ebayApplicationInfo;
    }

    @Override
    public ApplicationRunInfo getApplicationRunInfo() {
        return this.applicationRunInfo;
    }

    @Override
    public void setApplicationRunInfo(ApplicationRunInfo info) {
        this.applicationRunInfo = info;
    }

    @Override
    public List<AdminOverride> getAdminOverrides() {
        return this.admins;
    }

    @Override
    public void setAdminOverrides(List<AdminOverride> admins) {
        this.admins=admins;
    }

    @Override
    public boolean equalsObject(ConfigurationFileInfo object) {
        return equalsObject(object, false);
    }

    @Override
    public boolean equalsObject(ConfigurationFileInfo object, boolean ignoreStringCase) {
        boolean ret = object.getAdminOverrides().size() == this.admins.size();
        ret = compareAdmin(object);
        ret = !ret ? false : compareEbay(object);
        ret = !ret ? false : object.getApplicationRunInfo().equalsObject(applicationRunInfo);
        ret = !ret ? false : object.getEbayApplicationInfo().equalsObject(ebayApplicationInfo);
        ret = !ret ? false : object.getFTPInfo().equals(this.getFTPInfo());
        ret = !ret ? false : object.getVersion() == this.getVersion();

        return ret;

    }

    private boolean compareAdmin(ConfigurationFileInfo object) {
        if (this.admins == null && object.getAdminOverrides() == null) {
            return true;
        } else if (this.admins != null && object.getAdminOverrides() == null) {
            return false;
        } else if (this.admins == null && object.getAdminOverrides() != null) {
            return false;
        }

        if (this.admins.size() != object.getAdminOverrides().size()) {
            return false;
        }

        for (AdminOverride admin : this.admins) {
            for (AdminOverride ad : object.getAdminOverrides()) {
                if (ad.getAdminName().equals(admin.getAdminName())) {
                    if (ad.equalsObject(admin)) {
                        continue;
                    }
                }
                return false;
            }
        }

        return true;
    }

    private boolean compareEbay(ConfigurationFileInfo object) {
        if (this.ebayAcccounts == null && object.getEbayAccounts() == null) {
            return true;
        } else if (this.ebayAcccounts != null && object.getEbayAccounts() == null) {
            return false;
        } else if (this.ebayAcccounts == null && object.getEbayAccounts() != null) {
            return false;
        }
        for (EbayAccount ebayAccountO : this.getEbayAccounts()) {
            for (EbayAccount e : object.getEbayAccounts()) {
                if (ebayAccountO.getAccountName().equals(e.getAccountName())) {
                    if (ebayAccountO.equalsObject(e)) {
                        continue;
                    }
                }
                return false;
            }
        }

        return true;
    }
}
