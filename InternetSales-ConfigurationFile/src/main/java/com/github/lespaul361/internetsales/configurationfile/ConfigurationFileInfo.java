/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.util.List;

/**
 * Interface for the configuration info
 *
 */
public interface ConfigurationFileInfo extends CustomReadWrite,
        Copy<ConfigurationFileInfo>, Equals<ConfigurationFileInfo> {

    /**
     * The version of this file
     *
     * @return byte
     */
    byte getVersion();

    /**
     * Gets a list of Amazon accounts
     *
     * @return list of Amazon accounts
     */
    List<AmazonAccount> getAmazonAccounts();

    /**
     * Sets the list of Amazon accounts
     *
     * @param accounts list of Amazon accounts
     */
    void setAmazonAccounts(List<AmazonAccount> accounts);

    /**
     * Adds an amazon account to the list. After adding you will need to save
     * the file
     *
     * @param account an amazon account
     */
    void addAmazonAccount(AmazonAccount account);

    /**
     * Gets a list of <code>EbayAccount</code>s
     *
     * @return list of <code>EbayAccount</code>s
     */
    List<EbayAccount> getEbayAccounts();

    /**
     * Sets the list of <code>EbayAccount</code>s
     *
     * @param accounts list of <code>EbayAccount</code>s
     */
    void setEbayAccounts(List<EbayAccount> accounts);

    /**
     * Adds an <code>EbayAccount</code> to the list. After adding you will need
     * to save the file
     *
     * @param account an <code>EbayAccount</code>
     */
    void addEbayAccount(EbayAccount account);

    /**
     * Gets the FTP information
     *
     * @return the FTP information
     */
    FTPInfo getFTPInfo();

    /**
     * Sets the FTP information
     *
     * @param info the FTP information
     */
    void setFTPInfo(FTPInfo info);

    /**
     * Sets the Ebayapplications connection information
     *
     * @param info the Ebayapplications connection information
     */
    void setEbayApplicationInfo(EbayApplicationInfo info);

    /**
     * Sets the Ebayapplications connection information
     *
     * @return the Ebayapplications connection information
     */
    EbayApplicationInfo getEbayApplicationInfo();

    /**
     * Gets the time restrictions for running the application
     *
     * @return the ApplicationRunInfo
     */
    ApplicationRunInfo getApplicationRunInfo();

    /**
     * Sets the time restrictions for running the application
     *
     * @param info the ApplicationRunInfo
     */
    void setApplicationRunInfo(ApplicationRunInfo info);

    /**
     * Gets a list of the administrator's info
     *
     * @return list of the administrator's info
     */
    List<AdminOverride> getAdminOverrides();

    /**
     * Sets the list of the administrator's info
     *
     * @param admins list of the administrator's info
     */
    void setAdminOverrides(List<AdminOverride> admins);
}
