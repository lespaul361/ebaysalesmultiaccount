/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

/**
 *
 * @author Charles Hamilton
 */
public interface FTPInfo extends CustomReadWrite, Copy<FTPInfo>, Equals<FTPInfo> {

    /**
     * Gets the FTP user name
     *
     * @return the FTPUserName
     */
    public String getFTPUserName();

    /**
     * Sets the FTP user name
     *
     * @param FTPUserName the FTPUserName to set
     */
    public void setFTPUserName(String FTPUserName);

    /**
     * Gets the FTP IP address
     *
     * @return the FTPIP
     */
    public String getFTPIP();

    /**
     * Sets the FTP IP address
     *
     * @param FTPIP the FTPIP to set
     */
    public void setFTPIP(String FTPIP);

    /**
     * Gets the FTP password
     *
     * @return the FTPPassword
     */
    public String getFTPPassword();

    /**
     * Sets the FTP password
     *
     * @param FTPPassword the FTPPassword to set
     */
    public void setFTPPassword(String FTPPassword);

}
