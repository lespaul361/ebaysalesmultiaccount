/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * <p>
 * Holds the information for the accounts
 * </p>
 * 
 * @author Charles Hamilton
 */
public interface ConfigurationFile extends CustomReadWrite, 
        Copy<ConfigurationFile>, Equals<ConfigurationFile> {

    /**
     * Gets the {@link ConfigurationFileInfo} for the accounts
     * 
     * @return the ConfigurationFileInfo
     */
    ConfigurationFileInfo getConfigurationFileInfo();

    /**
     * Sets the {@link ConfigurationFileInfo} for the accounts. This does not
     * save the information. Use saveConfigurationFile to save the information
     * 
     * @param info
     *            the ConfigurationFileInfo
     */
    void setConfigurationFileInfo(ConfigurationFileInfo info);

    /**
     * Saves the ConfigurationFile to the supplied file
     * 
     * @param file
     *            the file to save the information to
     * @throws java.io.FileNotFoundException
     *             This exception will be thrown by the FileInputStream,
     *             FileOutputStream, and RandomAccessFile constructors when a
     *             file with the specified pathname does not exist. It will also
     *             be thrown by these constructors if the file does exist but
     *             for some reason is inaccessible, for example when an attempt
     *             is made to open a read-only file for writing.
     */
    void saveConfigurationFile(File file) throws FileNotFoundException;

    /**
     * Saves the ConfigurationFile to the supplied stream
     * 
     * @param stream
     *            the stream to save the information to
     */
    void saveConfigurationFile(OutputStream stream);

    /**
     * Reads the ConfigurationFile from the supplied file
     * 
     * @param file
     *            the file to save the information to
     * @throws java.io.FileNotFoundException
     *             This exception will be thrown by the FileInputStream,
     *             FileOutputStream, and RandomAccessFile constructors when a
     *             file with the specified pathname does not exist. It will also
     *             be thrown by these constructors if the file does exist but
     *             for some reason is inaccessible, for example when an attempt
     *             is made to open a read-only file for writing.
     */
    void readConfigurationFile(File file) throws FileNotFoundException;

    /**
     * Reads the ConfigurationFile to the supplied stream
     * 
     * @param stream
     *            the stream to read the information from
     */
    void readConfigurationFile(InputStream stream);
    
    /**
     * Gets the version of the file format
     * @return the version of the the file format
     */
    byte getFileVersion();
}
