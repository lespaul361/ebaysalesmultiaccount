/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AbstractConfigurationFile;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFile;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFileInfo;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Holds the {@link ConfigurationFileInfo} for this version
 */
public class ConfigurationFileV1 extends AbstractConfigurationFile 
        implements ConfigurationFile {

    /**
     * Constructs a new ConfigurationFileV1 object
     *
     * @param logger the logger
     */
    public ConfigurationFileV1(Logger logger) {
        super(logger);
    }

    @SuppressWarnings("OverridableMethodCallInConstructor")
    ConfigurationFileV1(ConfigurationFileInfo configurationFileInfo, Logger logger) {
        super(logger);
        setConfigurationFileInfo(configurationFileInfo);
    }

    @Override
    public void write(DataOutputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }

        try {
            stream.write(new byte[]{1});
        } catch (Exception e) {
            RuntimeException ex = new RuntimeException("Cannot write to stream");
            logError(Level.SEVERE, ex.getMessage(), ex);
            throw ex;
        }

        super.configurationFileInfo.write(stream);
    }

    @Override
    public Object read(DataInputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            if (1 != stream.readByte()) {
                throw new RuntimeException("Invalid File Versin");
            }
            ConfigurationFileInfo info = new ConfigurationFileInfoV1(getLogger());
            super.configurationFileInfo = (ConfigurationFileInfo) info.read(stream);
            return super.configurationFileInfo;
        } catch (Exception e) {
            throw new RuntimeException("Cannot read from stream");
        } finally {
            try {
                stream.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ConfigurationFile) {
            return getConfigurationFileInfo().equals(((ConfigurationFile) obj).getConfigurationFileInfo());
        }
        return getConfigurationFileInfo().equals(obj);
    }

    @Override
    public int hashCode() {
        return getConfigurationFileInfo().hashCode();
    }

    @Override
    public ConfigurationFile copy() {
        return new ConfigurationFileV1(configurationFileInfo, getLogger());
    }

    @Override
    public byte getFileVersion() {
        return 1;
    }

    @Override
    public boolean equalsObject(ConfigurationFile object) {
        return equalsObject(object, false);
    }

    @Override
    public boolean equalsObject(ConfigurationFile object, boolean ignoreStringCase) {
        if (this.getFileVersion() != object.getFileVersion()) {
            return false;
        }
        return this.configurationFileInfo.equalsObject(object.getConfigurationFileInfo());
    }

}
