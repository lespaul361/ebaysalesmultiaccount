/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile.version1;

import com.github.lespaul361.internetsales.configurationfile.AbstractFTPInfo;
import com.github.lespaul361.internetsales.configurationfile.FTPInfo;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * Holds information for the FTP server
 * </p>
 * The data is saved as:<br>
 * <table summary="The data is saved as:">
 * <tr><th>Byte</th><th>Value</th></tr>
 * <tr><td>0-3</td><td>Length of the IP address string</td></tr>
 * <tr><td>4+length of IP</td><td>IP address string</td></tr>
 * <tr><td>last + 4</td><td>Length of the password string</td></tr>
 * <tr><td>last + length of password</td><td>The password string</td></tr>
 * <tr><td>last + 4</td><td>Length of the user name string</td></tr>
 * <tr><td>last + length of use name</td><td>The user name string</td></tr>
 * </table>
 */
public class FTPInfoV1 extends AbstractFTPInfo {

    private static final long serialVersionUID = 782913643453371165L;

    private String ftpUserName;
    private String ftpIP;
    private String ftpPassword;

    /**
     * Constructs a new FTPInfoV1 object
     *
     * @param logger the logger
     */
    public FTPInfoV1(Logger logger) {
        super(logger);
    }

    FTPInfoV1(Logger logger, String ftpUserName, String ftpIP, String ftpPassword) {
        super(logger);
        this.ftpIP = ftpIP;
        this.ftpPassword = ftpPassword;
        this.ftpUserName = ftpUserName;
    }

    @Override
    public String getFTPUserName() {
        return ftpUserName;
    }

    @Override
    public void setFTPUserName(String FTPUserName) {
        this.ftpUserName = FTPUserName;
    }

    @Override
    public String getFTPIP() {
        return ftpIP;
    }

    @Override
    public void setFTPIP(String FTPIP) {
        this.ftpIP = FTPIP;
    }

    @Override
    public String getFTPPassword() {
        return ftpPassword;
    }

    @Override
    public void setFTPPassword(String FTPPassword) {
        this.ftpPassword = FTPPassword;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.ftpUserName);
        hash = 23 * hash + Objects.hashCode(this.ftpIP);
        hash = 23 * hash + Objects.hashCode(this.ftpPassword);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FTPInfoV1 other = (FTPInfoV1) obj;
        if (!Objects.equals(this.ftpUserName, other.ftpUserName)) {
            return false;
        }
        if (!Objects.equals(this.ftpIP, other.ftpIP)) {
            return false;
        }
        if (!Objects.equals(this.ftpPassword, other.ftpPassword)) {
            return false;
        }
        return true;
    }

    @Override
    public Object read(DataInputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            int len;
            len = stream.readInt();
            byte[] buffer = new byte[len];
            stream.read(buffer);
            this.ftpIP = new String(buffer, "UTF-8");
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            this.ftpPassword = new String(buffer, "UTF-8");
            len = stream.readInt();
            buffer = new byte[len];
            stream.read(buffer);
            this.ftpUserName = new String(buffer, "UTF-8");
            return this;
        } catch (Exception e) {
            logError(Level.SEVERE, "Corrupt Configuration File", e);
        }

        return null;
    }

    @Override
    public void write(DataOutputStream stream) {
        if (stream == null) {
            RuntimeException e = new RuntimeException("Stream Is Null");
            logError(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        try {
            byte[] buffer = this.ftpIP.getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            buffer = this.ftpPassword.getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
            buffer = this.ftpUserName.getBytes("UTF-8");
            stream.writeInt(buffer.length);
            stream.write(buffer);
        } catch (Exception e) {
            logError(Level.SEVERE, "Corrupt Configuration File", e);
        }
    }

    @Override
    public String toString() {
        return "FTPInfoV1{" + "FTPUserName=" + ftpUserName + ", FTPIP=" + ftpIP + ", FTPPassword=" + ftpPassword + '}';
    }

    @Override
    public FTPInfo copy() {
        return new FTPInfoV1(super.getLogger(), ftpUserName, ftpIP, ftpPassword);
    }

    @Override
    public boolean equalsObject(FTPInfo object) {
        return equalsObject(object, false);
    }

    @Override
    public boolean equalsObject(FTPInfo object, boolean ignoreStringCase) {
        try {
            if (ignoreStringCase) {
                if (!this.ftpIP.equalsIgnoreCase(object.getFTPIP())) {
                    return false;
                }
                if (!this.ftpPassword.equalsIgnoreCase(object.getFTPPassword())) {
                    return false;
                }
                if (!this.ftpUserName.equalsIgnoreCase(object.getFTPUserName())) {
                    return false;
                }
                return true;
            }
            
            if (!this.ftpIP.equals(object.getFTPIP())) {
                return false;
            }
            if (!this.ftpPassword.equals(object.getFTPPassword())) {
                return false;
            }
            if (!this.ftpUserName.equals(object.getFTPUserName())) {
                return false;
            }
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
