/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

/**
 * <p>
 * Holds the information for connecting to the Ebay application
 * </p>
 */
public interface EbayApplicationInfo extends CustomReadWrite, 
        Copy<EbayApplicationInfo>, Equals<EbayApplicationInfo> {

    /**
     * Gets the developer name
     *
     * @return a String
     */
    String getDeveloperName();

    /**
     * Sets the developer name
     *
     * @param name developer name
     */
    void setDeveloperName(String name);

    /**
     * Gets the application ID
     *
     * @return the application ID
     */
    String getAppId();

    /**
     * sets the application ID
     *
     * @param appId the application ID
     */
    void setAppId(String appId);

    /**
     * Gets the certificate ID
     *
     * @return the certificate ID
     */
    String getCertificateId();

    /**
     * Sets the certificate ID
     *
     * @param Id the certificate ID
     */
    void setCertificateId(String Id);

    /**
     * Gets the RU name
     *
     * @return the RU name
     */
    String getRuName();

    /**
     * Sets the RU name
     *
     * @param name the RU name
     */
    void setRuName(String name);
}
