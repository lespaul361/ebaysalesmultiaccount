/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.configurationfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Charles Hamilton
 */
public abstract class AbstractApplicationRunInfo implements ApplicationRunInfo {

    protected LocalTime startTime = null;
    protected LocalTime endTime = null;
    protected int interval = 30;
    protected int fetchType = 0;

    @Override
    public LocalTime getStartTime() {
        return startTime;
    }

    @Override
    public void setStartTime(LocalTime time) {
        this.startTime = time;
    }

    @Override
    public LocalTime getTimeEnd() {
        return this.endTime;
    }

    @Override
    public void setEndTime(LocalTime time) {
        this.endTime = time;
    }

    @Override
    public int getIntervalTime() {
        return this.interval;
    }

    @Override
    public void setIntervalTime(int value) {
        this.interval = value;
    }

    @Override
    public void setSalesFetchType(int salesFetchType) {
        if (salesFetchType < 0 || salesFetchType > 1) {
            throw new IllegalArgumentException("Invalid value for salesFetchType");
        }
    }

    @Override
    public int getSalesFetchType() {
        return this.fetchType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.startTime);
        hash = 97 * hash + Objects.hashCode(this.endTime);
        hash = 97 * hash + this.interval;
        hash = 97 * hash + this.fetchType;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractApplicationRunInfo other = (AbstractApplicationRunInfo) obj;
        if (this.interval != other.interval) {
            return false;
        }
        if (this.fetchType != other.fetchType) {
            return false;
        }
        if (!Objects.equals(this.startTime, other.startTime)) {
            return false;
        }
        if (!Objects.equals(this.endTime, other.endTime)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String fetchType = "";
        if (getSalesFetchType() == ApplicationRunInfo.COMPLETED_TYPE_PAID) {
            fetchType = "COMPLETED_TYPE_PAID";
        } else {
            fetchType = "COMPLETED_TYPE_SHIPPED";
        }
        
        return "AbstractApplicationRunInfo{" + "startTime=" + startTime
                + ", endTime=" + endTime + ", interval=" + interval + ", fetchType="
                + fetchType + '}';
    }

}
