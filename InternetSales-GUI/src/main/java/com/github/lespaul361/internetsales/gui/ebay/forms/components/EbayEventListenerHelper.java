/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener;

/**
 * Adds support for Ebayaccount editing listeners to components
 */
public interface EbayEventListenerHelper {

    /**
     * Adds an {@link EbayAccountListener} to the component
     * 
     * @param l
     *            EbayAccountListener
     */
    public void addEbayAccountListener(EbayAccountListener l);

    /**
     * Removes an {@link EbayAccountListener} to the component
     * 
     * @param l
     *            EbayAccountListener
     */
    public void removeEbayAccountListener(EbayAccountListener l);
}
