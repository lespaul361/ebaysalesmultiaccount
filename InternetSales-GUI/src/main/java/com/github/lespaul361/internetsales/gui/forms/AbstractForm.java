/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.forms;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Image;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * Wraps a {@link JDialog} to simplify the interaction
 */
public abstract class AbstractForm extends AbstractLogError {

    protected JDialog dialog;
    protected final Image logo;

    /**
     * Constructs a new AbstractForm object
     * 
     * @param logo
     *            the company logo
     * @param logger
     *            a logger for logging errors
     */
    public AbstractForm(Image logo, Logger logger) {
	super(logger, logo);
	this.logo = logo;
    }

    /**
     * <p>
     * setIconImage.
     * </p>
     * 
     * @param image
     *            a {@link java.awt.Image} object.
     */
    public void setIconImage(Image image) {
	dialog.setIconImage(image);
    }

    /**
     * <p>
     * isVisible.
     * </p>
     * 
     * @return a boolean.
     */
    public boolean isVisible() {
	return dialog.isVisible();
    }

    /**
     * <p>
     * setVisible.
     * </p>
     * 
     * @param visible
     *            a boolean.
     */
    abstract public void setVisible(final boolean visible);

    /**
     * <p>
     * dispose.
     * </p>
     */
    public void dispose() {
        Thread t = new Thread(() -> {
            dialog.setVisible(false);
            if (dialog.getParent() instanceof DummyFrame) {
                ((DummyFrame) dialog.getParent()).dispose();
            }
            dialog.dispose();
        });
        t.start();
    }

    /**
     * <p>
     * pack.
     * </p>
     */
    public void pack() {
	this.dialog.pack();
    }

    /**
     * Gets the parent Frame
     * 
     * @return the parent Frame
     */
    public Frame getParent() {
	return (this.dialog != null && this.dialog.getParent() != null ? (JFrame) this.dialog.getParent() : null);
    }

    /**
     * Gets the {@link JDialog} being wrapped
     * 
     * @return the {@link JDialog}
     */
    public Dialog getBaseDialog() {
	return this.dialog;
    }
}
