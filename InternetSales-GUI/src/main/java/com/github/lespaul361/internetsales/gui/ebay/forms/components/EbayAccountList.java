/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import java.awt.Component;
import java.util.logging.Logger;
import javax.swing.Icon;

/**
 *
 * @author Charles Hamilton
 */
public class EbayAccountList extends AbstractAccountList<EbayAccount, EbayAccountComponent> {

    private final Component parent;
    private final Icon icon;

    public EbayAccountList(Logger logger, String borderTitle, Component parent, Icon icon) {
        super(logger, borderTitle);
        this.parent = parent;
        this.icon = icon;
    }

    @Override
    public void addAccount(EbayAccount account) {
        super.accounts.add(account);
        addAccountComponent(new EbayAccountComponent(account, getLogger(),
                parent, icon));
    }

    @Override
    public void addAccountComponent(EbayAccountComponent component) {
        super.accountComponents.add(component);
        updateComponents();
    }

}
