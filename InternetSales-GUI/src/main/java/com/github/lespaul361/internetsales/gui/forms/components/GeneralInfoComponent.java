/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.internetsales.gui.forms.components;

import com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities;
import com.github.lespaul361.commons.version.Version;
import com.github.lespaul361.internetsales.configurationfile.ApplicationRunInfo;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import javax.swing.JPanel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Charles Hamilton
 */
public class GeneralInfoComponent extends JPanel {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm a");
    private ApplicationRunInfo info;
    private volatile ApplicationRunInfo curInfo;
    private final Version version;
    private String startTime = null;
    private String endTime = null;
    protected JComboBox<String> cmboStartTime;
    protected JComboBox<String> cmboEndTime;
    protected JComboBox<Integer> cmboIntervals = null;
    protected JComboBox<String> cmboEbayFetch = null;
    protected JPanel pnlTimes = null;
    protected JPanel pnlOptions = null;
    private int rightSide = 0;

    public GeneralInfoComponent(ApplicationRunInfo info, final Version version) {
        this.info = info;
        this.version = version;
        startTime = info.getStartTime().format(dtf);
        endTime = info.getTimeEnd().format(dtf);
        this.curInfo = info.copy();

        init();
    }

    public ApplicationRunInfo getApplicationRunInfo() {
        ApplicationRunInfo ret = curInfo;

        return ret;
    }

    public void addActionListener(ActionListener l) {
        listenerList.add(ActionListener.class, l);
    }

    public void removeActionListener(ActionListener l) {
        listenerList.remove(ActionListener.class, l);
    }

    private void init() {
        initThis();
        initVersion();
        initTimePanel();
        initOptions();
        initListeners();
    }

    private void initThis() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(Box.createVerticalStrut(15));
    }

    private void initVersion() {
       // this.add(Box.createVerticalStrut(5));
        Box boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(20));
        boxh.add(new JLabel("Version: "));
        boxh.add(new JLabel(version.toString()));
        setBoxSizes(boxh);
        boxh.add(Box.createHorizontalGlue());
        this.add(boxh);
        this.add(Box.createVerticalStrut(10));

    }

    private void initTimePanel() {
        this.pnlTimes = new JPanel();
        this.pnlTimes.setLayout(new BoxLayout(pnlTimes, BoxLayout.Y_AXIS));
        String[] timeValues = new String[]{"5:00", "5:30", "6:00", "6:30", "7:00",
            "7:30", "8:00", "8:30", "9:00"};
        JLabel lblStart = new JLabel("Start Time: ");
        JLabel lblEnd = new JLabel("End Time: ");
        int extraLen = lblStart.getPreferredSize().width - lblEnd.getPreferredSize().width;
        cmboStartTime = new JComboBox<>(timeValues);
        cmboEndTime = new JComboBox<>(timeValues);

        String tmpTime = startTime.substring(0, startTime.indexOf(" "));
        tmpTime = tmpTime.trim();

        cmboStartTime.setSelectedIndex(findIndex(timeValues, tmpTime));

        tmpTime = endTime.substring(0, endTime.indexOf(" "));
        tmpTime = tmpTime.trim();

        cmboEndTime.setSelectedIndex(findIndex(timeValues, tmpTime));

        this.pnlTimes.add(Box.createVerticalStrut(10));
        Box boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(10));
        boxh.add(lblStart);
        boxh.add(cmboStartTime);
        boxh.add(new JLabel(" AM"));
        setBoxSizes(boxh);
        boxh.add(Box.createHorizontalGlue());
        this.pnlTimes.add(boxh);

        this.pnlTimes.add(Box.createVerticalStrut(10));

        boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(10 + extraLen));
        boxh.add(lblEnd);
        boxh.add(cmboEndTime);
        boxh.add(new JLabel(" PM"));
        setBoxSizes(boxh);
        boxh.add(Box.createHorizontalGlue());
        this.pnlTimes.add(boxh);

        this.pnlTimes.add(Box.createVerticalStrut(10));

        this.pnlTimes.setBorder(new TitledBorder("Time Range"));

        this.add(this.pnlTimes);
    }

    private void initOptions() {
        initOptionsPanel();
        initIntervals();
        initEbayFetch();
    }

    private void initOptionsPanel() {
        this.pnlOptions = new JPanel();
        this.pnlOptions.setLayout(new BoxLayout(this.pnlOptions, BoxLayout.Y_AXIS));
        this.pnlOptions.add(Box.createVerticalStrut(10));
        this.pnlOptions.setBorder(new TitledBorder("Options"));
    }

    private void initIntervals() {
        Integer[] values = new Integer[]{15, 30, 45, 60, 75, 90, 105, 120};
        this.cmboIntervals = new JComboBox<>(values);

        this.cmboIntervals.setSelectedIndex(Arrays.binarySearch(values,
                this.info.getIntervalTime()));

        Box boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(10));
        boxh.add(new JLabel("Run Time Intervals In Minutes: "));
        boxh.add(cmboIntervals);
        setBoxSizes(boxh);
        boxh.add(Box.createHorizontalGlue());
        this.pnlOptions.add(boxh);
        this.pnlOptions.add(Box.createVerticalStrut(10));

    }

    private void initEbayFetch() {
        String[] values = new String[]{"On Paid", "On Shipped"};
        this.cmboEbayFetch = new JComboBox<>(values);
        JLabel lbl1 = new JLabel("Ebay Fetch Types: ");

        Box boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(10));
        boxh.add(lbl1);
        boxh.add(this.cmboEbayFetch);
        setBoxSizes(boxh);
        boxh.add(Box.createHorizontalGlue());
        this.pnlOptions.add(boxh);
        this.pnlOptions.add(Box.createVerticalStrut(10));

        if (this.info.getSalesFetchType() == ApplicationRunInfo.COMPLETED_TYPE_SHIPPED) {
            this.cmboEbayFetch.setSelectedIndex(1);
        } else if (this.info.getSalesFetchType() == ApplicationRunInfo.COMPLETED_TYPE_PAID) {
            this.cmboEbayFetch.setSelectedIndex(0);
        }

        this.add(this.pnlOptions);
    }

    private void initListeners() {
        ActionListener l = (ActionEvent e) -> {
            ActionEvent evt = new ActionEvent(this, e.getID(), e.getActionCommand());
            boolean fire = false;
            if (e.getSource() == cmboEbayFetch) {
                int val = cmboEbayFetch.getSelectedIndex();
                if (curInfo.getSalesFetchType() != val) {
                    curInfo.setSalesFetchType(val);
                    fire = true;
                }
            } else if (e.getSource() == cmboEndTime) {
                String tmp = cmboEndTime.getItemAt(cmboEndTime.getSelectedIndex());
                tmp += " PM";
                LocalTime time = LocalTime.parse(tmp, dtf);
                if (!time.equals(curInfo.getTimeEnd())) {
                    curInfo.setEndTime(time);
                    fire = true;
                }
            } else if (e.getSource() == cmboIntervals) {
                int val = cmboIntervals.getSelectedIndex();
                if (curInfo.getIntervalTime() != val) {
                    curInfo.setIntervalTime(val);
                    fire = true;
                }
            } else if (e.getSource() == cmboStartTime) {
                String tmp = cmboStartTime.getItemAt(cmboStartTime.getSelectedIndex());
                tmp += " AM";
                LocalTime time = LocalTime.parse(tmp, dtf);
                if (!time.equals(curInfo.getStartTime())) {
                    curInfo.setStartTime(time);
                    fire = true;
                }
            }
            if (fire) {
                fireEvent(evt);
            }
        };

        cmboEbayFetch.addActionListener(l);
        cmboEndTime.addActionListener(l);
        cmboIntervals.addActionListener(l);
        cmboStartTime.addActionListener(l);
    }

    private int findIndex(final String[] array, final String searchTerm) {
        int index = -1;

        for (int i = 0; i < array.length; i++) {
            if (array[i].equalsIgnoreCase(searchTerm)) {
                index = i;
                break;
            }
        }

        return index;
    }

    private void setBoxSizes(Box box) {
        Component[] components = box.getComponents();
        for (int i = 0; i < components.length; i++) {
            Component c = components[i];
            BoxUtilities.setSize(c, c.getPreferredSize());

        }
    }

    private void fireEvent(ActionEvent evt) {
        ActionListener[] listeners = listenerList.getListeners(ActionListener.class
        );
        for (int i = 0; i < listeners.length; i++) {
            listeners[i].actionPerformed(evt);
        }
    }
}
