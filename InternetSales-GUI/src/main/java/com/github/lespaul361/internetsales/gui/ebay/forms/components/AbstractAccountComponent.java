/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import static com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities.createBox;
import static com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities.setLargestButtonSize;
import com.github.lespaul361.internetsales.configurationfile.Account;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * An abstract class for making account components to add to
 * {@link Account}
 *
 * @param <T> the
 * {@link com.github.lespaul361.internetsales.configurationfile.Account} to use
 * to build this component
 */
public abstract class AbstractAccountComponent<T extends Account>
        extends AbstractJPanelWithActionListener {

    /**
     * The account
     */
    protected T account;
    /**
     * The edit button
     */
    protected JButton btnEdit;
    /**
     * The delete button
     */
    protected JButton btnDelete;
    /**
     * The button size
     */
    protected Dimension szButton;
    /**
     * The label with the account name
     */
    protected JLabel lblAccountName = null;

    private boolean isDelete = false;

    /**
     * Constructs and initializes the component to be used in an
     * {@link Account}
     *
     * @param account the account to build this component from
     * @param logger the {@link Logger}
     */
    public AbstractAccountComponent(T account, Logger logger) {
        super(logger);
        this.account = account;
        init();
    }

    /**
     * Gets the account this component is attached to
     *
     * @return the account
     */
    public T getAccount() {
        return this.account;
    }

    /**
     * Gets the components
     *
     * @return the components
     */
    public AbstractAccountComponent<T> getAccountComponent() {
        return this;
    }

    /**
     * Gets if the account is set for deletion or not
     *
     * @return the isDelete
     */
    public boolean isDelete() {
        return isDelete;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (!isDelete) {
            return;
        }
        //Strike through when an account is deleted
        Color color = lblAccountName.getForeground();
        Rectangle bounds = lblAccountName.getBounds();
        Dimension sz = lblAccountName.getSize();

        Point loc = new Point();
        Component currComp = lblAccountName;

        //get location relative to this component
        while (currComp != null && currComp != this) {
            Point rel = currComp.getLocation();
            loc.translate(rel.x, rel.y);
            currComp = currComp.getParent();
        }
        int y = loc.y;
        int x1 = loc.x;
        int x2 = x1 + sz.width;
        Graphics2D g2 = (Graphics2D) g;

        y += bounds.height / 2;
        Color oldColor = g2.getColor();
        g2.setColor(lblAccountName.getForeground());
        g2.drawLine(x1, y, x2, y);

        //reset to the original state
        g2.setColor(oldColor);
    }

    private void init() {
        initButtons();
        initLabel();
        initButtonSize();
        initButtonListeners();
        initPlaceButtons();
    }

    protected void initLabel() {
        lblAccountName = new JLabel(this.account.getAccountName() + ":");
    }

    protected void initButtons() {
        btnEdit = new JButton(" Edit ");
        btnDelete = new JButton(" Delete ");
    }

    protected void initButtonSize() {
        setLargestButtonSize(btnDelete, btnEdit);
    }

    protected void initPlaceButtons() {
        this.add(createBox(lblAccountName, 0, 5, 0, btnEdit));
        this.add(Box.createHorizontalStrut(5));
        this.add(btnDelete);
    }

    private void initButtonListeners() {

        btnEdit.addActionListener((ActionEvent e) -> {
            btnClicked(e);
        });

        btnDelete.addActionListener((ActionEvent e) -> {
            if (btnDelete.getText().trim().equalsIgnoreCase("delete")) {
                isDelete = true;
                btnEdit.setEnabled(false);
                lblAccountName.setForeground(Color.red);
                btnDelete.setText(" Undo Delete ");
                this.invalidate();
                this.repaint();
            } else {
                isDelete = false;
                btnEdit.setEnabled(true);
                lblAccountName.setForeground(Color.BLACK);
                btnDelete.setText(" Delete ");
                this.invalidate();
                this.repaint();
            }
            btnClicked(e);
        });

    }

    /**
     * Gets the action of a button being clicked
     *
     * @param e the {@link java.awt.event.ActionEvent}
     */
    protected void btnClicked(ActionEvent e) {
        JButton btn = (JButton) e.getSource();
        if (btn.equals(btnDelete)) {
            if (btnDelete.getText().trim().equalsIgnoreCase("delete")) {
                isDelete = true;
                btnEdit.setEnabled(false);
                lblAccountName.setForeground(Color.red);
                btnDelete.setText(" Undo Delete ");
                this.invalidate();
                this.repaint();
            } else {
                isDelete = false;
                btnEdit.setEnabled(true);
                lblAccountName.setForeground(Color.BLACK);
                btnDelete.setText(" Delete ");
                this.invalidate();
                this.repaint();
            }

        }
    }
}
