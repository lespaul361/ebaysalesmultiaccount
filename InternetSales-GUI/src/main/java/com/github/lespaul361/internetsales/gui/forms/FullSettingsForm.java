/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.forms;

import static com.github.lespaul361.commons.commonroutines.boxbuilding.BoxBuildRoutines.getBottomButtonPanel;
import static com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities.setSizesPrefAsMinWithMaxWidth;
import com.github.lespaul361.commons.version.Version;
import com.github.lespaul361.internetsales.configurationfile.AmazonAccount;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFileInfo;
import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo;
import com.github.lespaul361.internetsales.configurationfile.FTPInfo;
import com.github.lespaul361.internetsales.gui.forms.components.FTPComponent;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountEvent;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener;
import com.github.lespaul361.internetsales.gui.ebay.forms.EbaySecurityTokenDialog;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.AbstractAccountList;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayAccountComponent;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayAccountList;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayApplicationComponent;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayEventListenerHelper;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayNewAccountComponent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.EventListenerList;
import com.github.lespaul361.internetsales.gui.forms.components.GeneralInfoComponent;
import javax.swing.ImageIcon;

/**
 *
 * @author Charles Hamilton
 */
public class FullSettingsForm extends AbstractForm implements EbayEventListenerHelper {

    protected JTabbedPane mainTabbedPane = new JTabbedPane();
    protected JPanel panelFTP = new JPanel();
    protected JPanel panelEbay = new JPanel();
    protected JPanel panelGeneral = new JPanel();
    protected JPanel panelBottom = new JPanel();
    protected JPanel panelMain = new JPanel();
    protected JButton btnClose = new JButton("Cancel");
    protected JButton btnSave = new JButton("Save");
    protected EbayNewAccountComponent newEbayAccount = null;
    protected EbayApplicationComponent ebayApplicationComponent = null;
    protected GeneralInfoComponent generalInfoComponent = null;
    protected FTPComponent fTPComponent = null;

    private final Version version;
    private final List<EbayAccount> ebayAccounts = new ArrayList<>(5);
    private final List<AmazonAccount> amazonAccounts = new ArrayList<>(5);
    private final EbayApplicationInfo ebayApplicationInfo;
    private final FTPInfo fTPInfo;
    private final TrayIcon trayIcon;
    private EventListenerList listeners = new EventListenerList();
    private AbstractAccountList<EbayAccount, EbayAccountComponent> ebayListComponent = null;
    private ConfigurationFileInfo curConfig = null;
    private final ConfigurationFileInfo configurationFileInfo;

    public FullSettingsForm(Version version, ConfigurationFileInfo configInfo,
            Image logo, TrayIcon trayIcon, Logger logger) {
        super(logo, logger);
        this.version = version;
        this.ebayApplicationInfo = configInfo.getEbayApplicationInfo();
        this.fTPInfo = configInfo.getFTPInfo();
        this.ebayAccounts.addAll(configInfo.getEbayAccounts());
        this.amazonAccounts.addAll(configInfo.getAmazonAccounts());
        this.curConfig = configInfo.copy();
        this.configurationFileInfo = configInfo;
        this.trayIcon = trayIcon;

        init();
    }

    @Override
    public void setVisible(boolean visible) {
        PopupMenu pop = trayIcon.getPopupMenu();
        for (int i = 0; i < pop.getItemCount(); i++) {
            pop.getItem(i).setEnabled(!visible);
        }
        super.dialog.setVisible(visible);

    }

    @Override
    public void dispose() {
        ((JFrame) dialog.getParent()).dispose();
        PopupMenu pop = trayIcon.getPopupMenu();
        for (int i = 0; i < pop.getItemCount(); i++) {
            pop.getItem(i).setEnabled(true);
        }
        super.dialog.dispose();
        System.gc();
    }

    public ConfigurationFileInfo getConfigurationFileInfo() {
        return null;
        //TODO:Write code
    }

    public void addEbayUser(EbayAccount account) {
        this.curConfig.getEbayAccounts().add(account);
        this.ebayListComponent.addAccount(account);
    }

    @Override
    public void addEbayAccountListener(EbayAccountListener l) {
        this.listeners.add(EbayAccountListener.class, l);
    }

    @Override
    public void removeEbayAccountListener(EbayAccountListener l) {
        this.listeners.remove(EbayAccountListener.class, l);
    }

    private void init() {
        initButtonPanel();
        initMainPanel();
        initGeneralInfoPanel();
        initFTPPanel();
        initEbayPanel();
        initButtonListeners();
        initDialog();
        initActionListeners();
        checkSaveEnabled();
    }

    private void initMainPanel() {
        this.panelMain.setLayout(new BorderLayout());
        this.panelMain.add(mainTabbedPane, BorderLayout.NORTH);
        this.panelMain.add(panelBottom, BorderLayout.SOUTH);
        mainTabbedPane.setBorder(null);
        panelMain.setBorder(null);
    }

    private void initGeneralInfoPanel() {
        GeneralInfoComponent c = new GeneralInfoComponent(
                this.configurationFileInfo.getApplicationRunInfo(), this.version);
        this.generalInfoComponent = c;
        this.panelGeneral.setLayout(new BoxLayout(panelGeneral, BoxLayout.Y_AXIS));
        this.panelGeneral.add(c);
        this.mainTabbedPane.addTab("   General   ", panelGeneral);
    }

    private void initFTPPanel() {
        this.panelFTP.setLayout(new BoxLayout(panelFTP, BoxLayout.Y_AXIS));
        FTPComponent c = new FTPComponent(fTPInfo);
        initSize(c);
        this.fTPComponent = c;
        this.panelFTP.add(c);
        this.mainTabbedPane.addTab("   FTP   ", panelFTP);
    }

    private void initEbayPanel() {
        this.panelEbay.setLayout(new BoxLayout(panelEbay, BoxLayout.Y_AXIS));
        this.panelEbay.add(initEbaySettings());
        this.panelEbay.add(Box.createVerticalStrut(15));
        this.panelEbay.add(initEbayAccounts());
        this.panelEbay.add(Box.createVerticalStrut(15));
        this.panelEbay.add(initEbayNew());
        this.panelEbay.add(Box.createVerticalStrut(10));
        this.mainTabbedPane.addTab("   Ebay  ", panelEbay);
    }

    private Component initEbaySettings() {
        EbayApplicationComponent c = new EbayApplicationComponent(ebayApplicationInfo, getLogger());
        initSize(c);
        this.ebayApplicationComponent = c;
        return c;
    }

    private Component initButtonPanel() {
        panelBottom = new JPanel();
        panelBottom.setLayout(new BoxLayout(panelBottom, BoxLayout.Y_AXIS));
        JPanel pnlLine = new JPanel();
        pnlLine.setBackground(Color.BLACK);
        pnlLine.setPreferredSize(new Dimension(2, 1));
        pnlLine.setMinimumSize(new Dimension(2, 1));
        pnlLine.setMaximumSize(new Dimension(Integer.MAX_VALUE, 1));
        Box boxH = Box.createHorizontalBox();
        boxH.add(pnlLine);
        panelBottom.add(Box.createVerticalStrut(5));
        panelBottom.add(boxH);
        Dimension szButton = new Dimension(75, 25);
        panelBottom.add(getBottomButtonPanel(btnClose, btnSave, szButton));
        setButtonSize(btnSave, szButton);
        setButtonSize(btnClose, szButton);
        panelBottom.add(Box.createVerticalStrut(10));

        btnClose.setMnemonic(KeyEvent.VK_C);
        btnSave.setMnemonic(KeyEvent.VK_S);
        return panelBottom;
    }

    private Component initEbayAccounts() {
        this.ebayListComponent = new EbayAccountList(getLogger(), "Ebay Accounts",
                null, new ImageIcon(this.logo));
        this.ebayListComponent.add(Box.createVerticalStrut(10));

        for (EbayAccount ea : this.ebayAccounts) {
            this.ebayListComponent.addAccount(ea);
        }

        this.ebayListComponent.add(Box.createVerticalStrut(10));

        return this.ebayListComponent;
    }

    private Component initEbayNew() {
        this.newEbayAccount = new EbayNewAccountComponent();
        this.newEbayAccount.addEbayAccountListener(new EbayAccountListener() {
            @Override
            public void accountRequest(EbayAccountEvent evt) {
                if (evt.getAccountRequest() == EbayAccountEvent.REQUEST_ADD_KEY) {
                    EbaySecurityTokenDialog dlg = new EbaySecurityTokenDialog(
                            getLogger(), logo
                    );
                    int ret = dlg.showDialog();
                    if (ret == EbaySecurityTokenDialog.SAVE_SELECTED) {
                        int i = 0;
                        //TODO: write code
                    }
                }

                EbayAccountListener[] ls = listeners.getListeners(EbayAccountListener.class);
                for (int i = 0; i < ls.length; i++) {
                    ls[i].accountRequest(evt);
                }
            }
        });
        return this.newEbayAccount;
    }

    private void initMinFormSize() {
        int w = 0;
        int h = 0;

        h = panelMain.getPreferredSize().height;
        h += panelBottom.getPreferredSize().height;

        w = Math.max(w, panelEbay.getPreferredSize().width);
        w = Math.max(w, panelFTP.getPreferredSize().width);
        w += 50;
        Dimension d = new Dimension(w, h);

        super.dialog.setPreferredSize(d);
        super.dialog.setMinimumSize(d);
    }

    private void initButtonListeners() {
        this.btnClose.addActionListener((ActionEvent e) -> {
            try {
                EbayAccountEvent evt = new EbayAccountEvent(EbayAccountEvent.CANCEL, this);
                fireEvent(evt);
                fireEvent(e);
                dispose();
            } catch (Exception ex) {
                ex.printStackTrace(System.err);
            }
        });
        this.btnSave.addActionListener((ActionEvent e) -> {
            EbayAccountEvent evt = new EbayAccountEvent(EbayAccountEvent.CANCEL, this);
            fireEvent(evt);
            fireEvent(e);
        });
    }

    private void initSize(Component c) {
        setSizesPrefAsMinWithMaxWidth(c, c.getPreferredSize());
    }

    private void initDialog() {
        List<Image> imgs = new ArrayList<Image>();
        imgs.add(logo);
        super.dialog = new JDialog(new DummyFrame("Settings", imgs), true);
        initMinFormSize();
        super.dialog.setIconImage(logo);
        super.dialog.setTitle("Internet Sales Settings");
        super.dialog.getContentPane().add(panelMain);
        super.dialog.pack();
        super.dialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        super.dialog.setLocationRelativeTo(null);
    }

    private void initActionListeners() {
        ActionListener l = (ActionEvent e) -> {
            checkSaveEnabled();
        };

        this.generalInfoComponent.addActionListener(l);
        this.ebayApplicationComponent.addActionListener(l);
    }

    private void setButtonSize(JButton btn, Dimension sz) {
        btn.setPreferredSize(sz);
        btn.setMinimumSize(sz);
        btn.setMaximumSize(sz);
    }

    private void fireEvent(ActionEvent e) {
        List<ActionListener> listeners = new ArrayList<>(
                this.listeners.getListenerCount(ActionListener.class));
        for (ActionListener l : listeners) {
            l.actionPerformed(e);
        }
    }

    private void fireEvent(EbayAccountEvent e) {
        List<EbayAccountListener> listeners = new ArrayList<>(
                this.listeners.getListenerCount(EbayAccountListener.class));
        for (EbayAccountListener l : listeners) {
            l.accountRequest(e);
        }
    }

    private void buildCurrentConfiguration() {
        this.curConfig.setEbayAccounts(this.ebayListComponent.getAccounts());
        this.curConfig.setFTPInfo(this.fTPComponent.getFTPInfo());
        this.curConfig.setApplicationRunInfo(
                this.generalInfoComponent.getApplicationRunInfo());

    }

    private void checkSaveEnabled() {
        buildCurrentConfiguration();
        this.btnSave.setEnabled(!(configurationFileInfo.equalsObject(curConfig)));
    }
}
