/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import com.github.lespaul361.internetsales.gui.ebay.forms.EbaySecurityTokenDialog;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 * A component extending {@link AbstractAccountComponent} to add to an
 * {@link AbstractAccountList}
 */
public class EbayAccountComponent extends AbstractAccountComponent<EbayAccount> {

    private final Component parent;
    private final Icon icon;

    /**
     * Constructs a new EbayAccountComponent
     *
     * @param account the {@link EbayAccount}
     * @param logger the {@link Logger}
     * @param parent the parent {@link Component}
     * @param icon the {@link Icon}
     */
    public EbayAccountComponent(EbayAccount account, Logger logger,
            Component parent, Icon icon) {
        super(account, logger);
        btnDelete.setText(" Delete ");
        this.parent = parent;
        this.icon = icon;
    }

    @Override
    protected void btnClicked(ActionEvent e) {
        JButton btn = (JButton) e.getSource();
        if (btn.getText().toLowerCase().contains("update")) {
            String[] options = new String[]{" By Key ", " By Log In ", " Cancel "};
            int ret = JOptionPane.showOptionDialog(parent,
                    "How do you want to update?",
                    "Update Token",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    icon,
                    options,
                    options[2]);

            switch (ret) {
                case 0:
                    EbayAccountComponent cmp = null;
                    Component parent = (Component) e.getSource();
                    while (true) {
                        if (parent instanceof EbayAccountComponent) {
                            cmp = (EbayAccountComponent) parent;
                            break;
                        }
                        parent = parent.getParent();
                    }

                    EbaySecurityTokenDialog dlg = new EbaySecurityTokenDialog(
                            getLogger(), iconToImage(icon),
                            cmp.getAccount().getSecurityToken());
                    ret = dlg.showDialog();
                    if (ret == EbaySecurityTokenDialog.SAVE_SELECTED) {
                        int i=0;
                    } else {
                        break;
                    }
                case 1:

            }

        }
    }

    @Override
    protected void initButtons() {
        btnDelete = new JButton(" Undo Delete ");
        btnEdit = new JButton(" Update Key ");
    }

    private Image iconToImage(Icon icon) {
        if (icon instanceof ImageIcon) {
            return ((ImageIcon) icon).getImage();
        } else {
            BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
            icon.paintIcon(null, image.getGraphics(), 0, 0);
            return image;
        }
    }

}
