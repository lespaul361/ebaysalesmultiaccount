package com.github.lespaul361.internetsales.gui.forms;

/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import static com.github.lespaul361.commons.commonroutines.utilities.SetCursor.setDefaultCursor;
import static com.github.lespaul361.commons.commonroutines.utilities.SetCursor.setWaitCursor;
import java.awt.Dialog;
import java.awt.Window;
import java.util.logging.Logger;

/**
 * <p>
 * FrmProg class.
 * </p>
 *
 * @author Charles Hamilton
 * @version $Id: $Id
 */
public class ProgressForm extends AbstractForm {

    private javax.swing.JLabel jLabel1;
    private javax.swing.JProgressBar jProgressBar1;
    private JTextArea jTextOut;
    java.awt.Frame parentFrame = null;
    Window parentWindow = null;
    private final boolean isDisplayOutStream;

    /**
     * <p>
     * Constructor for FrmProg.
     * </p>
     *
     * @param parent a {@link java.awt.Frame} object.
     * @param modal a boolean.
     * @param logo the company logo
     * @param logger a {@link java.util.logging.Logger} for logging errors
     */
    public ProgressForm(java.awt.Frame parent, boolean modal, Image logo,
            Logger logger) {
        this(parent, modal, false, logo, logger);
    }

    /**
     * <p>
     * Constructor for FrmProg.
     * </p>
     *
     * @param parent a {@link java.awt.Frame} object.
     * @param modal a boolean.
     * @param displayOutStream a boolean.
     * @param logo the company logo
     * @param logger a {@link java.util.logging.Logger} for logging errors
     */
    public ProgressForm(java.awt.Frame parent, boolean modal, boolean displayOutStream,
            Image logo, Logger logger) {
        super(logo, logger);
        parentFrame = parent;
        isDisplayOutStream = displayOutStream;
        List<Image> imgs = new ArrayList<Image>();
        imgs.add(logo);// CreateImageFromResource.getImage("/logo.gif", ClassLoader.class));
        if (parent == null) {
            dialog = new JDialog(new DummyFrame("eBay Sales", imgs), modal);
        } else {
            dialog = new JDialog(parent, modal);
        }
        initComponents();
        dialog.setIconImage(imgs.get(0));
        dialog.setLocationRelativeTo(null);
        dialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

    }

    /**
     * <p>
     * Constructor for FrmProg.
     * </p>
     *
     * @param parent a {@link java.awt.Frame} object.
     * @param modal a boolean.
     * @param logo the company logo
     * @param logger a {@link java.util.logging.Logger} for logging errors
     */
    public ProgressForm(Window parent, boolean modal, Image logo,
            Logger logger) {
        this(parent, modal, false, logo, logger);
    }

    /**
     * <p>
     * Constructor for FrmProg.
     * </p>
     *
     * @param parent a {@link java.awt.Frame} object.
     * @param modal a boolean.
     * @param displayOutStream a boolean.
     * @param logo the company logo
     * @param logger a {@link java.util.logging.Logger} for logging errors
     */
    public ProgressForm(Window parent, boolean modal, boolean displayOutStream,
            Image logo, Logger logger) {
        super(logo, logger);
        parentWindow = parent;
        isDisplayOutStream = displayOutStream;
        List<Image> imgs = new ArrayList<Image>();
        imgs.add(logo);// CreateImageFromResource.getImage("/logo.gif", ClassLoader.class));
        if (parent == null) {
            dialog = new JDialog(new DummyFrame("eBay Sales", imgs), modal);
        } else {
            dialog = new JDialog(parent, Dialog.ModalityType.TOOLKIT_MODAL);
        }
        initComponents();
        dialog.setIconImage(imgs.get(0));
        dialog.setLocationRelativeTo(null);
        dialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

    }

    /**
     * <p>
     * getLabel.
     * </p>
     *
     * @return a {@link javax.swing.JLabel} object.
     */
    public JLabel getLabel() {
        return jLabel1;
    }

    /**
     * <p>
     * getProgress.
     * </p>
     *
     * @return a {@link javax.swing.JProgressBar} object.
     */
    public javax.swing.JProgressBar getProgress() {
        return jProgressBar1;
    }

    private int getInstanceCount(String line, String instance) {
        return line.split(instance).length;

    }

    /**
     * <p>
     * updateLabel.
     * </p>
     *
     * @param value a {@link java.lang.String} object.
     */
    public synchronized void updateLabel(final String value) {
        Runnable r = () -> {
            int lastCount = getInstanceCount(jLabel1.getText(), "<br>");
            if (value.contains("\n")) {
                String msg = "<html><body>{0}</body></html>";
                String tmp = "";
                String[] lines = value.split("\n");
                int total = lines.length;
                for (int i = 0; i < total; i++) {
                    tmp += lines[i];
                    if (i < total - 1) {
                        tmp += "<br>";
                    }
                }
                jLabel1.setText(MessageFormat.format(msg, tmp));
                if (lastCount != total) {
                    jLabel1.invalidate();
                    jLabel1.validate();
                    jLabel1.repaint();
                    //dialog.pack();
                }
            } else {
                jLabel1.setText(value);
                if (lastCount > 0) {
                    jLabel1.invalidate();
                    jLabel1.validate();
                    jLabel1.repaint();
                    //dialog.pack();
                }
            }
        };

        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }

    }

    /**
     * <p>
     * updateProgress.
     * </p>
     *
     * @param value a int.
     * @param total a int.
     */
    public synchronized void updateProgress(final int value, final int total) {
        SwingUtilities.invokeLater(() -> {
            if (jProgressBar1.isIndeterminate()) {
                jProgressBar1.setIndeterminate(false);
            }
            jProgressBar1.setMaximum(total);
            jProgressBar1.setValue(value);
        });
    }

    /**
     * <p>
     * showProgressBar.
     * </p>
     *
     * @param value a boolean.
     */
    public void showProgressBar(boolean value) {
        jProgressBar1.setVisible(value);
    }

    @Override
    public void setVisible(final boolean visible) {
        Thread t = new Thread(() -> {
            SwingUtilities.invokeLater(() -> {
                setWaitCursor(jLabel1);
            });

            if (!visible) {
                dialog.setVisible(false);
                if (dialog.getParent() instanceof DummyFrame) {
                    ((DummyFrame) dialog.getParent()).dispose();
                }
                SwingUtilities.invokeLater(() -> {
                    setDefaultCursor(jLabel1);
                });

            } else {
                if (parentFrame == null) {
                    List<Image> images = new ArrayList<>();
                    images.add(super.logo);
                    parentFrame = new DummyFrame("Progress", images);
                }
                parentFrame.setVisible(true);
                SwingUtilities.invokeLater(() -> {
                    setWaitCursor(jLabel1);
                });
                dialog.setVisible(visible);
            }
        });
        t.start();

    }

    /**
     * <p>
     * getParent.
     * </p>
     *
     * @return a {@link java.awt.Frame} object.
     */
    public java.awt.Frame getParent() {
        return parentFrame;
    }

    /**
     * Sets the <code>progressbar</code> to indeterminate or not indeterminate
     *
     * @param b a boolean value
     * @see javax.swing.JProgressBar#setIndeterminate(boolean)
     */
    public void setProgressIndeterminate(boolean b) {
        if (b == jProgressBar1.isIndeterminate()) {
            return;
        }

        if (!b) {
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeLater(() -> {
                    jProgressBar1.setIndeterminate(false);
                });
            } else {
                jProgressBar1.setIndeterminate(false);
            }

        } else {
            Thread t = new Thread(new IndeterminedProgressRunnable(this));
            t.start();
        }
    }

    /**
     * Gets if the <code>progressbar</code> is currently indeterminate or not
     * indeterminate
     *
     * @return a boolean value
     * @see javax.swing.JProgressBar#isIndeterminate()
     */
    public boolean isProgressIndeterminate() {
        return jProgressBar1.isIndeterminate();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Init Code">
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1.setText("Ready");

        JPanel pnl = new JPanel();
        pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
        pnl.add(Box.createVerticalStrut(15));
        Box boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(15));
        boxh.add(jLabel1);
        boxh.add(Box.createHorizontalStrut(15));
        boxh.add(Box.createHorizontalGlue());
        pnl.add(boxh);
        pnl.add(Box.createHorizontalStrut(10));
        jProgressBar1.setPreferredSize(new Dimension(300, 20));
        boxh = Box.createHorizontalBox();
        boxh.add(Box.createHorizontalStrut(15));
        boxh.add(jProgressBar1);
        boxh.add(Box.createHorizontalStrut(15));
        pnl.add(boxh);
        pnl.add(Box.createVerticalStrut(5));
        if (isDisplayOutStream) {
            jTextOut = new JTextArea();
            // jTextOut.setPreferredSize(new Dimension(500, 300));
            jTextOut.setAutoscrolls(true);
            JScrollPane pa = new JScrollPane(jTextOut);
            pa.setPreferredSize(new Dimension(300, 300));
            pa.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            pa.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            pnl.add(Box.createVerticalStrut(10));
            pnl.add(pa);
            class CustomOutputStream extends OutputStream {

                private JTextArea textArea;

                public CustomOutputStream(JTextArea textArea) {
                    this.textArea = textArea;
                }

                @Override
                public void write(int b) throws IOException {
                    // redirects data to the text area
                    textArea.setText(textArea.getText() + String.valueOf((char) b));
                    // scrolls the text area to the end of data
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    // keeps the textArea up to date
                    textArea.update(textArea.getGraphics());
                }
            }
            PrintStream printStream = new PrintStream(new CustomOutputStream(jTextOut));
            System.setOut(printStream);
            System.setErr(printStream);
        }
        dialog.getContentPane().add(pnl);
        dialog.pack();
        if (!isDisplayOutStream) {
            dialog.setResizable(false);
        }
    }// </editor-fold>

}
