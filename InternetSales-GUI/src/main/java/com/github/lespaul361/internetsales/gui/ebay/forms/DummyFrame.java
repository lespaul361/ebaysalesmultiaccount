/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;

/**
 * a dummy frame for being a parent to other frames so the child frame is
 * attached to an invisible frame with an icon
 */
class DummyFrame extends JFrame {

    /**
     * Constructs a new DummyFrame
     *
     * @param title the title for the frame
     * @param iconImages a list of icon images
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    DummyFrame(String title, List<? extends Image> iconImages) {
        super(title);
        setUndecorated(true);
        setVisible(true);
        setLocationRelativeTo(null);
        setIconImages(iconImages);
    }

    /**
     * Constructs a new DummyFrame
     *
     * @param title the title for the frame
     * @param iconImage an icon image
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    DummyFrame(String title, Image iconImage) {
        super(title);
        setUndecorated(true);
        setVisible(true);
        setLocationRelativeTo(null);
        List<? extends Image> iconImages = new ArrayList<>(
                Arrays.asList(new Image[]{iconImage}));
        setIconImages(iconImages);
    }
}
