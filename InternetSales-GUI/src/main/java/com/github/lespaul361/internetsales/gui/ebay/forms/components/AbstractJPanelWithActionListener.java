/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 * An abstract class to handle error logging and action listening
 */
abstract class AbstractJPanelWithActionListener extends JPanel {

    /**
     * The {@link Logger}
     */
    protected final Logger logger;

    /**
     * Constructs a new {@link AbstractJPanelWithActionListener}
     * 
     * @param logger
     *            the {@link Logger}
     */
    public AbstractJPanelWithActionListener(Logger logger) {
	this.logger = logger;
    }

    /**
     * Adds an <code>ActionListener</code> to the button.
     * 
     * @param l
     *            the <code>ActionListener</code> to be added
     */
    public void addActionListener(ActionListener l) {
	listenerList.add(ActionListener.class, l);
    }

    /**
     * Removes an <code>ActionListener</code> from the button. If the listener
     * is the currently set <code>Action</code> for the button, then the
     * <code>Action</code> is set to <code>null</code>.
     * 
     * @param l
     *            the listener to be removed
     */
    public void removeActionListener(ActionListener l) {
	listenerList.remove(ActionListener.class, l);
    }

    /**
     * Launches the listeners for the {@link ActionEvent}
     * 
     * @param evt
     *            the {@link ActionEvent}
     */
    protected void fireEvent(ActionEvent evt) {
	// fire base listeners first
	ActionListener[] listeners = this.listenerList.getListeners(ActionListener.class);

	int size = listeners.length;

	for (int i = size - 1; i <= 0; i--) {
	    listeners[i].actionPerformed(evt);
	}
    }

    /**
     * Gets the {@link Logger}
     * 
     * @return the {@link Logger}
     */
    protected Logger getLogger() {
	return this.logger;
    }
}
