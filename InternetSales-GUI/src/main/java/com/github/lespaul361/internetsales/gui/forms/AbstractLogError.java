/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.forms;

import com.github.lespaul361.internetsales.gui.events.LogExceptionEvent;
import com.github.lespaul361.internetsales.gui.events.LogExceptionListener;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Provides functionality for error logging
 */
public abstract class AbstractLogError implements LogError{

    private final Logger logger;
    private final List<LogExceptionListener> listenersList = new ArrayList<>(10);
    private final Image logo;

    /**
     * Constructs the AbstractLogError
     *
     * @param logger the {@link java.util.logging.Logger}
     * @param logo the logo
     */
    public AbstractLogError(Logger logger, Image logo) {
        this.logger = logger;
        this.logo = logo;
    }

    /**
     * Logs an error if the logger is not null otherwise it prints the stack
     * trace to the System.err stream
     *
     * @param level the {@link java.util.logging.Level}
     * @param msg the error message or other provided message
     * @param error the {@link Throwable}
     */
    protected void logError(Level level, String msg, Throwable error) {
        logError(level, false, null, msg, false, 0, error);
    }

    /**
     * Logs an error if the logger is not null otherwise it prints the stack
     * trace to the System.err stream
     *
     * @param level the {@link java.util.logging.Level}
     * @param showMessage to show a message with the error or not
     * @param parent a parent control or null
     * @param msg the error message or other provided message
     * @param error the {@link Throwable}
     */
    protected void logError(Level level, boolean showMessage, Frame parent,
            String msg, Throwable error) {
        logError(level, showMessage, parent, msg, false, 0, error);

    }

    /**
     * Logs an error if the logger is not null otherwise it prints the stack
     * trace to the System.err stream
     *
     * @param level the {@link java.util.logging.Level}
     * @param showMessage to show a message with the error or not
     * @param parent a parent control or null
     * @param msg the error message or other provided message
     * @param exit to exit the program or not
     * @param exitCode the error code to display if exit is true
     * @param error the {@link Throwable}
     */
    protected void logError(Level level, boolean showMessage, Frame parent,
            String msg, boolean exit, int exitCode, Throwable error) {
        if (logger != null) {
            logger.log(level, msg, error);
        } else {
            error.printStackTrace(System.err);
            System.err.println("No logger set");
        }

        if (showMessage) {
            String[] options = new String[]{" OK "};
            int ret = JOptionPane.showOptionDialog(parent,
                    getMessagePanel(msg == null ? error.getMessage() : msg), "Error",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
                    new ImageIcon(logo), options, options[0]);
        }
        
        LogExceptionEvent event=new LogExceptionEvent(error, level, exit, exitCode, 
                showMessage, parent, error);
        fireEvent(event);
        if(exit){
            System.exit(exitCode);
        }
    }

    @Override
    public void addLogExceptionListener(LogExceptionListener l) {
        this.listenersList.add(l);
    }

    @Override
    public void removeLogExceptionListener(LogExceptionListener l) {
        try {
            this.listenersList.remove(l);
        } catch (Exception e) {
        }
    }

    /**
     * Gets the {@link java.util.logging.Logger}
     *
     * @return the {@link java.util.logging.Logger}
     */
    protected Logger getLogger() {
        return logger;
    }

    /**
     * Launches the {@link LogExceptionListener}s
     *
     * @param evt the {@link LogExceptionEvent}
     */
    protected void fireEvent(LogExceptionEvent evt) {
        for (LogExceptionListener l : listenersList) {
            l.ExceptionLogged(evt);
        }
    }

    private JPanel getMessagePanel(String message) {
        JPanel main = new JPanel();
        main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
        JTextArea txt = new JTextArea(message);
        txt.setEditable(false);

        int w = 0;
        int h = 0;
        w = Math.min(300, txt.getPreferredSize().width);
        h = Math.max(400, txt.getPreferredSize().height);
        Dimension sz = new Dimension(w, h);
        txt.setPreferredSize(sz);
        txt.setMinimumSize(sz);
        JScrollPane scrollPane = new JScrollPane(txt);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(sz);
        scrollPane.setMinimumSize(sz);
        main.add(scrollPane);

        return main;
    }
}
