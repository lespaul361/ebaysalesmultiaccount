/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.forms;

import com.github.lespaul361.internetsales.gui.events.LogExceptionListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dialog;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Window;
import javax.management.NotificationListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

/**
 *
 * @author Charles Hamilton
 */
public abstract class AbstractWizard implements Wizard {

    protected final Dialog dialog;
    protected final Window parentWindow;
    protected final EventListenerList listenerList = new EventListenerList();
    protected final Image logo;
    private final JPanel pnlMain = new JPanel(new BorderLayout());
    private final JPanel pnlCard = new JPanel(new CardLayout());
    private final JButton btnNext = new JButton(" Next ");
    private final JButton btnCancel = new JButton(" Cancel ");
    private final JButton btnPrevious = new JButton(" Previous ");

    public AbstractWizard(Window parentWindow, String title) {
        this(parentWindow, title, null);
    }

    public AbstractWizard(Window parentWindow, String title, Panel... tabs) {
        this(parentWindow, title, null, tabs);
    }

    public AbstractWizard(Window parentWindow, String title, Image image, Panel... tabs) {
        this.parentWindow = parentWindow;
        this.dialog = new JDialog(parentWindow, title);
        this.addTabs(tabs);
        this.logo = image;
    }

    @Override
    public void removeLogExceptionListener(LogExceptionListener l) {
        listenerList.remove(LogExceptionListener.class, l);
    }

    @Override
    public void addLogExceptionListener(LogExceptionListener l) {
        listenerList.add(LogExceptionListener.class, l);
    }

    @Override
    public void addNotificationListener(NotificationListener l) {
        listenerList.add(NotificationListener.class, l);
    }

    @Override
    public void removeNotificationListener(NotificationListener l) {
        listenerList.remove(NotificationListener.class, l);
    }

    @Override
    public void addTab(Panel panel) {
        this.addTabs(panel);
    }

    @Override
    public void addTabs(Panel... panel) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeTab(Panel panel) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
