/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.forms;

import com.github.lespaul361.internetsales.gui.events.LogExceptionListener;

/**
 * 
 * @author Charles Hamilton
 */
public interface LogError {

    /**
     * Removes a
     * {@link com.github.lespaul361.internetsales.gui.events.LogExceptionListener}
     * from the {@link java.util.List}
     * 
     * @param l
     *            the
     *            {@link com.github.lespaul361.internetsales.gui.events.LogExceptionListener}
     */
    public void removeLogExceptionListener(LogExceptionListener l);

    /**
     * Adds a
     * {@link com.github.lespaul361.internetsales.gui.events.LogExceptionListener}
     * to the {@link java.util.List}
     * 
     * @param l
     *            the
     *            {@link com.github.lespaul361.internetsales.gui.events.LogExceptionListener}
     */
    public void addLogExceptionListener(LogExceptionListener l);

}
