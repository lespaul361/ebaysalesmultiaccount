/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.events;

import java.util.EventObject;

/**
 * An event to for requesting Ebay account information for the Ebay components
 */
public class EbayAccountEvent extends EventObject {

    /**
     * Requests adding an account by Ebay log in
     */
    public final static int REQUEST_ADD_ACCOUNT = 0;

    /**
     * Requests adding an account by Ebay security token
     */
    public final static int REQUEST_ADD_KEY = 1;
    /**
     * Sends a cancel command to stop any thread and terminate adding an account
     */
    public final static int CANCEL = 2;

    private final int accountRequest;

    /**
     * Constructs a new EbayAccountEvent
     * 
     * @param accountRequest
     *            Sets the request type of REQUEST_ADD_ACCOUNT or
     *            REQUEST_ADD_KEY May throw {@link IllegalArgumentException} if
     *            accountRequest is out or range
     * @param source
     *            the source of the event
     */
    public EbayAccountEvent(int accountRequest, Object source) {
	super(source);
	if (accountRequest < 0 || accountRequest > 2) {
	    RuntimeException re = new IllegalArgumentException("Invalid request type");
	    throw re;
	}
	this.accountRequest = accountRequest;
    }

    /**
     * Gets the account request type of REQUEST_ADD_ACCOUNT or REQUEST_ADD_KEY
     * 
     * @return the accountRequest
     */
    public int getAccountRequest() {
	return accountRequest;
    }

}
