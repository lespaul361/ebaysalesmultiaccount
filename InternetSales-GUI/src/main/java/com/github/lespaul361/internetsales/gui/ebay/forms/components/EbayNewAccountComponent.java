/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import static com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities.setLargestButtonSize;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountEvent;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * A component to show buttons to get a new
 * {@link com.github.lespaul361.internetsales.configurationfile.EbayAccount}
 */
public class EbayNewAccountComponent extends JPanel implements EbayEventListenerHelper {

    private final EbayEventListenerDelegate listenerDelegate = new EbayEventListenerDelegate();
    /**
     * The button to send start getting account from a security token
     */
    
    protected JButton btnNewEbayKey = null;
    /**
     * The button to send start getting account from logging in
     */
    protected JButton btnNewEbayLogIn = null;

    /**
     * Constructs a new EbayNewAccountComponent
     */
    public EbayNewAccountComponent() {
        init();
    }

    @Override
    public void addEbayAccountListener(EbayAccountListener l) {
        listenerDelegate.addEbayAccountListener(l);
    }

    @Override
    public void removeEbayAccountListener(EbayAccountListener l) {
        listenerDelegate.removeEbayAccountListener(l);
    }

    private void init() {
        initButtons();
        initButtonListener();
        initMain();
    }

    private void initButtons() {
        btnNewEbayLogIn = new JButton(" New From Login ");
        btnNewEbayKey = new JButton(" New From Key ");

        setLargestButtonSize(btnNewEbayKey, btnNewEbayLogIn);

    }

    private void initButtonListener() {
        btnNewEbayLogIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EbayAccountEvent evt = new EbayAccountEvent(
                        EbayAccountEvent.REQUEST_ADD_ACCOUNT,
                        EbayNewAccountComponent.this);
                listenerDelegate.fireEvents(evt);
            }
        });

        btnNewEbayKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EbayAccountEvent evt = new EbayAccountEvent(
                        EbayAccountEvent.REQUEST_ADD_KEY,
                        EbayNewAccountComponent.this);
                listenerDelegate.fireEvents(evt);
            }
        });
    }

    private void initMain() {
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.add(Box.createHorizontalGlue());
        this.add(btnNewEbayKey);
        this.add(Box.createHorizontalStrut(10));
        this.add(btnNewEbayLogIn);
        this.add(Box.createHorizontalStrut(10));
    }

}
