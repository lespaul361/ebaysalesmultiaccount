/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities;
import static com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities.createBox;
import com.github.lespaul361.commons.commonroutines.textfieldcontextmenu.DefaultContextMenu;
import com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.TransferHandler;
import static javax.swing.TransferHandler.COPY;
import javax.swing.border.TitledBorder;

/**
 * A component to hold information for editing the Ebayapplication information
 */
public class EbayApplicationComponent extends AbstractJPanelWithActionListener
        implements EbayEventListenerHelper {

    /**
     * The {@link JTextField} holding the AppID string
     */
    protected JTextField txtAppId = new JTextField();
    /**
     * The {@link JTextField} holding the CertificateID string
     */
    protected JTextField txtCertId = new JTextField();
    /**
     * The {@link JTextField} holding the DeveloperID string
     */
    protected JTextField txtDevId = new JTextField();
    /**
     * The {@link JTextField} holding the RuName string
     */
    protected JTextField txtRU = new JTextField();

    /**
     * The {@link JButton} to save the info
     */
    protected JButton btnSaveEbayAccountInfo = new JButton(" Save ");
    
    private boolean showButton;
    private final boolean showBorder;
    
    private EbayApplicationInfo info;
    private final EbayEventListenerDelegate listenerDelegate = new EbayEventListenerDelegate();
    private final DefaultContextMenu textContextMenu = new DefaultContextMenu(false);
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
    public static final String PROP_SHOWBUTTON = "showButton";
    public static final String PROP_FIELD_CHANGE = "fieldValueChange";

    /**
     * Constructs a new EbayApplicationComponent from the provided
     * {@link com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo}
     *
     * @param info the
     * {@link com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo}
     * @param logger the {@link Logger}
     */
    public EbayApplicationComponent(EbayApplicationInfo info, Logger logger) {
        this(info, true, true, logger);
    }

    /**
     * Constructs a new EbayApplicationComponent from the provided
     * {@link com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo}
     *
     * @param info the
     * {@link com.github.lespaul361.internetsales.configurationfile.EbayApplicationInfo}
     * @param showButton to show the button or not
     * @param showBorder to show the border or not
     * @param logger the {@link Logger}
     */
    public EbayApplicationComponent(EbayApplicationInfo info, boolean showButton,
            boolean showBorder, Logger logger) {
        super(logger);
        this.info = info;
        this.showBorder = showBorder;
        this.showButton = showButton;
        init();
    }

    /**
     * Gets the updated {@link EbayApplicationInfo}
     *
     * @return the EbayApplicationInfo
     */
    public EbayApplicationInfo getEbayApplicationInfo() {
        EbayApplicationInfo ret = this.info.copy();
        ret.setAppId(this.txtAppId.getText().trim());
        ret.setCertificateId(this.txtCertId.getText().trim());
        ret.setRuName(this.txtRU.getText().trim());
        ret.setDeveloperName(this.txtDevId.getText().trim());
        
        return ret;
    }
    
    @Override
    public void addEbayAccountListener(EbayAccountListener l) {
        listenerDelegate.addEbayAccountListener(l);
    }
    
    @Override
    public void removeEbayAccountListener(EbayAccountListener l) {
        listenerDelegate.removeEbayAccountListener(l);
    }
    
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    @Override
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }
    
    @Override
    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }
    
    public void setAppID(String value) {
        String oldValue = this.txtAppId.getText();
        this.txtAppId.setText(value);
        this.propertyChangeSupport.firePropertyChange(
                PROP_FIELD_CHANGE, oldValue, value);
    }
    
    public void setCertificateID(String value) {
        String oldValue = this.txtCertId.getText();
        this.txtCertId.setText(value);
        this.propertyChangeSupport.firePropertyChange(
                PROP_FIELD_CHANGE, oldValue, value);
    }
    
    public void setDeveloperID(String value) {
        String oldValue = this.txtDevId.getText();
        this.txtDevId.setText(value);
        this.propertyChangeSupport.firePropertyChange(
                PROP_FIELD_CHANGE, oldValue, value);
    }
    
    public void setRUID(String value) {
        String oldValue = this.txtRU.getText();
        this.txtRU.setText(value);
        this.propertyChangeSupport.firePropertyChange(
                PROP_FIELD_CHANGE, oldValue, value);
    }
    
    public String getAppID() {
        return this.txtAppId.getText();
    }
    
    public String getCertificateID() {
        return this.txtCertId.getText();
    }
    
    public String getDeveloperID() {
        return txtDevId.getText();
    }
    
    public String getRUID() {
        return txtRU.getText();
    }
    
    private void init() {
        initMain();
        initValues();
        initTextFieldEnhancements();
        initListener();
        if (showBorder) {
            this.setBorder(new TitledBorder("Ebay Application Settings"));
        }
    }
    
    private void initMain() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        int vStrut = 10;
        int hStrut = 5;
        
        JLabel lblAppId = new JLabel("App ID: ");
        JLabel lblCertID = new JLabel("Certificate ID: ");
        JLabel lblDevID = new JLabel("Developer ID: ");
        JLabel lblRU = new JLabel("RU Name: ");
        
        Dimension szTextBox = new Dimension(150, lblCertID.getPreferredSize().height + 4);
        Dimension szMax = new Dimension(Integer.MAX_VALUE, szTextBox.height);
        Dimension szMin = new Dimension(150, szTextBox.height);
        
        int lenAppID = lblAppId.getPreferredSize().width;
        int lenCertID = lblCertID.getPreferredSize().width;
        int lenDevID = lblDevID.getPreferredSize().width;
        int lenRU = lblRU.getPreferredSize().width;
        
        this.add(Box.createVerticalStrut(vStrut));
        this.add(createBox(lblAppId, szTextBox, szMax, szMin, hStrut, lenCertID - lenAppID, txtAppId));
        this.add(Box.createVerticalStrut(vStrut));
        this.add(createBox(lblCertID, szTextBox, szMax, szMin, hStrut, 0, txtCertId));
        this.add(Box.createVerticalStrut(vStrut));
        this.add(createBox(lblDevID, szTextBox, szMax, szMin, hStrut, lenCertID - lenDevID, txtDevId));
        this.add(Box.createVerticalStrut(vStrut));
        this.add(createBox(lblRU, szTextBox, szMax, szMin, hStrut, lenCertID - lenRU, txtRU));
        this.add(Box.createVerticalStrut(vStrut));
        Box b = Box.createHorizontalBox();
        b.add(Box.createHorizontalStrut(lenCertID + txtAppId.getPreferredSize().width));
        b.add(Box.createHorizontalGlue());
        if (isShowButton()) {
            BoxUtilities.setSize(btnSaveEbayAccountInfo, btnSaveEbayAccountInfo.getPreferredSize());
            b.add(btnSaveEbayAccountInfo);
            b.add(Box.createHorizontalStrut(hStrut));
        }
        this.add(b);
        this.add(Box.createVerticalStrut(vStrut));
        
        this.btnSaveEbayAccountInfo.setEnabled(false);
        
        this.setTransferHandler(new FileTransferHandler("dat"));
        
    }
    
    private void initValues() {
        this.txtAppId.setText((this.info == null ? "" : this.info.getAppId()));
        this.txtCertId.setText((this.info == null ? "" : this.info.getCertificateId()));
        this.txtDevId.setText((this.info == null ? "" : this.info.getDeveloperName()));
        this.txtRU.setText((this.info == null ? "" : this.info.getRuName()));
    }
    
    private void initTextFieldEnhancements() {
        textContextMenu.add(txtAppId);
        textContextMenu.add(txtCertId);
        textContextMenu.add(txtDevId);
        textContextMenu.add(txtRU);
        
        txtAppId.setDragEnabled(true);
        txtCertId.setDragEnabled(true);
        txtDevId.setDragEnabled(true);
        txtRU.setDragEnabled(true);
        txtAppId.setTransferHandler(new TextFieldTransferHandler());
        txtCertId.setTransferHandler(new TextFieldTransferHandler());
        txtDevId.setTransferHandler(new TextFieldTransferHandler());
        txtRU.setTransferHandler(new TextFieldTransferHandler());
    }
    
    private void initListener() {
        class TextKeyListener implements KeyListener {
            
            private volatile String lastValue = "";
            
            public TextKeyListener() {
            }
            
            public TextKeyListener(String val) {
                lastValue = (val == null) ? "" : val;
            }
            
            @Override
            public void keyTyped(KeyEvent e) {
            }
            
            @Override
            public void keyPressed(KeyEvent e) {
            }
            
            @Override
            public void keyReleased(KeyEvent e) {
                String newValue = ((JTextField) e.getComponent()).getText();
                propertyChangeSupport.firePropertyChange(PROP_FIELD_CHANGE, lastValue, newValue);
                lastValue = newValue;
                checkSaveButton();
            }
        }
        
        btnSaveEbayAccountInfo.addActionListener((ActionEvent e) -> {
            ActionEvent evt = new ActionEvent(EbayApplicationComponent.this,
                    e.getID(), e.getActionCommand());
            fireEvent(evt);
        });
        
        txtAppId.addKeyListener(new TextKeyListener(txtAppId.getText()));
        txtCertId.addKeyListener(new TextKeyListener(txtCertId.getText()));
        txtDevId.addKeyListener(new TextKeyListener(txtDevId.getText()));
        txtRU.addKeyListener(new TextKeyListener(txtRU.getText()));
    }
    
    private void checkSaveButton() {
        if (txtAppId.getText().trim().equalsIgnoreCase("")
                || txtCertId.getText().trim().equalsIgnoreCase("")
                || txtDevId.getText().trim().equalsIgnoreCase("")
                || txtRU.getText().trim().equalsIgnoreCase("")) {
            setShowButton(false);
            return;
        }
        if (this.info != null) {
            if (txtAppId.getText().trim().equalsIgnoreCase(this.info.getAppId())
                    && txtCertId.getText().trim().equalsIgnoreCase(this.info.getCertificateId())
                    && txtDevId.getText().trim().equalsIgnoreCase(this.info.getDeveloperName())
                    && txtRU.getText().trim().equalsIgnoreCase(this.info.getRuName())) {
                setShowButton(false);
                return;
            }
        }
        
        setShowButton(true);
    }

    /**
     * @return the showButton
     */
    public boolean isShowButton() {
        return showButton;
    }

    /**
     * @param showButton the showButton to set
     */
    public void setShowButton(boolean showButton) {
        boolean oldShowButton = this.showButton;
        this.showButton = showButton;
        propertyChangeSupport.firePropertyChange(PROP_SHOWBUTTON, oldShowButton, showButton);
        this.btnSaveEbayAccountInfo.setEnabled(showButton);
    }
    
}

class TextFieldTransferHandler extends TransferHandler {
    
    private String fileExtension;
    
    public TextFieldTransferHandler() {
        this("");
    }
    
    public TextFieldTransferHandler(String fileExtension) {
        if (fileExtension.trim().equalsIgnoreCase("")) {
            fileExtension = "";
        } else {
            this.fileExtension = fileExtension.trim();
        }
    }
    
    @Override
    public boolean canImport(TransferHandler.TransferSupport support) {
        // only support drops (not clipboard paste)
        if (!support.isDrop()) {
            return false;
        }
        support.setDropAction(COPY);
        
        return support.isDataFlavorSupported(DataFlavor.stringFlavor);
        
    }
    
    @Override
    public boolean importData(TransferSupport support) {
        if (!canImport(support)) {
            return false;
        }
        
        Transferable transferable = support.getTransferable();
        
        if (isStringTransfer(support)) {
            return transferString(support);
        }
        return transferFile(support);
    }
    
    private boolean transferString(TransferSupport support) {
        String line = null;
        try {
            Transferable transferable = support.getTransferable();
            line = (String) transferable.getTransferData(DataFlavor.stringFlavor);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }
        
        JTextField fld = (JTextField) support.getComponent();
        fld.setText(line);
        
        return true;
    }
    
    private boolean transferFile(TransferSupport support) {
        try {
            Transferable transferable = support.getTransferable();
            List<java.io.File> files = (List<java.io.File>) transferable
                    .getTransferData(DataFlavor.javaFileListFlavor);
            files.forEach((File f) -> {
                System.out.println(f.getAbsoluteFile());
            });
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }
        
        return true;
    }
    
    private boolean isStringTransfer(TransferSupport support) {
        try {
            Transferable transferable = support.getTransferable();
            String line = (String) transferable.getTransferData(DataFlavor.stringFlavor);
            return true;
        } catch (Exception e) {
        }
        
        return false;
    }
}

class FileTransferHandler extends TransferHandler {
    
    private String fileExtension;
    
    public FileTransferHandler() {
        this("");
    }
    
    public FileTransferHandler(String fileExtension) {
        if (fileExtension.trim().equalsIgnoreCase("")) {
            fileExtension = "";
        } else {
            this.fileExtension = fileExtension.trim();
        }
    }
    
    @Override
    public boolean canImport(TransferHandler.TransferSupport support) {
        // only support drops (not clipboard paste)
        if (!support.isDrop()) {
            return false;
        }
        support.setDropAction(COPY);
        
        if (!support.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            return false;
        }
        try {
            List<File> files = (List<File>) support.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
            if (files.size() != 1) {
                return false;
            }
            if (files.get(0).getAbsolutePath().toLowerCase().endsWith(fileExtension.toLowerCase())) {
                return true;
            }
        } catch (UnsupportedFlavorException ex) {
            Logger.getLogger(FileTransferHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileTransferHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    @Override
    public boolean importData(TransferHandler.TransferSupport support) {
        if (!canImport(support)) {
            return false;
        }
        
        Transferable transferable = support.getTransferable();
        
        return transferFile(support);
    }
    
    private boolean transferFile(TransferHandler.TransferSupport support) {
        try {
            Transferable transferable = support.getTransferable();
            List<java.io.File> files = (List<java.io.File>) transferable
                    .getTransferData(DataFlavor.javaFileListFlavor);
            files.forEach((File f) -> {
                System.out.println(f.getAbsoluteFile());
            });
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }
        
        return true;
    }
    
    private boolean isStringTransfer(TransferHandler.TransferSupport support) {
        try {
            Transferable transferable = support.getTransferable();
            String line = (String) transferable.getTransferData(DataFlavor.stringFlavor);
            return true;
        } catch (Exception e) {
        }
        
        return false;
    }
}
