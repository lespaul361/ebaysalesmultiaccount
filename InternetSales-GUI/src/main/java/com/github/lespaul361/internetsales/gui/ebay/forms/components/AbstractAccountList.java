/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import com.github.lespaul361.internetsales.configurationfile.Account;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.border.TitledBorder;

public abstract class AbstractAccountList<T extends Account, C extends AbstractAccountComponent> 
        extends AbstractJPanelWithActionListener {

    private final Logger logger;
    private final String borderTitle;

    protected List<T> accounts = new ArrayList<>();
    protected List<C> accountComponents = new ArrayList<>();

    protected JScrollPane scrollPane;
    protected JPanel scrollPanel;

    private final int vStrut = 15;
    private final int hStrut = 10;

    public AbstractAccountList(Logger logger, String borderTitle) {
        super(logger);
        this.logger = logger;
        this.borderTitle = borderTitle;
        init();
    }

    protected void logError(Level level, String msg, Throwable error) {
        logger.log(level, msg, error);
    }

    public Logger getLogger() {
        return this.logger;
    }

    protected void init() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.scrollPanel = new JPanel();
        this.scrollPanel.setLayout(new BoxLayout(scrollPanel, BoxLayout.Y_AXIS));
        this.scrollPane = new JScrollPane(this.scrollPanel);
        this.scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        this.add(Box.createHorizontalStrut(hStrut));
        this.add(Box.createHorizontalGlue());
        this.add(scrollPane);
        this.add(Box.createHorizontalGlue());
        this.add(Box.createHorizontalStrut(hStrut));
        this.setBorder(new TitledBorder(borderTitle));
    }

    abstract public void addAccount(T account);

    abstract public void addAccountComponent(C component);

    public List<T> getAccounts() {
        return accounts;
    }

    protected void updateComponents() {
        this.scrollPanel.removeAll();
        List<Component> components = (List<Component>) (List<?>) this.accountComponents;
        int h, w = 0;

        this.scrollPanel.add(Box.createVerticalStrut(vStrut));

        for (JComponent c : this.accountComponents) {
            w = Math.max(w, c.getPreferredSize().width);
            Box boxH = Box.createHorizontalBox();
            boxH.add(Box.createHorizontalStrut(hStrut));
            c.setMinimumSize(c.getPreferredSize());
            c.setMaximumSize(c.getPreferredSize());
            boxH.add(c);
            boxH.add(Box.createHorizontalStrut(hStrut));
            boxH.add(Box.createHorizontalGlue());
            this.scrollPanel.add(boxH);
            this.scrollPanel.add(Box.createHorizontalGlue());
            this.scrollPanel.add(Box.createVerticalStrut(vStrut));
        }

        Dimension sz = new Dimension();
        h = this.accountComponents.get(0).getPreferredSize().height * 2;
        h += vStrut * 3;
        sz.width = w;
        sz.height = h;
        this.scrollPanel.setMinimumSize(sz);
        this.scrollPanel.setPreferredSize(sz);
        sz = new Dimension(sz);
        sz.width = Integer.MAX_VALUE;
        this.setMaximumSize(sz);
    }
}
