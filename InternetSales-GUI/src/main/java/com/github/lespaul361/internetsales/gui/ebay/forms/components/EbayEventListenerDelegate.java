/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms.components;

import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountEvent;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener;
import javax.swing.event.EventListenerList;

/**
 * A class used to delegate {@link EbayAccountListener}
 */
public class EbayEventListenerDelegate implements EbayEventListenerHelper {

    /**
     * the {@link javax.swing.event.EventListenerList}
     */
    EventListenerList listeners = new EventListenerList();

    @Override
    public void addEbayAccountListener(EbayAccountListener l) {
        listeners.add(EbayAccountListener.class, l);
    }

    @Override
    public void removeEbayAccountListener(EbayAccountListener l) {
        listeners.remove(EbayAccountListener.class, l);
    }

    /**
     * Has the
     * {@link com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener}s
     * send the event
     *
     * @param evt the
     * {@link com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountEvent}
     *
     */
    void fireEvents(EbayAccountEvent evt) {
        EbayAccountListener[] listeners = this.listeners.getListeners(EbayAccountListener.class);
        for (int i = listeners.length - 1; i >= 0; i--) {
            listeners[i].accountRequest(evt);
        }
    }

}
