/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui;

import com.github.lespaul361.internetsales.gui.forms.AbstractLogError;
import com.github.lespaul361.commons.commonroutines.utilities.MessageRoutines;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * Creates and adds the InternetSalesTrayIcon
 */
public class InternetSalesTrayIcon extends AbstractLogError {

    private final Image logo;
    private final String toolTip;
    private MenuItem mnuExit = new MenuItem("Exit");
    private MenuItem mnuSettings = new MenuItem("Settings...");
    private List<ActionListener> listeners = new ArrayList<>(5);
    private TrayIcon ti = null;

    /**
     * Constructs a new EbaySalesTrayIcon object
     *
     * @param logo the logo image
     * @param toolTip The tool tip text to display
     * @param logger the {@link java.util.logging.Logger} for the program
     */
    public InternetSalesTrayIcon(Image logo, String toolTip, Logger logger) {
        super(logger,logo);
        this.logo = logo;
        this.toolTip = toolTip;
        SwingUtilities.invokeLater(() -> {
            init();
        });
    }

    /**
     * Adds an {@link java.awt.event.ActionListener}
     *
     * @param l the {@link java.awt.event.ActionListener}
     */
    public void AddActionListener(ActionListener l) {
        this.listeners.add(l);
    }

    /**
     * Gets the {@link java.awt.TrayIcon}
     *
     * @return the {@link java.awt.TrayIcon}
     */
    public TrayIcon getTrayIcon() {
        return this.ti;
    }

    private void init() {
        PopupMenu pop = new PopupMenu();
        ActionListener listener = (ActionEvent e) -> {
            fireActionEvent(e);
        };
        mnuExit.setShortcut(new MenuShortcut(KeyEvent.VK_X));
        mnuExit.addActionListener(listener);
        mnuSettings.setShortcut(new MenuShortcut(KeyEvent.VK_S));
        mnuSettings.addActionListener(listener);

        pop.add(mnuSettings);
        pop.addSeparator();
        pop.add(mnuExit);

        TrayIcon trayIcon = new TrayIcon(logo, this.toolTip);
        trayIcon.setPopupMenu(pop);

        SystemTray tray = SystemTray.getSystemTray();
        try {
            tray.add(trayIcon);
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
            MessageRoutines.showOkOnly(e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE, null);
            System.exit(1);
        }
        this.ti = trayIcon;
    }

    /**
     * Sends the {@link java.awt.event.ActionEvent} to the
     * {@link java.awt.event.ActionListener}s
     *
     * @param e the {@link java.awt.event.ActionEvent}
     */
    protected void fireActionEvent(ActionEvent e) {
        for (ActionListener l : this.listeners) {
            l.actionPerformed(e);
        }
    }

}
