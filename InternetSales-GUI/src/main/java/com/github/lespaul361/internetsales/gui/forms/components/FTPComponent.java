/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.forms.components;

import static com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities.createBox;
import com.github.lespaul361.commons.commonroutines.textfieldcontextmenu.DefaultContextMenu;
import com.github.lespaul361.internetsales.configurationfile.FTPInfo;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Charles Hamilton
 */
public class FTPComponent extends JPanel {

    private final FTPInfo info;
    protected final JTextField txtIP = new JTextField();
    protected final JTextField txtUserName = new JTextField();
    protected JTextField txtPassword = new JTextField();
    protected JButton btnSaveFTPInfo = new JButton(" Save");
    private final DefaultContextMenu textContextMenu = new DefaultContextMenu(false);

    public FTPComponent(FTPInfo ftpInfo) {
        this.info = ftpInfo;
        this.btnSaveFTPInfo.setEnabled(false);
        init();
    }

    public void addActionListener(ActionListener l) {
        listenerList.add(ActionListener.class, l);
    }

    public void removeActionListener(ActionListener l) {
        listenerList.remove(ActionListener.class, l);
    }

    /**
     * Gets the updated {@link FTPInfo}
     *
     * @return the FTPInfo
     */
    public FTPInfo getFTPInfo() {
        this.info.setFTPIP(this.txtIP.getText().trim());
        this.info.setFTPPassword(this.txtPassword.getText().trim());
        this.info.setFTPUserName(this.txtUserName.getText().trim());

        return this.info;
    }

    private void init() {
        initMainPanel();
        initValues();
        initTextFieldEnhancements();
        initSaveListener();
    }

    private void initMainPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        int vStrut = 10;
        int hStrut = 25;

        JLabel lblAddress = new JLabel("FTP IP Address: ");
        JLabel lblUserName = new JLabel("User Name: ");
        JLabel lblPassword = new JLabel("Password: ");

        Dimension szTextBox = new Dimension(150, lblAddress.getPreferredSize().height + 4);
        Dimension szMax = new Dimension(Integer.MAX_VALUE, szTextBox.height);
        Dimension szMin = new Dimension(50, szTextBox.height);

        int lenAddress = lblAddress.getPreferredSize().width;
        int lenUserName = lblUserName.getPreferredSize().width;
        int lenPassword = lblPassword.getPreferredSize().width;

        Box boxH = createBox(lblAddress, szTextBox, szMax, szMin, hStrut, 0, txtIP);
        this.add(Box.createVerticalStrut(20));
        this.add(boxH);
        boxH = createBox(lblUserName, szTextBox, szMax, szMin, hStrut,
                lenAddress - lenUserName, txtUserName);
        this.add(Box.createVerticalStrut(vStrut));
        this.add(boxH);
        boxH = createBox(lblPassword, szTextBox, szMax, szMin, hStrut,
                lenAddress - lenPassword, txtPassword);
        this.add(Box.createVerticalStrut(vStrut));
        this.add(boxH);
        this.add(Box.createVerticalStrut(vStrut));
        boxH = Box.createHorizontalBox();
        boxH.add(Box.createHorizontalStrut(lenAddress
                + txtIP.getPreferredSize().width));
        boxH.add(Box.createHorizontalGlue());
        boxH.add(btnSaveFTPInfo);
        boxH.add(Box.createHorizontalStrut(hStrut));
        this.add(boxH);

        this.add(Box.createVerticalStrut(vStrut));
        this.setBorder(new TitledBorder("FTP Settings"));
    }

    private void initValues() {
        this.txtIP.setText(this.info.getFTPIP());
        this.txtPassword.setText(this.info.getFTPPassword());
        this.txtUserName.setText(this.info.getFTPUserName());
    }

    private void initTextFieldEnhancements() {
        textContextMenu.add(txtIP);
        textContextMenu.add(txtPassword);
        textContextMenu.add(txtUserName);

        txtIP.setDragEnabled(true);
        txtPassword.setDragEnabled(true);
        txtUserName.setDragEnabled(true);
    }

    private void initSaveListener() {
        btnSaveFTPInfo.addActionListener((ActionEvent e) -> {
            ActionEvent evt = new ActionEvent(FTPComponent.this, e.getID(),
                    e.getActionCommand());
            fireEvent(evt);
        });
    }

    private void initTextKeyListeners() {
        KeyListener kl = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                checkSaveButton();
            }
        };
        txtIP.addKeyListener(kl);
        txtPassword.addKeyListener(kl);
        txtUserName.addKeyListener(kl);
    }

    private void checkSaveButton() {
        if (txtIP.getText().trim().equalsIgnoreCase("")
                || txtPassword.getText().trim().equalsIgnoreCase("")
                || txtUserName.getText().trim().equalsIgnoreCase("")) {
            btnSaveFTPInfo.setEnabled(false);
            return;
        }

        if (txtIP.getText().trim().equalsIgnoreCase(this.info.getFTPIP())
                && txtPassword.getText().trim().equals(this.info.getFTPPassword())
                && txtUserName.getText().trim().equals(this.info.getFTPUserName())) {
            btnSaveFTPInfo.setEnabled(false);
            return;
        }

        btnSaveFTPInfo.setEnabled(true);
    }

    private void fireEvent(ActionEvent evt) {
        ActionListener[] listeners = listenerList.getListeners(ActionListener.class
        );
        for (ActionListener listener : listeners) {
            listener.actionPerformed(evt);
        }
    }
}
