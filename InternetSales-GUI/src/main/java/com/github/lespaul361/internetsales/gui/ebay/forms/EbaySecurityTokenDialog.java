/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.ebay.forms;

import com.github.lespaul361.commons.commonroutines.textfieldcontextmenu.DefaultContextMenu;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayEventListenerDelegate;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayEventListenerHelper;
import com.github.lespaul361.internetsales.gui.forms.AbstractLogError;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Charles Hamilton
 */
public class EbaySecurityTokenDialog extends AbstractLogError implements EbayEventListenerHelper {

    private final EbayEventListenerDelegate listenerDelegate = new EbayEventListenerDelegate();
    private final JTextArea txtKey = new JTextArea();
    private final JButton btnSaveToken = new JButton(" Save ");
    private final JButton btnCancelToken = new JButton(" Cancel ");
    private final String oldToken;
    public static final int CANCEL_SELECTED = 0;
    public static final int SAVE_SELECTED = 1;
    private final Image logo;
    private volatile int value = -1;

    public EbaySecurityTokenDialog(Logger logger, Image logo) {
        this(logger, logo, null);
    }

    public EbaySecurityTokenDialog(Logger logger, Image logo, String oldString) {
        super(logger, logo);
        this.logo = logo;
        oldToken = oldString;
    }

    public int showDialog() {
        JButton[] options = new JButton[]{btnSaveToken, btnCancelToken};

        btnCancelToken.addActionListener((ActionEvent e) -> {
            JOptionPane pane = getOptionPane((JComponent) e.getSource());
            pane.setValue(CANCEL_SELECTED);
            value = CANCEL_SELECTED;
        });

        btnSaveToken.addActionListener((ActionEvent e) -> {
            JOptionPane pane = getOptionPane((JComponent) e.getSource());
            pane.setValue(SAVE_SELECTED);
            value = SAVE_SELECTED;
        });

        txtKey.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (txtKey.getText() != null) {
                    if (oldToken == null || oldToken.trim().equalsIgnoreCase("")) {
                        if (txtKey.getText().length() > 0) {
                            btnSaveToken.setEnabled(true);
                            return;
                        } else {
                            btnSaveToken.setEnabled(false);
                            return;
                        }
                    } else {
                        if (txtKey.getText().length() > 0
                                && oldToken.length() != txtKey.getText().length()) {
                            btnSaveToken.setEnabled(true);
                            return;
                        } else if (txtKey.getText().length() > 0
                                && oldToken.length() == txtKey.getText().length()
                                && oldToken.equalsIgnoreCase(txtKey.getText())) {
                            btnSaveToken.setEnabled(false);
                            return;
                        } else if (txtKey.getText().length() > 0
                                && oldToken.length() == txtKey.getText().length()
                                && !oldToken.equalsIgnoreCase(txtKey.getText())) {
                            btnSaveToken.setEnabled(true);
                            return;
                        }
                    }
                    if (txtKey.getText().length() > 0
                            && txtKey.getText().length() != txtKey.getText().length()) {
                        btnSaveToken.setEnabled(true);
                        return;
                    } else if (txtKey.getText().length() > 0
                            && txtKey.getText().length() == txtKey.getText().length()) {
                        btnSaveToken.setEnabled((txtKey.getText().equalsIgnoreCase(oldToken)));
                    }
                }

                btnSaveToken.setEnabled(
                        false);
            }
        });

        JOptionPane.showOptionDialog(
                null, initForm(),
                "Set From Token",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE,
                (logo == null) ? null : new ImageIcon(logo),
                options,
                options[1]);

        return this.value;
    }

    public String getSecurityToken() {
        return txtKey.getText().trim();
    }

    @Override
    public void addEbayAccountListener(EbayAccountListener l) {
        listenerDelegate.addEbayAccountListener(l);
    }

    @Override
    public void removeEbayAccountListener(EbayAccountListener l) {
        listenerDelegate.removeEbayAccountListener(l);
    }

    private JOptionPane getOptionPane(JComponent parent) {
        JOptionPane pane = null;
        if (!(parent instanceof JOptionPane)) {
            pane = getOptionPane((JComponent) parent.getParent());
        } else {
            pane = (JOptionPane) parent;
        }
        return pane;
    }

    private JComponent initForm() {
        JPanel pnl = new JPanel();
        pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));
        Box boxH = Box.createHorizontalBox();
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(new JLabel("Key:"));
        boxH.add(Box.createHorizontalGlue());
        pnl.add(boxH);
        Dimension d = new Dimension(300, 200);
        JScrollPane scrollPane = new JScrollPane(txtKey,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setMinimumSize(d);
        scrollPane.setPreferredSize(d);
        d = new Dimension(d);
        d.height = Integer.MAX_VALUE;
        scrollPane.setMaximumSize(d);
        txtKey.setLineWrap(true);
        txtKey.setText(oldToken);
        DefaultContextMenu contectMenu = new DefaultContextMenu(false);
        contectMenu.add(txtKey);
        pnl.add(scrollPane);
        pnl.add(Box.createVerticalStrut(15));
        btnSaveToken.setEnabled(false);
        return pnl;
    }
}
