/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.gui.forms;

import javax.swing.SwingUtilities;

/**
 *
 * @author David Hamilton
 */
class IndeterminedProgressRunnable implements Runnable {
    
    private final ProgressForm frmProgress;
    private final String messageText;

    /**
     * <p>
     * Constructor for IndeterminedProgressRunnable.
     * </p>
     *
     * @param frm a {@link ProgressForm} object.
     */
    public IndeterminedProgressRunnable(ProgressForm frm) {
        this.frmProgress = frm;
        messageText = "";
    }

    /**
     * <p>
     * Constructor for IndeterminedProgressRunnable.
     * </p>
     *
     * @param frm a {@link ProgressForm} object.
     * @param text a {@link java.lang.String} object.
     */
    public IndeterminedProgressRunnable(ProgressForm frm, String text) {
        this.frmProgress = frm;
        this.messageText = text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                frmProgress.showProgressBar(true);
                if (!messageText.equalsIgnoreCase("")) {
                    // frmProgress1.getLabel().setText(messageText);
                    frmProgress.updateLabel(messageText);
                }
                frmProgress.getProgress().setMaximum(10);
                frmProgress.getProgress().setMinimum(0);
                frmProgress.getProgress().setStringPainted(false);
                frmProgress.getProgress().setIndeterminate(true);
            }
        };
        
        if (!SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }
    }
    
}
