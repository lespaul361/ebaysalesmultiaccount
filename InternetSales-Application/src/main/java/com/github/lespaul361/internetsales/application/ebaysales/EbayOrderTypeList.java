/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.ebaysales;

import com.ebay.soap.eBLBaseComponents.OrderType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.UnaryOperator;

/**
 *
 * @author Charles Hamilton
 */
public class EbayOrderTypeList implements List<OrderType> {

    private final List<OrderType> list = new ArrayList<OrderType>(200);
    private boolean isSorted = false;

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<OrderType> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean add(OrderType e) {
        return list.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return list.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends OrderType> c) {
        return list.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends OrderType> c) {
        return list.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    @Override
    public void replaceAll(UnaryOperator<OrderType> operator) {
        list.replaceAll(operator);
    }

    @Override
    public void sort(Comparator<? super OrderType> c) {
        list.sort(c);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public boolean equals(Object o) {
        return list.equals(o);
    }

    @Override
    public int hashCode() {
        return list.hashCode();
    }

    @Override
    public OrderType get(int index) {
        return list.get(index);
    }

    @Override
    public OrderType set(int index, OrderType element) {
        return list.set(index, element);
    }

    @Override
    public void add(int index, OrderType element) {
        list.add(index, element);
    }

    @Override
    public OrderType remove(int index) {
        return list.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    @Override
    public ListIterator<OrderType> listIterator() {
        return list.listIterator();
    }

    @Override
    public ListIterator<OrderType> listIterator(int index) {
        return list.listIterator(index);
    }

    @Override
    public List<OrderType> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }

    @Override
    public Spliterator<OrderType> spliterator() {
        return list.spliterator();
    }

    /**
     * Gets the {@link OrderType} requested
     *
     * @param order the {@link OrderType} to find
     * @return the {@link OrderType} or null if not found
     */
    public OrderType findEbayOrder(OrderType order) {
        return findEbayOrder(order.getOrderID());
    }

    /**
     * Gets the {@link OrderType} requested
     *
     * @param orderId the {@link OrderType} ID
     * @return the {@link OrderType} or null if not found
     */
    public OrderType findEbayOrder(String orderId) {
        if (!this.isSorted) {
            list.sort(new Comparator<OrderType>() {
                @Override
                public int compare(OrderType o1, OrderType o2) {
                    return o1.getOrderID().compareTo(o2.getOrderID());
                }
            });
            isSorted = true;
        }
        int currentIndex = 0;
        int retCompared = 0;
        int highIndex = list.size();
        int lowIndex = 0;
        String curID = "";

        //check if in range
        retCompared = orderId.compareTo(list.get(0).getOrderID());
        if (retCompared < 0) {
            return null;
        } else if (retCompared == 0) {
            return list.get(0);
        }
        retCompared = orderId.compareTo(list.get(list.size() - 1).getOrderID());
        if (retCompared > 0) {
            return null;
        } else if (retCompared == 0) {
            return list.get(list.size() - 1);
        }

        try {
            while (highIndex - lowIndex > 0) {
                currentIndex = highIndex - ((highIndex - lowIndex) / 2);
                if (currentIndex == list.size()) {
                    return null;
                }
                curID = list.get(currentIndex).getOrderID();
                retCompared = curID.compareTo(orderId);
                if (retCompared > 0) {
                    highIndex = currentIndex;
                } else if (retCompared < 0) {
                    lowIndex = currentIndex;
                } else {
                    return list.get(currentIndex);
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }

        return null;
    }

}
