/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.internetsalesfetch;

import com.github.lespaul361.internetsales.application.Sale;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jdom2.Document;
import org.jdom2.Element;

/**
 *
 * @author David Hamilton
 */
class SalesHelper {

    public static List<Sale> getNewSales(Map<String, Document> documentMap, List<Sale> allSales) {
        List<Element> documentElements = getElementsFromMap(documentMap);
        LocalDate startDate = getLatestDateFromElements(documentElements);

        startDate.minusDays(1);
        allSales = getSalesSince(startDate, allSales);
        List<String> salesOrderIds = getOrderIdsFromSales(allSales);
        List<String> ftpOrderIds = getOrderIdsFromElements(documentElements);
        documentElements = null;
        List<String> newSaleIds = getOrdersNotInList(salesOrderIds, ftpOrderIds);
        ftpOrderIds = null;
        salesOrderIds = null;
        List<Sale> newSales = getSalesFromOrderIds(allSales, newSaleIds);

        return newSales;
    }

    /**
     * Gets a List of {@link Element}s from the supplied map
     *
     * @param documentMap a map containing the {@link Document}s
     * @return List of {@link Element}s
     */
    public static List<Element> getElementsFromMap(Map<String, Document> documentMap) {
        List<Element> elements = new ArrayList<>(300);

        Deque<String> keys = new ArrayDeque<>(documentMap.keySet());
        String currentKey = null;
        Element rootEle = null;

        while ((currentKey = keys.poll()) != null) {
            Document currentDoc = documentMap.get(currentKey);
            rootEle = currentDoc.getRootElement();
            List<Element> children = rootEle.getChildren();
            elements.addAll(children);
        }

        return elements;
    }

    /**
     * Gets the order ID from an {@link Element}
     *
     * @param element a XML {@link Element}
     * @return the order ID as a String
     */
    public static String getOrderIdFromElement(Element element) {
        if (element.getName().equalsIgnoreCase("order")) {
            return element.getAttributeValue("ID");
        }

        return null;
    }

    /**
     * Gets a list of order IDs from a list of {@link Element}s
     *
     * @param elements list of {@link Element}s
     * @return list of order IDs as strings
     */
    public static List<String> getOrderIdsFromElements(List<Element> elements) {
        List<String> orderIds = new ArrayList<>(300);

        Deque<Element> eles = new ArrayDeque<>(elements);
        Element ele = null;
        while ((ele = eles.poll()) != null) {
            orderIds.add(getOrderIdFromElement(ele));
        }

        return orderIds;
    }

    /**
     * Gets a list of order IDs from a list of {@link Sale}s
     *
     * @param sales list of {@link Sale}s
     * @return list of order IDs as strings
     */
    public static List<String> getOrderIdsFromSales(List<Sale> sales) {
        List<String> orderIds = new ArrayList<>(300);

        sales.forEach((sale) -> orderIds.add(sale.getOrderId()));

        return orderIds;
    }

    /**
     * Gets the last {@link LocalDate} of the uploads to the FTP server
     *
     * @param elements list of {@link Element}s
     * @return the {@link LocalDate} of the latest uploads to the FTP server
     */
    public static LocalDate getLatestDateFromElements(List<Element> elements) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate latestDate = null;
        LocalDate currentDate = null;
        String date = "";
        for (Element element : elements) {
            date = element.getAttribute("DateTime").getValue();
            TemporalAccessor ta = formatter.parse(date);
            if (latestDate == null) {
                latestDate = LocalDate.from(ta);
                continue;
            }
            currentDate = LocalDate.from(ta);
            int compare = latestDate.compareTo(currentDate);
            if (compare < 0) {
                latestDate = currentDate;
            }
        }

        return latestDate;
    }

    /**
     * Gets a list of {@link Sale}s starting at the {@link LocalDate} supplied
     *
     * @param date the {@link LocalDate} as the starting point for the
     * {@link Sale}s
     * @param sales list of {@link Sale}s
     * @return list of {@link Sale}s in the date range
     */
    public static List<Sale> getSalesSince(LocalDate date, List<Sale> sales) {
        List<Sale> retSales = sales.stream()
                .filter(sale -> sale.getSaleDate().compareTo(date) >= 0)
                .collect(Collectors.toList());

        return retSales;
    }

    /**
     * Gets a list of Strings of items in list1 not found in list2
     *
     * @param list1 the first list containing items to find
     * @param list2 the second list containing items to search through
     * @return list of Strings of items in list1 not found in list2
     */
    public static List<String> getOrdersNotInList(List<String> list1, List<String> list2) {
        List<String> retSales = list1.stream()
                .filter((saleId) -> {
                    return !list2.contains(saleId);
                })
                .collect(Collectors.toList());

        return retSales;
    }

    /**
     * Gets a list of {@link Sale}s from the supplied list of {@link Sale} using
     * the list of order IDs
     *
     * @param sales list of {@link Sale}s
     * @param orderIds list of order IDs
     * @return list of {@link Sale}s
     */
    public static List<Sale> getSalesFromOrderIds(List<Sale> sales, List<String> orderIds) {
        List<Sale> retSales = sales.stream()
                .filter((sale) -> {
                    return orderIds.contains(sale.getOrderId());
                })
                .collect(Collectors.toList());

        return retSales;
    }

    /**
     * Gets a {@link Document} from the supplied list of {@link Element}s
     *
     * @param elements list of {@link Element}s
     * @return a {@link Document}
     */
    public static Document buildDocument(List<Element> elements) {
        Document document = new Document(new Element("Orders"));
        document.getRootElement().addContent(elements);
        return document;
    }

    /**
     * Gets a list of {@link Element}s from the supplied list of {@link Sale}s
     * @param sales list of {@link Sale}s
     * @return list of {@link Element}s
     */
    public static List<Element> getListOfElementsFromSales(List<Sale> sales) {
        final List<Element> retList = new ArrayList<>(sales.size());

        sales.forEach((sale) -> retList.add(sale.getSaleElement()));

        return retList;
    }
}
