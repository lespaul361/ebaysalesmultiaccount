/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.ebaysales;

import com.ebay.soap.eBLBaseComponents.AddressType;
import com.ebay.soap.eBLBaseComponents.OrderType;
import com.ebay.soap.eBLBaseComponents.TransactionType;
import com.github.lespaul361.internetsales.application.AbstractSale;
import com.github.lespaul361.internetsales.application.States;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 *
 * @author David Hamilton
 */
public class EbaySale extends AbstractSale {

    private final OrderType orderType;

    public EbaySale(OrderType orderType) {
        this.orderType = orderType;
        initSalesInfo();
    }

    @Override
    public int getSaleType() {
        return SALE_TYPE_EBAY;
    }

    private void initSalesInfo() {
        super.setOrderId(this.orderType.getOrderID());
        super.setSaleDate(convertFromEbayTime(this.orderType.getPaidTime()).toLocalDate());
        super.setSellerId(this.orderType.getSellerUserID());
        initBuyerInfo();
        initShippingInfo();
        initTransactions();
    }

    private void initBuyerInfo() {
        try {
            super.setFirstName(getFirstName(this.orderType));
            super.setLastName(getLastName(this.orderType));
        } catch (Exception e) {
        }
    }

    private void initShippingInfo() {
        AddressType address = this.orderType.getShippingAddress();
        super.setAddress1(address.getStreet1());
        super.setAddress2(address.getStreet2() != null ? address.getStreet2() : "");
        super.setCity(address.getCityName());
        super.setCountry(address.getCountryName());
        super.setState(States.State.valueOfAbbreviation(
                address.getStateOrProvince().toUpperCase()));
        super.setZipCode(address.getPostalCode().trim());
    }

    private void initTransactions() {
        List<TransactionType> transactions = new ArrayList<>(
                Arrays.asList(this.orderType.getTransactionArray().getTransaction())
        );

        transactions.forEach((transaction)
                -> {
            EbayTransaction et = new EbayTransaction(transaction);
            getTransactions().add(et);
        });

    }

    private String getFirstName(OrderType order) throws Exception {
        String ret = order.getShippingAddress().getFirstName();
        if (ret != null && !ret.isEmpty()) {
            return ret;
        }

        String name = order.getShippingAddress().getName();
        int pos = name.lastIndexOf(" ");

        return name.substring(0, pos).trim();
    }

    private String getLastName(OrderType order) throws Exception {
        String ret = order.getShippingAddress().getLastName();
        if (ret != null && !ret.isEmpty()) {
            return ret;
        }

        String name = order.getShippingAddress().getName();
        int pos = name.lastIndexOf(" ");

        return name.substring(pos, name.length()).trim();
    }

    private LocalDateTime toLocalDateTime(Calendar calendar) {
        if (calendar == null) {
            return null;
        }
        TimeZone tz = calendar.getTimeZone();
        ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
        return LocalDateTime.ofInstant(calendar.toInstant(), zid);
    }

    private LocalDateTime convertFromEbayTime(Calendar time) {
        return convertFromEbayTime(toLocalDateTime(time));
    }

    private LocalDateTime convertFromEbayTime(LocalDateTime time) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM yyyy hh:mm:s a");
        StringBuilder sb = new StringBuilder(100);
        sb.append(time.format(dtf)).append(" offset: ");
        TimeZone tz = Calendar.getInstance().getTimeZone();
        long offsetMinutes = tz.getOffset(new Date().getTime());
        offsetMinutes = offsetMinutes / 60 / 1_000;
        sb.append(offsetMinutes / 60).append(" ");
        time = time.plusMinutes(offsetMinutes);
        sb.append(time.format(dtf));
        System.out.println(sb);
        return time;
    }

}
