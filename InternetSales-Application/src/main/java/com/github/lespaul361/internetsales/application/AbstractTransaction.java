/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import static com.github.lespaul361.internetsales.application.XMLHelper.getXMLText;
import org.jdom2.Element;

/**
 *
 * @author David Hamilton
 */
public abstract class AbstractTransaction implements Transaction {

    private String itemTitle;
    private int quantity;
    private double itemSalePrice;
    private double itemTax;

    @Override
    public String getItemTitle() {
        return itemTitle;
    }

    @Override
    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public double getItemSalePrice() {
        return itemSalePrice;
    }

    @Override
    public void setItemSalePrice(double itemSalePrice) {
        this.itemSalePrice = itemSalePrice;
    }

    @Override
    public double getItemTax() {
        return itemTax;
    }

    @Override
    public void setItemTax(double itemTax) {
        this.itemTax = itemTax;
    }

    @Override
    public Element getTransactionElement() {
        Element transaction = new Element("Transaction");
        Element subElement = new Element("ItemTitle");
        subElement.setText(getXMLText(getItemTitle()));
        transaction.addContent(subElement);
        subElement = new Element("Qty");
        subElement.setText(String.valueOf(getQuantity()));
        transaction.addContent(subElement);
        subElement = new Element("SellAmount");
        subElement.setText(getItemSalePrice() == 0 ? "0.0" : String.valueOf(getItemSalePrice()));
        transaction.addContent(subElement);
        subElement = new Element("TaxAmount");
        subElement.setText(getItemTax() == 0 ? "0.0" : String.valueOf(getItemTax()));
        transaction.addContent(subElement);

        return transaction;
    }

}
