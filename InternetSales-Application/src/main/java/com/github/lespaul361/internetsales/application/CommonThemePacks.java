/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.internetsales.application;

import com.theme.TextTheme;
import com.theme.ThemePackage;
import com.theme.WindowTheme;
import java.awt.Color;
import java.awt.Font;

/**
 * 
 * @author David Hamilton
 */
public class CommonThemePacks {

    public static ThemePackage getAferHoursTheme() {
	ThemePackage pack = new ThemePackage();

	WindowTheme window = new WindowTheme();
	window.background = new Color(0, 0, 0);
	window.foreground = new Color(16, 124, 162);
	window.opacity = 0.96f;
	window.width = 330;
	window.height = 120;

	TextTheme text = new TextTheme();
	text.title = new Font("Arial", Font.BOLD, 22);
	text.subtitle = new Font("Arial", Font.PLAIN, 14);
	text.titleColor = new Color(200, 200, 200);
	text.subtitleColor = new Color(200, 200, 200);

	pack.setTheme(WindowTheme.class, window);
	pack.setTheme(TextTheme.class, text);

	return pack;
    }

    public static ThemePackage getNewOrdersTheme() {
	ThemePackage pack = new ThemePackage();

	WindowTheme window = new WindowTheme();
	window.background = new Color(0, 0, 0);
	window.foreground = new Color(16, 124, 162);
	window.opacity = 0.8f;
	window.width = 330;
	window.height = 120;

	TextTheme text = new TextTheme();
	text.title = new Font("Arial", Font.BOLD, 22);
	text.subtitle = new Font("Arial", Font.PLAIN, 14);
	text.titleColor = new Color(200, 200, 200);
	text.subtitleColor = new Color(200, 200, 200);

	pack.setTheme(WindowTheme.class, window);
	pack.setTheme(TextTheme.class, text);

	return pack;
    }
}
