/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import java.util.logging.Level;
import java.util.logging.Logger;

class AbstractLogError {

    private final Logger logger;

    public AbstractLogError(Logger logger) {
        this.logger = logger;
    }

    protected void logError(Level level, String msg, Throwable error) {
        logger.log(level, msg, error);
    }

    public Logger getLogger() {
        return this.logger;
    }
}
