/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import com.github.lespaul361.commons.DisplayNotification;
import com.github.lespaul361.commons.version.Version;
import com.github.lespaul361.internetsales.application.internetsalesfetch.InternetSalesFetcher;
import com.github.lespaul361.internetsales.gui.InternetSalesTrayIcon;
import java.awt.Image;
import java.net.URL;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFile;
import com.github.lespaul361.internetsales.configurationfile.version1.ConfigurationFileV1;
import com.theme.ThemePackage;
import com.utils.Time;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import javax.swing.SwingUtilities;

/**
 *
 * @author Charles Hamilton
 */
public class InternetSales {

    private final boolean IS_SANDBOX = false;
    private final boolean isTesting = false;
    private final boolean isShowSettings = false;
    public static final int EXIT_CODE_CONFIG_FILE = 1;
    private FileHandler FILE_HANDLER;
    private final Logger LOGGER = Logger.getLogger("Internet Sales");
    private Image logo = null;
    private Image logoPNG = null;
    static final Version version = new Version(1, 0, 0);
    private InternetSalesTrayIcon trayIcon = null;
    private ConfigurationFile configFile = null;
    private File conFile = new File("D:\\Netbeans InternetSales\\ebaysalesmultiaccount\\InternetSales-ConfigurationFile\\config.dat"); //new File("config.dat");
    private Thread thrFetchSales = null;
    private final ShowNotificationListener showNotificationListener = (ShowNotificationEvent e) -> {
        showNotification(e);
    };

    public static void main(String[] args) {
        new InternetSales(args);
    }

    public InternetSales(String[] args) {
        if (args != null) {
            if (args.length > 0) {
                String path = args[0].trim();
                path = path.replace("\"", "");
                path = path.endsWith("\\") ? path : path + "\\";
                conFile = new File(path + "config.dat");
            }
        }

        this.logo = createImage("/logo.gif", "");
        if (this.logo == null) {
            this.logo = createImage("logo.gif", "");
        }

        this.logoPNG = createImage("/icon.png", "");
        if (this.logoPNG == null) {
            this.logoPNG = createImage("icon.png", "");
        }

        initLAF();
        initLogger();

        if (!checkConfigFile()) {
            showConfigWizzard();
        }

        initTrayIcon();
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
        }

        class SettingsRunnable implements Runnable {

            private int state = 0;

            @Override
            public void run() {
                while (true) {
                    if (state == 0) {
                        if (trayIcon == null) {
                            try {
                                Thread.sleep(100);
                            } catch (Exception e) {
                            }
                            continue;
                        } else {
                            state = 1;
                        }
                    }
                    if (state == 1) {
                        if (trayIcon.getTrayIcon() == null) {
                            try {
                                Thread.sleep(100);
                            } catch (Exception e) {
                            }
                            continue;
                        } else {
                            state = 2;
                        }
                    }
                    if (state == 2) {
                        if (trayIcon.getTrayIcon().getPopupMenu() == null) {
                            try {
                                Thread.sleep(100);
                            } catch (Exception e) {
                            }
                            continue;
                        } else {
                            state = 3;
                        }
                    }
                    if (state == 3) {
                        if (trayIcon.getTrayIcon().getPopupMenu().getItem(0) == null) {
                            try {
                                Thread.sleep(100);
                            } catch (Exception e) {
                            }
                            continue;
                        } else {
                            try {
                                SwingUtilities.invokeLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        trayIcon.getTrayIcon().getPopupMenu().getItem(0).setEnabled(false);
                                        System.out.println("pop menu set enabled false");
                                    }
                                });
                            } catch (Exception e) {
                            } finally {
                                break;
                            }
                        }
                    }
                }
            }

        }
        Thread thr = new Thread(new SettingsRunnable());
        //if not using the settings menu then use a thread to make sure it is 
        //loaded and set the menu enabled to false
        if (!isShowSettings) {
            thr.start();
        }

        startFetchThread();

    }

    private Image createImage(String path, String description) {
        URL imageURL = ClassLoader.getSystemResource(path);
        if (imageURL != null) {
            return new ImageIcon(imageURL, description).getImage();
        }

        return null;
    }

    private void trayIconMenuClicked(ActionEvent e) {
        MenuItem item = (MenuItem) e.getSource();
        if (item.getLabel().equalsIgnoreCase("exit")) {
            System.exit(0);
        } else if (item.getLabel().equalsIgnoreCase("settings...")) {
            showSettingsForm();
        }
    }

    private void showSettingsForm() {
        SettingsFormHandler handler = new SettingsFormHandler(configFile, trayIcon,
                logo, LOGGER);
        handler.showSettingsForm();
    }

    private void initLogger() {
        try {
            FILE_HANDLER = new FileHandler("log-InternetSales.txt", true);
            LOGGER.addHandler(FILE_HANDLER);
        } catch (Exception e) {
        }

        try {
            ConsoleHandler hnd = new ConsoleHandler();
            LOGGER.addHandler(hnd);
            FILE_HANDLER.setFormatter(hnd.getFormatter());
        } catch (Exception e) {
        }

        LOGGER.setLevel(Level.SEVERE);
    }

    private void initLAF() {
        try {
            // Set System L&F
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException ulafe) {
            try {
                // Set cross-platform Java L&F (also called "Metal")
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
                LOGGER.log(Level.SEVERE, ulafe.toString(), ulafe);
            }
        } catch (ClassNotFoundException cnfe) {
            try {
                // Set cross-platform Java L&F (also called "Metal")
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
                LOGGER.log(Level.SEVERE, cnfe.toString(), cnfe);
            }

        } catch (InstantiationException ie) {
            try {
                // Set cross-platform Java L&F (also called "Metal")
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
                LOGGER.log(Level.SEVERE, ie.toString(), ie);
            }
        } catch (IllegalAccessException iae) {
            try {
                // Set cross-platform Java L&F (also called "Metal")
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
                LOGGER.log(Level.SEVERE, iae.toString(), iae);
            }
        }
    }

    private void initTrayIcon() {
        SwingUtilities.invokeLater(() -> {
            this.trayIcon = new InternetSalesTrayIcon(logo, "Internet Sales", LOGGER);
            this.trayIcon.AddActionListener((ActionEvent e) -> {
                trayIconMenuClicked(e);
            });
        });

    }

    private boolean checkConfigFile() {
        configFile = new ConfigurationFileV1(LOGGER);
        if (conFile.exists()) {
            try {
                configFile.readConfigurationFile(conFile);
                return true;
            } catch (Exception e) {
                //TODO: write code
            }
        }
        return false;
    }

    private void showConfigWizzard() {
        SettingsFormHandler handler = new SettingsFormHandler(trayIcon, logo, LOGGER);
        handler.showWizard();
    }

    private void startFetchThread() {
        InternetSalesFetcher salesFetcher = new InternetSalesFetcher(
                configFile.getConfigurationFileInfo(), logo, LOGGER);
        BackGroundFetchRunnable r = new BackGroundFetchRunnable(
                this.configFile.getConfigurationFileInfo().getApplicationRunInfo(),
                salesFetcher,
                LOGGER);
        r.addShowNotificationListener(showNotificationListener);
        this.thrFetchSales = new Thread(r);
        this.thrFetchSales.setDaemon(true);
        this.thrFetchSales.start();
    }

    private void showNotification(ShowNotificationEvent evt) {
        SwingUtilities.invokeLater(() -> {
            showNotification(evt.getTitle(),
                    evt.getMessage(),
                    evt.getIcon(),
                    evt.getIconType(),
                    evt.getThemePackag()
            );
        });
    }

    private void showNotification(String title, String message, ImageIcon icon,
            DisplayNotification.MessageIconType iconType, ThemePackage themePackage) {
        DisplayNotification.NotificationBuilder builder = new DisplayNotification.NotificationBuilder(title, message);
        String path = GetWindowsNotificationSound.getPath();

        if (path != null && !path.isEmpty()) {
            try {
                File audio = new File(path);
                if (audio.exists()) {
                    FileInputStream fis = new FileInputStream(audio);
                    builder.sound(fis);
                }
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }

        }
        if (themePackage != null) {
            builder.theme(themePackage);
        }
        if (icon != null) {
            builder.icon(icon);
        } else if (iconType != null) {
            builder.icon(iconType);
        }
        Time t = Time.seconds(10);
        builder.time(t);

        builder.showNotification();
    }

}
