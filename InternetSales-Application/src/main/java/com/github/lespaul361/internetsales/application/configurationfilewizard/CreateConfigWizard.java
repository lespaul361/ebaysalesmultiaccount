/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.configurationfilewizard;

import com.github.lespaul361.commons.commonroutines.boxbuilding.BoxUtilities;
import com.github.lespaul361.internetsales.configurationfile.AbstractLogError;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountEvent;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountListener;
import com.github.lespaul361.internetsales.gui.ebay.forms.EbaySecurityTokenDialog;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayApplicationComponent;
import com.github.lespaul361.internetsales.gui.ebay.forms.components.EbayEventListenerHelper;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.SecondaryLoop;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

/**
 *
 * @author Charles Hamilton
 */
public class CreateConfigWizard extends AbstractLogError implements EbayEventListenerHelper {

    protected final JFrame frame = new JFrame("Config File Wizard");
    public final static int CANCEL = 0;
    private final static int SAVE = 1;

    private volatile int buttonReturn = -1;

    protected final JPanel pnlButtons = new JPanel();
    protected final JPanel pnlMain = new JPanel();
    protected final JPanel pnlCardContainer = new JPanel();
    protected final JPanel pnlIntro = new JPanel();
    protected final JPanel pnlEbayAppInfo = new JPanel();
    protected final JPanel pnlEbayUserList = new JPanel();
    protected final JPanel pnlFTPInfo = new JPanel();
    protected final JPanel pnlEbayRunInfo = new JPanel();

    protected final JButton btnWizardCancel = new JButton(" Cancel ");
    protected final JButton btnWizardNext = new JButton(" Next ");
    protected final JButton btnWizardPrevious = new JButton(" previous ");
    protected final JButton btnWizardNewEbayUser = new JButton(" New Ebay User ");

    protected EbayApplicationComponent ebayApplicationComponent = null;
    protected EbaySecurityTokenDialog ebaySecurityTokenDialog = null;

    protected final Image logo;
    private transient volatile int curPanel = 1;
    private transient final int panelCount = 5;
    private transient final EventListenerList listenerList = new EventListenerList();

    private class EbayUser {

        public String name;
        public String token;
    }

    private final CardLayout cardLayout = new CardLayout();
    private List<EbayUser> ebayUsers = new ArrayList<>(5);
    private String curPanelNumber = "1";

    public CreateConfigWizard(Image logo, Logger logger) {
        super(logger);
        this.logo = logo;
        init();
    }

    @Override
    public void addEbayAccountListener(EbayAccountListener l) {
        this.listenerList.add(EbayAccountListener.class, l);
    }

    @Override
    public void removeEbayAccountListener(EbayAccountListener l) {
        this.listenerList.remove(EbayAccountListener.class, l);
    }

    public int showDialog() {
        this.frame.setVisible(true);
        return runSecondLoop();
    }

    public String getCertificateID() {
        return this.ebayApplicationComponent.getCertificateID();
    }

    public String getApplicationID() {
        return this.ebayApplicationComponent.getAppID();
    }

    public String getRUName() {
        return this.ebayApplicationComponent.getRUID();
    }

    public String getDeveloperID() {
        return this.ebayApplicationComponent.getDeveloperID();
    }

    public Frame getBaseFrame() {
        return this.frame;
    }

    public int getCurrentTabIndex() {
        return this.curPanel;
    }

    public void setCurrentTabIndex(int index) {
    }

    private void init() {
        initMainPanel();
        initContainer();
        initButtonPanel();
        initNavButtonListeners();
        initIntroPanel();
        initEbayAppPanel();
        initEbayUserList();
        initNewEbayUserListener();

        frame.getContentPane().add(pnlMain);
        frame.setIconImage(logo);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setLocationRelativeTo(null);
    }

    private void initMainPanel() {
        pnlMain.setLayout(new BorderLayout());
        pnlMain.add(pnlCardContainer, BorderLayout.NORTH);
        pnlMain.add(pnlButtons, BorderLayout.SOUTH);
    }

    private void initContainer() {
        pnlCardContainer.setLayout(cardLayout);
        pnlCardContainer.add(pnlIntro, "1");
        pnlCardContainer.add(pnlEbayAppInfo, "2");
        pnlCardContainer.add(pnlEbayUserList, "3");
        pnlCardContainer.add(pnlEbayRunInfo, "4");
        pnlCardContainer.add(pnlFTPInfo, "5");
    }

    private void initIntroPanel() {
        pnlIntro.setLayout(new BoxLayout(pnlIntro, BoxLayout.Y_AXIS));
        pnlIntro.add(Box.createVerticalGlue());
        Box boxH = Box.createHorizontalBox();
        boxH.add(Box.createHorizontalStrut(15));
        boxH.add(Box.createHorizontalGlue());
        JLabel lbl = new JLabel("<html>This wizard with step you through setting up a new "
                + "configuration file<html>");
        boxH.add(lbl);
        boxH.add(Box.createHorizontalStrut(15));
        boxH.add(Box.createHorizontalGlue());
        pnlIntro.add(boxH);
        pnlIntro.add(Box.createVerticalGlue());
    }

    private void initEbayAppPanel() {
        ebayApplicationComponent = new EbayApplicationComponent(null, false,
                false, getLogger());
        pnlEbayAppInfo.setLayout(new BoxLayout(pnlEbayAppInfo, BoxLayout.Y_AXIS));
        pnlEbayAppInfo.add(Box.createVerticalStrut(15));
        pnlEbayAppInfo.add(ebayApplicationComponent);
        pnlEbayAppInfo.add(Box.createVerticalStrut(15));

        ebayApplicationComponent.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equalsIgnoreCase(
                    ebayApplicationComponent.PROP_SHOWBUTTON)) {
                if (curPanel == 2) {
                    btnWizardNext.setEnabled((boolean) evt.getNewValue());
                }
            }
        });
    }

    private void initEbayUserList() {
        pnlEbayUserList.setLayout(new BoxLayout(pnlEbayUserList, BoxLayout.Y_AXIS));
        if (this.ebayUsers.size() == 0) {
            initEbayUserListEmpty();
        } else {
            initEbayUserListNotEmpty();
        }
    }

    private void initEbayUserListEmpty() {
        pnlEbayUserList.setLayout(new BoxLayout(pnlEbayUserList, BoxLayout.Y_AXIS));
        pnlEbayUserList.add(Box.createVerticalStrut(15));
        pnlEbayUserList.add(Box.createVerticalGlue());
        pnlEbayUserList.add(Box.createHorizontalStrut(15));
        pnlEbayUserList.add(Box.createHorizontalGlue());
        pnlEbayUserList.add(btnWizardNewEbayUser);
        pnlEbayUserList.add(Box.createHorizontalStrut(15));
        pnlEbayUserList.add(Box.createHorizontalGlue());
        pnlEbayUserList.add(Box.createVerticalStrut(15));
        pnlEbayUserList.add(Box.createVerticalGlue());
    }

    private void initEbayUserListNotEmpty() {

    }

    private void initButtonPanel() {
        pnlButtons.setLayout(new BoxLayout(pnlButtons, BoxLayout.Y_AXIS));
        pnlButtons.add(Box.createVerticalStrut(10));

        BoxUtilities.setLargestButtonSize(btnWizardPrevious, btnWizardNext, btnWizardCancel);

        Box boxH = Box.createHorizontalBox();
        boxH.add(Box.createHorizontalGlue());
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(btnWizardPrevious);
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(btnWizardCancel);
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(btnWizardNext);
        boxH.add(Box.createHorizontalStrut(10));
        boxH.add(Box.createHorizontalBox());

        Dimension d = boxH.getPreferredSize();
        boxH.setMinimumSize(d);
        d = new Dimension(d);
        d.width = Integer.MAX_VALUE;
        boxH.setMaximumSize(d);

        pnlButtons.add(boxH);
        pnlButtons.add(Box.createVerticalStrut(10));

        btnWizardPrevious.setEnabled(false);

    }

    private void initNavButtonListeners() {
        btnWizardNext.addActionListener((ActionEvent e) -> {
            navButtonNext(e);
        });

        btnWizardPrevious.addActionListener((ActionEvent evt) -> {
            navButtonPrevious(evt);
        });

        btnWizardCancel.addActionListener((ActionEvent evt) -> {
            buttonReturn = CANCEL;
            //frame.dispose();
        });
    }

    private void initNewEbayUserListener() {
        btnWizardNewEbayUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final int BY_KEY = 0;
                final int LOG_IN = 1;
                final int CANCEL = 2;

                int ret = showTypeOfLogInDialog();
                switch (ret) {
                    case BY_KEY:
                        newEbayUserFromKey();
                        break;
                    case LOG_IN:
                        newEbayUserFromLogIn();
                        break;
                    case CANCEL:
                        break;
                }
            }
        });
    }

    private int runSecondLoop() {
        Toolkit kit = Toolkit.getDefaultToolkit();

        // Create secondary loop from awt Toolkit, this is the object  
        // we'll use to block the flow of code without freezing the UI  
        // until another task/thread finishes its work  
        final SecondaryLoop loop
                = kit.getSystemEventQueue().createSecondaryLoop();

        Thread work = new Thread() {

            public void run() {
                while (buttonReturn == -1) {
                }

                if (buttonReturn > -1) {
                    loop.exit();
                }
            }
        };

        // We start the thread to do the real work  
        work.start();

        // Blocks until loop.exit() is called  
        loop.enter();

        return buttonReturn;
    }

    private int showTypeOfLogInDialog() {
        String[] options = new String[]{" By Key ", " By Log In ", " Cancel "};
        int ret = JOptionPane.showOptionDialog(frame,
                "Do You Already Have A Key?",
                "Get Account",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                (logo == null) ? null : new ImageIcon(logo),
                options,
                options[2]);
        return ret;
    }

    private void newEbayUserFromLogIn() {
        EbayAccountEvent evt = new EbayAccountEvent(
                EbayAccountEvent.REQUEST_ADD_ACCOUNT, this);
        EbayAccountListener[] listeners = this.listenerList.getListeners(
                EbayAccountListener.class);
        for (EbayAccountListener l : listeners) {
            l.accountRequest(evt);
        }
    }

    private void newEbayUserFromKey() {
        int ret = -1;
        EbaySecurityTokenDialog dlg = new EbaySecurityTokenDialog(
                getLogger(), logo,
                null);
        ret = dlg.showDialog();
        if (ret == EbaySecurityTokenDialog.SAVE_SELECTED) {
            int i = 0;
        }
    }

    private void navButtonNext(ActionEvent e) {
        if (curPanel < panelCount) {
            curPanel++;
            cardLayout.show(pnlCardContainer, String.valueOf(curPanel));
        }
        switch (curPanel) {
            case 2:
                checkNavButtonsPnl2();
                break;
            case 3:
                checkNavButtonsPnl3();
                break;
        }
    }

    private void navButtonPrevious(ActionEvent e) {
        if (curPanel > 1) {
            curPanel--;
            cardLayout.show(pnlCardContainer, String.valueOf(curPanel));
        }

        btnWizardNext.setEnabled(curPanel != panelCount);
        btnWizardPrevious.setEnabled(curPanel > 1);
    }

    private void checkNavButtonsPnl2() {

        String a, c, d, r = "";
        a = this.ebayApplicationComponent.getAppID().trim();
        c = this.ebayApplicationComponent.getCertificateID().trim();
        d = this.ebayApplicationComponent.getDeveloperID().trim();
        r = this.ebayApplicationComponent.getRUID().trim();

        if (a.equalsIgnoreCase("")
                || c.equalsIgnoreCase("")
                || d.equalsIgnoreCase("")
                || r.equalsIgnoreCase("")) {
            btnWizardNext.setEnabled(false);

        }

        if (curPanel != 2) {
            btnWizardNext.setEnabled(curPanel != panelCount);
        }

        btnWizardPrevious.setEnabled(curPanel > 1);
    }

    private void checkNavButtonsPnl3() {
    }
}
