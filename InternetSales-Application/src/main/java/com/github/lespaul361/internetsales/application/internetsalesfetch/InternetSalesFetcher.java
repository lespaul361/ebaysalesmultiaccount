/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.internetsalesfetch;

import com.github.lespaul361.internetsales.application.ebaysales.EbaySalesFetchRunnable;
import com.github.lespaul361.commons.DisplayNotification;
import com.github.lespaul361.ebaydatalayer.EbayAccountLive;
import com.github.lespaul361.internetsales.application.CommonThemePacks;
import com.github.lespaul361.internetsales.application.Sale;
import com.github.lespaul361.internetsales.application.ShowNotificationEvent;
import com.github.lespaul361.internetsales.application.ShowNotificationListener;
import com.github.lespaul361.internetsales.configurationfile.AbstractLogError;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFileInfo;
import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import com.github.lespaul361.internetsales.configurationfile.FTPInfo;
import com.github.lespaul361.internetsales.ftp.DeleteOldXMLFiles;
import com.github.lespaul361.internetsales.ftp.FTPOrders;
import java.awt.Image;
import java.awt.SecondaryLoop;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.event.EventListenerList;
import org.apache.commons.net.ftp.FTPFile;
import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Charles Hamilton
 */
public class InternetSalesFetcher extends AbstractLogError {

    //protected final FTPMethods ftp;
    protected final ConfigurationFileInfo confingurationInfo;
    protected final Image logo;
    private File documentsFile = null;
    private Map<String, Document> xmlDocumentMap = null;
    private final String TMP_FILE_NAME = "tmpInternateSales.tmp";
    private List<EbayAccountLive> ebayConnections = null;
    private Map<Thread, Runnable> fetchMap = new HashMap<>(10);
    private final String TEMP_DIR_PATH;
    private FTPOrders ftpOrders = null;
    private LocalDate lastRunDate = null;
    private List<FTPFile> ftpFiles = null;
    private int newSalesCount = 0;
    private EventListenerList eventList = new EventListenerList();
    private Image logoPNG = null;

    public InternetSalesFetcher(ConfigurationFileInfo confingurationInfo,
            Image logo, Logger logger) {
        super(logger);
        this.confingurationInfo = confingurationInfo;
        this.logo = logo;
        TEMP_DIR_PATH = getTempFolderLocation();
        this.logoPNG = createImage("/icon.png", "");
        if (this.logoPNG == null) {
            this.logoPNG = createImage("icon.png", "");
        }
    }

    public void runFetch() {
        try {
            this.newSalesCount = 0;
            boolean isSaveXMLData = false;

            readXMLMap();
            if (this.xmlDocumentMap == null) {
                isSaveXMLData = true;
            }
            runThreads();
            Map<String, Document> tmpMap = getFTPRunnable();
            isSaveXMLData = isSaveXMLData
                    ? true
                    : !tmpMap.equals(this.xmlDocumentMap);
            isSaveXMLData = isSaveXMLData
                    ? true
                    : prepareFTPFiles();
            this.xmlDocumentMap = this.xmlDocumentMap == null
                    ? tmpMap
                    : this.xmlDocumentMap;
            //suggest a clean up
            System.gc();
            List<Sale> sales = getAllSales();
            sales = SalesHelper.getNewSales(xmlDocumentMap, sales);
            this.newSalesCount = uploadSalesToFTP(sales);
            System.out.println(this.newSalesCount + " written");

            if (this.newSalesCount > 0) {
                ShowNotificationEvent evt = createNotification();
                fireShowNotification(evt);
            }

            isSaveXMLData = isSaveXMLData
                    ? true
                    : this.newSalesCount > 0;
            if (isSaveXMLData) {
                writeXMLMap();
            }
            //suggest a clean up
            System.gc();
        } catch (Exception e) {
            super.logError(Level.SEVERE, TMP_FILE_NAME, e);
        }
    }

    /**
     * @return the newSalesCount
     */
    public int getNewSalesCount() {
        return newSalesCount;
    }

    /**
     * @return the ebayConnections
     */
    public List<EbayAccountLive> getEbayConnections() {
        return ebayConnections;
    }

    public void addShowNotificationListener(ShowNotificationListener l) {
        eventList.add(ShowNotificationListener.class, l);
    }

    public void removeShowNotificationListener(ShowNotificationListener l) {
        eventList.remove(ShowNotificationListener.class, l);
    }

    protected void fireShowNotification(ShowNotificationEvent evt) {
        List<ShowNotificationListener> listeners = new ArrayList<>(
                Arrays.asList(
                        this.eventList.getListeners(ShowNotificationListener.class)));
        listeners.forEach(listener -> listener.showNotification(evt));
    }

    protected void addEbayFetchThreads(Calendar lastRun) {
        if (this.getEbayConnections() == null) {
            this.ebayConnections = new ArrayList<>(
                    this.confingurationInfo.getEbayAccounts().size());
            for (EbayAccount account : this.confingurationInfo.getEbayAccounts()) {
                EbayAccountLive con = new EbayAccountLive(
                        this.confingurationInfo.getEbayApplicationInfo(),
                        account,
                        getLogger(),
                        this.logo);
                this.getEbayConnections().add(con);
            }
        }

        lastRun.add(Calendar.DATE, -1);

        for (EbayAccountLive con : this.getEbayConnections()) {
            EbaySalesFetchRunnable r = new EbaySalesFetchRunnable(con, lastRun);
            Thread thr = new Thread(r);
            fetchMap.put(thr, r);
        }
    }

    protected void addFTPFetchThread(
            FTPOrders ftpOrders, Map<String, Document> documentMap) {
        FetchFTPSalesRunnable r = new FetchFTPSalesRunnable(ftpOrders, documentMap);
        Thread thr = new Thread(r);
        this.fetchMap.put(thr, r);
    }

    protected void runFetchThreads() {
        final List<Thread> threads = new ArrayList<>(this.fetchMap.keySet());
        Toolkit kit = Toolkit.getDefaultToolkit();
        final SecondaryLoop loop
                = kit.getSystemEventQueue().createSecondaryLoop();

        Thread work = new Thread() {

            @Override
            public void run() {
                for (Thread thr : threads) {
                    thr.start();
                }
                while (true) {
                    try {
                        boolean isDone = true;
                        for (Thread thread : threads) {
                            if (thread.isAlive()) {
                                isDone = false;
                                break;
                            }
                        }
                        if (isDone) {
                            break;
                        }
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                }
                loop.exit();
            }
        };

        // We start the thread to do the real work  
        work.start();

        // Blocks until loop.exit() is called  
        loop.enter();
    }

    private void readXMLMap() {
        File file = new File(TEMP_DIR_PATH + TMP_FILE_NAME);

        if (!file.exists()) {
            return;
        }

        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int bufferSize = 4096;
            byte[] buffer = new byte[bufferSize];
            int read = 0;
            while ((read = fis.read(buffer)) > -1) {
                baos.write(buffer, 0, read);
            }
            buffer = baos.toByteArray();
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            ObjectInputStream ois = new ObjectInputStream(bais);
            this.xmlDocumentMap = (Map<String, Document>) ois.readObject();

            System.gc();
        } catch (Exception e) {
            logError(Level.WARNING, e.getMessage(), e);
            this.xmlDocumentMap = null;
        }

    }

    private void writeXMLMap() {
        File file = new File(TEMP_DIR_PATH + TMP_FILE_NAME);
        try {
            if (!file.exists()) {
                String path = file.getAbsolutePath();
                createFilePath(path);
            }
            Map<String, Document> tmpMap = new XMLMap(xmlDocumentMap);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(tmpMap);
            oos.flush();
            oos.close();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baos.toByteArray());
            baos.flush();
            baos.close();
            fos.flush();
            fos.close();

        } catch (Exception e) {
            logError(Level.WARNING, e.getMessage(), e);
        }

    }

    private void createFilePath(String path) throws IOException {
        String[] pathParts = path.split("\\\\");
        File file = new File(path);
        StringBuilder tmpPath = new StringBuilder(200);
        File fleTmp = new File(tmpPath.toString());
        int i = -1;

        do {
            tmpPath.append(pathParts[++i] + "\\");
            fleTmp = new File(tmpPath.toString());
            if (!fleTmp.exists()) {
                if (!fleTmp.getAbsolutePath().toLowerCase().contains(".tmp")) {
                    fleTmp.mkdir();
                } else {
                    fleTmp.createNewFile();
                }
            }
        } while (!file.exists());
    }

    private Map<String, Document> getFTPRunnable() throws Exception {
        Collection<Runnable> runnables = this.fetchMap.values();
        for (Runnable r : runnables) {
            if (r instanceof FetchFTPSalesRunnable) {
                FetchFTPSalesRunnable fetchRunnable = (FetchFTPSalesRunnable) r;
                if (fetchRunnable.isException()) {
                    throw fetchRunnable.getException();
                }
                return fetchRunnable.documentMap;
            }
        }
        return null;
    }

    private boolean deleteOldFTPFiles() {
        DeleteOldXMLFiles delOld = new DeleteOldXMLFiles(
                this.confingurationInfo.getFTPInfo().getFTPIP(),
                this.confingurationInfo.getFTPInfo().getFTPUserName(),
                this.confingurationInfo.getFTPInfo().getFTPPassword());
        try {
            return delOld.delete((byte) 2);
        } catch (Exception e) {
        }
        return false;
    }

    private String getTempFolderLocation() {
        File tmpFile = new File(System.getenv("APPDATA"));
        if (tmpFile.exists()) {
            if (tmpFile.getAbsolutePath().toLowerCase().endsWith("roaming")) {
                String tmp = tmpFile.getAbsolutePath().substring(
                        0,
                        tmpFile.getAbsolutePath().lastIndexOf("\\"));
                tmpFile = new File(tmp);
            }
            tmpFile = new File(tmpFile.getAbsolutePath() + "\\Local\\InternetSales");

        } else {
            tmpFile = new File(System.getProperty("java.io.tmpdir"));
        }

        String ret = tmpFile.getAbsolutePath();
        if (!ret.endsWith("\\")) {
            ret = ret + "\\";
        }
        return ret;
    }

    private boolean prepareFTPFiles() throws Exception {

        boolean isDeleteRan = false;
        if ((this.lastRunDate != null
                && lastRunDate.compareTo(LocalDate.now()) < 0)
                || this.lastRunDate == null) {
            isDeleteRan = deleteOldFTPFiles();
            this.lastRunDate = LocalDate.now();
        }
        return isDeleteRan;
    }

    private FTPOrders getFTPOrders() throws Exception {
        this.ftpOrders = new FTPOrders(
                this.confingurationInfo.getFTPInfo().getFTPUserName(),
                this.confingurationInfo.getFTPInfo().getFTPPassword(),
                this.confingurationInfo.getFTPInfo().getFTPIP());
        this.ftpOrders.refreshFTPFiles();
        return this.ftpOrders;
    }

    private void attachFetchThreads() throws Exception {
        this.fetchMap.clear();
        getFTPOrders();
        long lastFileOnFTP = ftpOrders.getLastFTPUploadInMili();
        Calendar lastFTPCal = Calendar.getInstance();
        lastFTPCal.setTimeInMillis(lastFileOnFTP);
        addEbayFetchThreads(lastFTPCal);
        addFTPFetchThread(ftpOrders, xmlDocumentMap);
    }

    private String getNewFileName() {
        String fileName = String.valueOf(Calendar.getInstance().getTimeInMillis());
        fileName = fileName.concat(".xml");
        return fileName;
    }

    private List<Sale> getAllSales() throws Exception {
        Collection<Runnable> runnables = this.fetchMap.values();
        List<Sale> sales = new ArrayList<>();

        runnables.forEach((runnable) -> {
            if (runnable instanceof SalesFetchRunnable) {
                sales.addAll(((SalesFetchRunnable) runnable).getSales());
            }
        });

        return sales;
    }

    private void runThreads() throws Exception {
        attachFetchThreads();
        runFetchThreads();
    }

    private ShowNotificationEvent createNotification() {
        StringBuilder sb = new StringBuilder(300);
        if (this.newSalesCount == 1) {
            sb.append("A new order has ");
        } else if (this.newSalesCount > 1) {
            sb.append(this.newSalesCount);
            sb.append(" new orders have ");
        }
        sb.append("\n been added to the que");
        ShowNotificationEvent evt = new ShowNotificationEvent("Orders Added",
                sb.toString(),
                new ImageIcon(this.logoPNG),
                DisplayNotification.MessageIconType.None,
                CommonThemePacks.getNewOrdersTheme(),
                logo);

        return evt;
    }

    private Image createImage(String path, String description) {
        URL imageURL = ClassLoader.getSystemResource(path);
        if (imageURL != null) {
            return new ImageIcon(imageURL, description).getImage();
        }

        return null;
    }

    private int uploadSalesToFTP(List<Sale> sales)
            throws SocketException, IOException {
        if (sales.isEmpty()) {
            return 0;
        }
        String xmlName = getNewFileName();
        Document doc = SalesHelper.buildDocument(
                SalesHelper.getListOfElementsFromSales(sales));
        xmlDocumentMap.put(xmlName, doc);
        XMLOutputter xmlOutput = new XMLOutputter();
        Format f = Format.getPrettyFormat();
        f.setEncoding("UTF-16");
        xmlOutput.setFormat(f);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        xmlOutput.output(doc, out);
        if (ftpOrders.getFtp().getclient() == null) {
            FTPInfo ftpInfo = this.confingurationInfo.getFTPInfo();
            ftpOrders = new FTPOrders(ftpInfo.getFTPUserName(),
                    ftpInfo.getFTPPassword(),
                    ftpInfo.getFTPIP());
        }
        ftpOrders.uploadOrders(xmlName, out.toByteArray());
        ftpOrders.getFtp().CloseConnection();

        ftpOrders = null;
        xmlOutput = null;
        out = null;
        f = null;
        doc = null;
        System.gc();
        return sales.size();
    }
}
