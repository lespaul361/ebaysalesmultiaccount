/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import org.jdom2.Element;

/**
 *
 * @author David Hamilton
 */
public interface Transaction {

    /**
     * Gets the title of the item
     *
     * @return title of the item
     */
    public String getItemTitle();

    /**
     * Sets the item title
     *
     * @param itemTitle item title
     */
    public void setItemTitle(String itemTitle);

    /**
     * Gets the number of this items sold
     *
     * @return number of this items sold
     */
    public int getQuantity();

    /**
     * Sets the number of this items sold
     * @param quantity number of this items sold
     */
    public void setQuantity(int quantity);

    /**
     * Gets the price this item was sold at
     *
     * @return price this item was sold at
     */
    public double getItemSalePrice();

    /**
     * Sets the price this item was sold at
     *
     * @param price price this item was sold at
     */
    public void setItemSalePrice(double price);

    /**
     * Gets the tax charged for this item
     *
     * @return tax charged for this item
     */
    public double getItemTax();

    /**
     * Sets the tax charged for this item
     *
     * @param tax tax charged for this item
     */
    public void setItemTax(double tax);

    /**
     * Gets the XML element for this transaction
     *
     * @return XML element for this transaction
     */
    public Element getTransactionElement();
}
