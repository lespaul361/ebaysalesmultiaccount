/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.lespaul361.internetsales.application;

import com.github.lespaul361.commons.commonroutines.utilities.DesktopApi;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

/**
 * 
 * @author David Hamilton
 */
class GetWindowsNotificationSound {
    /**
     * <p>
     * getPath.
     * </p>
     * 
     * @return a {@link java.lang.String} object.
     */
    public static String getPath() {
	DesktopApi.EnumOS os = DesktopApi.getOs();

	if (os == DesktopApi.EnumOS.windows) {
	    String regPath = "";
	    String root = System.getenv("SystemRoot");
	    double v = getOSVersion();
	    if ((v == 10.0) || (v == 8.1) || ((v == 8.1))) {
		regPath = "reg query " + '"' + "HKCU\\AppEvents\\Schemes\\Apps\\"
			+ ".Default\\Notification.IM\\.Current\" /ve";
	    } else if (v == 7.0) {
		regPath = "reg query " + '"' + "HKCU\\AppEvents\\Schemes\\Apps\\"
			+ ".Default\\MailBeep\\.Current\" /ve";
	    } else {
		return null;
	    }
	    try {
		Process process = Runtime.getRuntime().exec(regPath);
		StreamReader reader = new StreamReader(process.getInputStream());
		reader.start();
		process.waitFor();
		reader.join();
		String output = reader.getResult();
		String[] tmp = output.split("\\s+");
		boolean write = false;
		output = "";
		for (int counter = 0; counter < tmp.length; counter++) {
		    if (!write) {
			if (tmp[counter].toLowerCase().contains("system")
				|| tmp[counter].toLowerCase().contains("windows")) {
			    write = true;
			    output = tmp[counter];
			}
		    } else {
			output = output + " " + tmp[counter];
		    }
		}
		output = output.toLowerCase();
		output = output.replace("%systemroot%", root);
		return output;
	    } catch (Exception e) {
	    }

	}
	return null;
    }

    private static double getOSVersion() {
	Runtime rt;
	Process pr;
	BufferedReader in;
	String line;
	final String SEARCH_TERM = "OS Version:";
	try {
	    rt = Runtime.getRuntime();
	    pr = rt.exec("SYSTEMINFO");
	    in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
	    while ((line = in.readLine()) != null) {
		if (line.toLowerCase().contains(SEARCH_TERM.toLowerCase())) // found
									    // the
									    // OS
									    // you
									    // are
									    // using
		{
		    // extract the full os name
		    String version = line.substring(line.lastIndexOf(SEARCH_TERM) + SEARCH_TERM.length(),
			    line.length() - 1);
		    version = version.trim();
		    String[] tmp = version.split("\\s+");
		    version = tmp[0].trim();
		    tmp = version.split("[.]");
		    if (tmp.length > 1) {
			version = tmp[0] + "." + tmp[1];
		    }
		    double v = Double.parseDouble(version);
		    if (v >= 10.0) {
			return 10;
		    }
		    if (v >= 6.3) {
			return 8.1;
		    }
		    if (v == 6.2) {
			return 8.0;
		    }
		    if (v == 6.1) {
			return 7.0;
		    }
		}
	    }
	} catch (Exception e) {
	}
	return 0;
    }

}

/**
 * <p>
 * Constructor for StreamReader.
 * </p>
 * 
 * @param is
 *            a {@link java.io.InputStream} object.
 */
class StreamReader extends Thread {

    private InputStream is;
    private StringWriter sw = new StringWriter();

    /**
     * <p>
     * run.
     * </p>
     * 
     * @param is
     *            a {@link java.io.InputStream} object.
     */
    public StreamReader(InputStream is) {
	this.is = is;
    }

    /**
     * <p>
     * run.
     * </p>
     */
    public void run() {
	try {
	    int c;
	    while ((c = is.read()) != -1) {
		sw.write(c);
		/**
		 * <p>
		 * getResult.
		 * </p>
		 * 
		 * @return a {@link java.lang.String} object.
		 */
	    }
	} catch (IOException e) {
	}
    }

    /**
     * <p>
     * getResult.
     * </p>
     * 
     * @return a {@link java.lang.String} object.
     */
    public String getResult() {
	return sw.toString();
    }
}
