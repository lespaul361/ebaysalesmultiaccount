/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.internetsalesfetch;

import com.github.lespaul361.internetsales.application.ebaysales.EbaySalesFetchRunnable;
import com.ebay.soap.eBLBaseComponents.OrderType;
import com.github.lespaul361.internetsales.application.ebaysales.EbaySale;
import com.github.lespaul361.internetsales.configurationfile.AbstractLogError;
import com.github.lespaul361.internetsales.configurationfile.ApplicationRunInfo;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFileInfo;
import java.awt.Image;
import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.jdom2.Document;
import org.jdom2.Element;

/**
 *
 * @author Charles Hamilton
 */
class EbayHelper extends AbstractLogError {

    protected final ConfigurationFileInfo confingurationInfo;
    protected final Image logo;
    private final String TEMP_DIR_PATH;
    private final List<EbaySalesFetchRunnable> ebayRunnables;
    private Map<String, Document> documentMap;

    public EbayHelper(ConfigurationFileInfo confingurationInfo, Image logo,
            String TEMP_DIR_PATH, List<EbaySalesFetchRunnable> ebayRunnables,
            Map<String, Document> documentMap, Logger logger) {
        super(logger);
        this.confingurationInfo = confingurationInfo;
        this.logo = logo;
        this.TEMP_DIR_PATH = TEMP_DIR_PATH;
        this.ebayRunnables = ebayRunnables;
        TEMP_DIR_PATH = getTempFolderLocation();
        this.documentMap = documentMap;
    }

    public List<Element> getElements() {
        List<String> ftpOrderIds = getOrderIds(documentMap);
        List<String> ebayOrderIds = getOrderIds(ebayRunnables);
        List<String> newOrderIds = getUnsavedOrderIds(ebayOrderIds, ftpOrderIds);
        ftpOrderIds = null;
        ebayOrderIds = null;
        List<OrderType> newOrderTypes = getNewOrderTypes(ebayRunnables, newOrderIds);
        newOrderIds = null;
        //suggest clean up
        System.gc();
        List<OrderType> orderTypesToUpload = new ArrayList<>(newOrderTypes.size());
        for (OrderType order : newOrderTypes) {
            if (order.getPaidTime() != null) {
                if (this.confingurationInfo.getApplicationRunInfo().getSalesFetchType()
                        == ApplicationRunInfo.COMPLETED_TYPE_PAID) {
                    orderTypesToUpload.add(order);
                    continue;
                } else if (this.confingurationInfo.getApplicationRunInfo().getSalesFetchType()
                        == ApplicationRunInfo.COMPLETED_TYPE_SHIPPED) {
                    orderTypesToUpload.add(order);
                    continue;
                }
            }
        }
        List<Element> elements = new ArrayList<>(orderTypesToUpload.size());

        for (OrderType order : orderTypesToUpload) {
            EbaySale sale = new EbaySale(order);
            elements.add(sale.getSaleElement());
        }

        return elements;
    }

    public static List<String> getOrderIds(Map<String, Document> map) {
        List<String> retOrderIds = new ArrayList<>(500);
        Deque<String> keys = new ArrayDeque<>(map.keySet());
        String currentKey = null;
        Element rootEle = null;
        List<Element> elements = null;

        while ((currentKey = keys.poll()) != null) {
            Document currentDoc = map.get(currentKey);
            rootEle = currentDoc.getRootElement();
            elements = rootEle.getChildren();
            for (Element childEle : elements) {
                if (childEle.getName().equalsIgnoreCase("order")) {
                    retOrderIds.add(childEle.getAttributeValue("ID"));
                }
            }
        }

        return retOrderIds;
    }

    public static List<String> getOrderIds(List<EbaySalesFetchRunnable> runnables) {
        List<String> retOrderIds = new ArrayList<>(500);
        Deque<OrderType> orders = null;
        OrderType currentOrderType = null;
        for (EbaySalesFetchRunnable r : runnables) {
            orders = new ArrayDeque<>(r.getEbayOrderTypes());
            while ((currentOrderType = orders.poll()) != null) {
                retOrderIds.add(currentOrderType.getOrderID());
            }
        }

        return retOrderIds;
    }

    public static List<String> getUnsavedOrderIds(
            List<String> ebayIds, List<String> savedIds) {
        List<String> ret = ebayIds.stream()
                .filter((orderId) -> {
                    return !savedIds.contains(orderId);
                })
                .collect(Collectors.toList());
        return ret;
    }

    public static OrderType findEbayOrder(String orderId, List<OrderType> orders) {
        orders.sort(new Comparator<OrderType>() {
            @Override
            public int compare(OrderType o1, OrderType o2) {
                return o1.getOrderID().compareTo(o2.getOrderID());
            }

        });

        int currentIndex = 0;
        int retCompared = 0;
        int highIndex = orders.size();
        int lowIndex = 0;
        String curID = "";

        //check if in range
        retCompared = orderId.compareTo(orders.get(0).getOrderID());
        if (retCompared < 0) {
            return null;
        } else if (retCompared == 0) {
            return orders.get(0);
        }
        retCompared = orderId.compareTo(orders.get(orders.size() - 1).getOrderID());
        if (retCompared > 0) {
            return null;
        } else if (retCompared == 0) {
            return orders.get(orders.size() - 1);
        }

        try {
            while (highIndex - lowIndex > 0) {
                currentIndex = highIndex - ((highIndex - lowIndex) / 2);
                if (currentIndex == orders.size()) {
                    return null;
                }
                curID = orders.get(currentIndex).getOrderID();
                retCompared = curID.compareTo(orderId);
                if (retCompared > 0) {
                    highIndex = currentIndex;
                } else if (retCompared < 0) {
                    lowIndex = currentIndex;
                } else {
                    return orders.get(currentIndex);
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }

        return null;
    }

    public static List<OrderType> getNewOrderTypes(
            List<EbaySalesFetchRunnable> runnables, List<String> newOrderIds) {
        List<OrderType> retNewOrderTypes = new ArrayList<>(50);
        OrderType currentOrder = null;
        for (String orderId : newOrderIds) {
            currentOrder = null;
            for (EbaySalesFetchRunnable r : runnables) {
                currentOrder = findEbayOrder(orderId, r.getEbayOrderTypes());
                if (currentOrder != null) {
                    retNewOrderTypes.add(currentOrder);
                    break;
                }
            }
        }

        return retNewOrderTypes;
    }

    private String getTempFolderLocation() {
        File tmpFile = new File(System.getenv("APPDATA"));
        if (tmpFile.exists()) {
            if (tmpFile.getAbsolutePath().toLowerCase().endsWith("roaming")) {
                String tmp = tmpFile.getAbsolutePath().substring(
                        0,
                        tmpFile.getAbsolutePath().lastIndexOf("\\"));
                tmpFile = new File(tmp);
            }
            tmpFile = new File(tmpFile.getAbsolutePath() + "\\Local\\InternetSales");

        } else {
            tmpFile = new File(System.getProperty("java.io.tmpdir"));
        }

        String ret = tmpFile.getAbsolutePath();
        if (!ret.endsWith("\\")) {
            ret = ret + "\\";
        }
        return ret;
    }
}
