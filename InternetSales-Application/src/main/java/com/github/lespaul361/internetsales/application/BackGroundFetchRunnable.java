/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import com.github.lespaul361.commons.DisplayNotification;
import com.github.lespaul361.ebaydatalayer.EbayAccountLive;
import com.github.lespaul361.internetsales.application.internetsalesfetch.InternetSalesFetcher;
import com.github.lespaul361.internetsales.configurationfile.AbstractLogError;
import com.github.lespaul361.internetsales.configurationfile.ApplicationRunInfo;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.EventListenerList;

/**
 *
 * @author Charles Hamilton
 */
public class BackGroundFetchRunnable extends AbstractLogError implements Runnable {

    private final boolean isIgnoreTimeRange = false;
    private final ApplicationRunInfo appInfo;
    private long lastRunTimeMil = 0;
    private List<EbayAccountLive> ebayConnections = null;
    private final InternetSalesFetcher fetcher;
    private boolean isCleanUp = false;
    private boolean isFirstRun = true;
    private EventListenerList eventList = new EventListenerList();
    private final ShowNotificationListener showNotificationListener = (ShowNotificationEvent e) -> {
        fireShowNotification(e);
    };

    public BackGroundFetchRunnable(ApplicationRunInfo appInfo, 
            InternetSalesFetcher fetcher, Logger logger) {
        super(logger);
        this.appInfo = appInfo;
        this.fetcher = fetcher;
        fetcher.addShowNotificationListener(showNotificationListener);
    }

    public void addShowNotificationListener(ShowNotificationListener l) {
        eventList.add(ShowNotificationListener.class, l);
    }

    public void removeShowNotificationListener(ShowNotificationListener l) {
        eventList.remove(ShowNotificationListener.class, l);
    }

    @Override
    public void run() {
        int counter = 0;
        int countToClean = 10;

        while (true) {
            if (isFirstRun) {
                if (!isInRunTimeRange()) {
                    StringBuilder sb = new StringBuilder(200);
                    sb.append("This application runs from: \n");
                    sb.append(appInfo.getStartTime().format(
                            DateTimeFormatter.ofPattern("hh:mm a")));
                    sb.append(" to ");
                    sb.append(appInfo.getTimeEnd().format(
                            DateTimeFormatter.ofPattern("hh:mm a")));
                    sb.append("\n");
                    sb.append("The application will run in the \n background");
                    ShowNotificationEvent evt = new ShowNotificationEvent(
                            "After Hours",
                            sb.toString(),
                            null,
                            DisplayNotification.MessageIconType.Warning,
                            CommonThemePacks.getAferHoursTheme(),
                            this);
                    isFirstRun = false;
                    fireShowNotification(evt);
                } else {
                    isFirstRun = false;
                }
            }
            if (isRunFetch() || isIgnoreTimeRange) {
                try {
                    this.lastRunTimeMil = System.currentTimeMillis();
                    fetcher.runFetch();
                    this.isCleanUp = true;
                    System.gc();
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                    super.logError(Level.SEVERE, e.getMessage(), e);
                    System.exit(1);
                }
            } else {
                if (this.isCleanUp) {
                    System.gc();
                    counter++;
                    if (counter == countToClean) {
                        isCleanUp = false;
                        counter = 0;
                    }
                    try {
                        Thread.sleep(1000 * 10);//10 sec
                    } catch (Exception e) {
                    }
                } else {
                    try {
                        Thread.sleep(1000 * 30);//30 seconds
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    protected void fireShowNotification(ShowNotificationEvent evt) {
        List<ShowNotificationListener> listeners = new ArrayList<>(
                Arrays.asList(
                        this.eventList.getListeners(ShowNotificationListener.class)));
        listeners.forEach(listener -> listener.showNotification(evt));
    }

    private boolean isInRunTimeRange() {
        if (isIgnoreTimeRange) {
            return true;
        }
        
        LocalTime now = LocalTime.now();
        return (appInfo.getStartTime().compareTo(now) <= 0) && (appInfo.getTimeEnd().compareTo(now) >= 0);
    }

    private boolean isRunFetch() {
        if (!isInRunTimeRange()) {
            return false;
        }

        if (lastRunTimeMil == 0) {
            return true;
        }

        int runInterval = appInfo.getIntervalTime();
        long nextRunTimeMil = lastRunTimeMil + (1000 * 60 * runInterval);
        long curTimeMili = System.currentTimeMillis();
        return nextRunTimeMil <= curTimeMili;

    }
}
