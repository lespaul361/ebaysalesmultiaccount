/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.internetsalesfetch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Charles Hamilton
 */
public class XMLMap implements Map<String, Document>, Serializable {

    private static final long serialVersionUID = 4L;
    private Map<String, Document> xmlData = new HashMap<>(250);

    public XMLMap(Map<String, Document> map) {
        Iterator<String> iterator = map.keySet().iterator();
        String fileName = null;
        Document doc = null;
        while (iterator.hasNext()) {
            fileName = iterator.next();
            doc = map.get(fileName);
            this.xmlData.put(fileName, doc);
        }
    }

    @Override
    public int size() {
        return xmlData.size();
    }

    @Override
    public boolean isEmpty() {
        return xmlData.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return xmlData.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return xmlData.containsValue(value);
    }

    @Override
    public Document get(Object key) {
        return xmlData.get(key);
    }

    @Override
    public Document put(String key, Document value) {
        return xmlData.put(key, value);
    }

    @Override
    public Document remove(Object key) {
        return xmlData.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Document> m) {
        xmlData.putAll(m);
    }

    @Override
    public void clear() {
        xmlData.clear();
    }

    @Override
    public Set<String> keySet() {
        return xmlData.keySet();
    }

    @Override
    public Collection<Document> values() {
        return xmlData.values();
    }

    @Override
    public Set<Map.Entry<String, Document>> entrySet() {
        return xmlData.entrySet();
    }

    @Override
    public boolean equals(Object o) {
        return xmlData.equals(o);
    }

    @Override
    public int hashCode() {
        return xmlData.hashCode();
    }

    @Override
    public Document getOrDefault(Object key, Document defaultValue) {
        return xmlData.getOrDefault(key, defaultValue);
    }

    @Override
    public void forEach(BiConsumer<? super String, ? super Document> action) {
        xmlData.forEach(action);
    }

    @Override
    public void replaceAll(BiFunction<? super String, ? super Document, ? extends Document> function) {
        xmlData.replaceAll(function);
    }

    @Override
    public Document putIfAbsent(String key, Document value) {
        return xmlData.putIfAbsent(key, value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return xmlData.remove(key, value);
    }

    @Override
    public boolean replace(String key, Document oldValue, Document newValue) {
        return xmlData.replace(key, oldValue, newValue);
    }

    @Override
    public Document replace(String key, Document value) {
        return xmlData.replace(key, value);
    }

    @Override
    public Document computeIfAbsent(String key, Function<? super String, ? extends Document> mappingFunction) {
        return xmlData.computeIfAbsent(key, mappingFunction);
    }

    @Override
    public Document computeIfPresent(String key, BiFunction<? super String, ? super Document, ? extends Document> remappingFunction) {
        return xmlData.computeIfPresent(key, remappingFunction);
    }

    @Override
    public Document compute(String key, BiFunction<? super String, ? super Document, ? extends Document> remappingFunction) {
        return xmlData.compute(key, remappingFunction);
    }

    @Override
    public Document merge(String key, Document value, BiFunction<? super Document, ? super Document, ? extends Document> remappingFunction) {
        return xmlData.merge(key, value, remappingFunction);
    }

    @Override
    public String toString() {
        return this.xmlData.toString();
    }

    private void writeObject(ObjectOutputStream oos) throws Exception {
        oos.writeInt(xmlData.size());
        Iterator<String> iterator = xmlData.keySet().iterator();
        byte[] buffer = null;
        int counter = 0;
        while (iterator.hasNext()) {
            String fileName = iterator.next();
            oos.writeUTF(fileName);
            Document doc = this.xmlData.get(fileName);
            XMLOutputter xmlOutput = new XMLOutputter();
            Format f = Format.getPrettyFormat();
            f.setEncoding("UTF-16");
            xmlOutput.setFormat(f);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            xmlOutput.output(doc, baos);
            buffer = baos.toByteArray();
            String s = new String(buffer, "UTF-16");
            char[] c = s.toCharArray();
            oos.writeInt(buffer.length);
            oos.writeObject(buffer);
        }
    }

    private void readObject(ObjectInputStream ois) throws Exception {
        xmlData = this.xmlData == null ? new HashMap<>() : this.xmlData;
        int len = ois.readInt();
        String fileName = null;
        byte[] buffer = null;
        int size = 0;
        int counter = 0;
        for (int i = 0; i < len; i++) {
            fileName = ois.readUTF();
            size = ois.readInt();
            buffer = (byte[]) ois.readObject();
            String s = new String(buffer, "UTF-16");
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            SAXBuilder builder = new SAXBuilder();
            Document document = null;
            try {
                StringReader reader = new StringReader(s.trim());
                document = builder.build(reader);
            } catch (Exception e) {
                e.printStackTrace(System.err);
            }

            this.xmlData.put(fileName, document);
        }
    }
}
