/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.ebaysales;

import com.ebay.soap.eBLBaseComponents.TransactionType;
import com.github.lespaul361.internetsales.application.AbstractTransaction;

/**
 *
 * @author David Hamilton
 */
public class EbayTransaction extends AbstractTransaction {
    
    private final TransactionType transactionType;
    
    public EbayTransaction(TransactionType transactionType) {
        this.transactionType = transactionType;
        initInfo();
    }
    
    private void initInfo() {
        super.setItemSalePrice(
                this.transactionType.getTransactionPrice() == null
                ? 0.0
                : this.transactionType.getTransactionPrice().getValue());
        
        super.setItemTax(
                this.transactionType.getTaxes().getTotalTaxAmount() == null
                ? 0.0
                : this.transactionType.getTaxes().getTotalTaxAmount().getValue());
        
        super.setItemTitle(this.transactionType.getItem().getTitle());
        super.setQuantity(this.transactionType.getQuantityPurchased());
    }
    
}
