/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import java.time.LocalDate;
import java.util.List;
import org.jdom2.Element;

public interface Sale {

    public static final int SALE_TYPE_EBAY = 0;
    public static final int SALE_TYPE_AMAZON = 1;

    /**
     * Gets the seller ID
     *
     * @return seller ID
     */
    public String getSellerId();

    /**
     * Sets the seller ID
     *
     * @param sellerId seller ID
     */
    public void setSellerId(String sellerId);

    /**
     * Gets the buyers first name
     *
     * @return buyers first name
     */
    public String getFirstName();

    /**
     * Sets the buyers first name
     *
     * @param firstName buyers first name
     */
    public void setFirstName(String firstName);

    /**
     * Gets the buyers last name
     *
     * @return buyers last name
     */
    public String getLastName();

    /**
     * Sets the buyers last name
     *
     * @param lastName buyers last name
     */
    public void setLastName(String lastName);

    /**
     * Gets the date of the sale
     *
     * @return date of the sale
     */
    public LocalDate getSaleDate();

    /**
     * Sets the date of the sale
     *
     * @param saleDate date of the sale
     */
    public void setSaleDate(LocalDate saleDate);

    /**
     * Gets the order ID
     *
     * @return order ID
     */
    public String getOrderId();

    /**
     * Sets the order ID
     *
     * @param orderId order ID
     */
    public void setOrderId(String orderId);

    /**
     * Gets address1
     *
     * @return address1
     */
    public String getAddress1();

    /**
     * Sets address1
     *
     * @param address1 address1
     */
    public void setAddress1(String address1);

    /**
     * Gets address2
     *
     * @return address2
     */
    public String getAddress2();

    /**
     * Sets address2
     *
     * @param address2 address2
     */
    public void setAddress2(String address2);

    /**
     * Gets the state
     *
     * @return state
     */
    public States.State getState();

    /**
     * Sets the state
     *
     * @param state the state
     */
    public void setState(States.State state);

    /**
     * Gets the country
     *
     * @return country
     */
    public String getCountry();

    /**
     * Sets the country
     *
     * @param country country
     */
    public void setCountry(String country);

    /**
     * Gets the zip code
     *
     * @return zip code
     */
    public String getZipCode();

    /**
     * Sets the zip code
     *
     * @param zipCode zip code
     */
    public void setZipCode(String zipCode);

    /**
     * Gets the city
     *
     * @return city
     */
    public String getCity();

    /**
     * Sets the city
     *
     * @param city city
     */
    public void setCity(String city);

    /**
     * Gets the number of transactions in this sale
     *
     * @return number of transactions in this sale
     */
    public int getTransactionCount();

    /**
     * Gets a list of transactions
     *
     * @return list of transactions
     */
    public List<Transaction> getTransactions();

    /**
     * Sets a list of transactions
     *
     * @param transactions list of transactions
     */
    public void setTransactions(List<Transaction> transactions);

    /**
     * Gets the XML element for this sale
     *
     * @return XML element for this sale
     */
    public Element getSaleElement();

    /**
     * Gets the origin of this sale
     *
     * @return origin of this sale
     */
    public int getSaleType();
}
