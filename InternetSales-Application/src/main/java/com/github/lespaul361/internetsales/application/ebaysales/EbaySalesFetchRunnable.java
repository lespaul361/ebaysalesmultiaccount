/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.ebaysales;

import com.ebay.soap.eBLBaseComponents.OrderType;
import com.github.lespaul361.ebaydatalayer.EbayAccountLive;
import com.github.lespaul361.internetsales.application.Sale;
import com.github.lespaul361.internetsales.application.ebaysales.EbaySale;
import com.github.lespaul361.internetsales.application.internetsalesfetch.AbstractSalesFetchRunnable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Charles Hamilton
 */
public class EbaySalesFetchRunnable extends AbstractSalesFetchRunnable {

    private final EbayAccountLive ebay;
    private List<OrderType> ebayOrderTypes = null;
    private final Calendar startCalendar;
    private final List<Sale> ebaySales = new ArrayList<>(50);

    public EbaySalesFetchRunnable(EbayAccountLive ebay, Calendar startCalendar) {
        this.ebay = ebay;
        this.startCalendar = startCalendar;
    }

    @Override
    public void run() {
        try {
            ebayOrderTypes = ebay.getOrders(startCalendar);
        } catch (Exception e) {
            this.exception = e;
        }

    }

    /**
     * @return the ebayOrderTypes
     */
    public List<OrderType> getEbayOrderTypes() {
        return ebayOrderTypes;
    }

    @Override
    public List<Sale> getSales() {
        if (ebaySales.isEmpty() && getEbayOrderTypes().size() > 0) {
            List<OrderType> paidOrders = getEbayOrderTypes().stream()
                    .filter(order -> order.getPaidTime() != null)
                    .collect(Collectors.toList());

            paidOrders.forEach((order)
                    -> {
                ebaySales.add(new EbaySale(order));
            });
        }
        return ebaySales;
    }

}
