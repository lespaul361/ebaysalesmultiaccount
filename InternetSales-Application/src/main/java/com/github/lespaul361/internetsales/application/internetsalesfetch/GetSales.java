/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application.internetsalesfetch;

import com.github.lespaul361.internetsales.application.Sale;
import java.util.List;

/**
 *
 * @author David Hamilton
 */
public interface GetSales {

    /**
     * Gets a list of the sales from this runnable
     *
     * @return list of the sales
     */
    public abstract List<Sale> getSales();
}
