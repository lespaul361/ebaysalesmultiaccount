/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import com.github.lespaul361.commons.DisplayNotification;
import com.theme.ThemePackage;
import java.util.EventObject;
import javax.swing.ImageIcon;

/**
 *
 * @author David Hamilton
 */
public class ShowNotificationEvent extends EventObject {

    private final String title;
    private final String message;
    private final ImageIcon icon;
    private final DisplayNotification.MessageIconType iconType;
    private final ThemePackage themePackage;

    public ShowNotificationEvent(String title, String message, ImageIcon icon,
            DisplayNotification.MessageIconType iconType,
            ThemePackage themePackage, Object source) {
        super(source);
        this.title = title;
        this.message = message;
        this.icon = icon;
        this.iconType = iconType;
        this.themePackage = themePackage;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the icon
     */
    public ImageIcon getIcon() {
        return icon;
    }

    /**
     * @return the iconType
     */
    public DisplayNotification.MessageIconType getIconType() {
        return iconType;
    }

    /**
     * @return the themePackage
     */
    public ThemePackage getThemePackag() {
        return themePackage;
    }

}
