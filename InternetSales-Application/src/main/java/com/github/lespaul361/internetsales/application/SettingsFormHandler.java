/*
 * Copyright (C) 2019 Charles Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import com.ebay.sdk.ApiContext;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import com.github.lespaul361.ebaydatalayer.EbayAccountLive;
import com.github.lespaul361.ebaydatalayer.EbayLogInHelper;
import com.github.lespaul361.ebaydatalayer.EbayValueEvent;
import com.github.lespaul361.ebaydatalayer.Exceptions.EbayConfigurationException;
import com.github.lespaul361.internetsales.configurationfile.ConfigurationFile;
import com.github.lespaul361.internetsales.configurationfile.EbayAccount;
import com.github.lespaul361.internetsales.configurationfile.version1.EbayAccountV1;
import static com.github.lespaul361.internetsales.application.InternetSales.version;
import com.github.lespaul361.internetsales.gui.forms.FullSettingsForm;
import com.github.lespaul361.internetsales.gui.InternetSalesTrayIcon;
import com.github.lespaul361.internetsales.gui.ebay.events.EbayAccountEvent;
import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import static com.github.lespaul361.internetsales.application.InternetSales.EXIT_CODE_CONFIG_FILE;
import com.github.lespaul361.internetsales.application.configurationfilewizard.CreateConfigWizard;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * Handles the events for the settings form
 */
class SettingsFormHandler extends AbstractLogError {

    private final boolean IS_SANDBOX;
    private final static boolean USE_ACCOUNT_TYPE = false;

    private ConfigurationFile configFile;
    private final InternetSalesTrayIcon trayIcon;
    private final Image logo;
    private FullSettingsForm form = null;
    private String certificateId;
    private String appId;
    private String developerName;
    private String ruName;
    EbayLogInHelper helper = null;
    private Window parentWindow = null;
    private CreateConfigWizard wizard = null;

    /**
     * Constructs a new SettingFormHandler. Uses the production URL
     *
     * @param configFile the configuration file
     * @param trayIcon the tray icon
     * @param logo the logo
     * @param logger the logger
     */
    public SettingsFormHandler(ConfigurationFile configFile,
            InternetSalesTrayIcon trayIcon, Image logo, Logger logger) {
        super(logger);
        this.configFile = configFile;
        this.trayIcon = trayIcon;
        this.logo = logo;
        this.certificateId = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getCertificateId();
        this.appId = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getAppId();
        this.developerName = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getDeveloperName();
        this.ruName = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getRuName();
        this.IS_SANDBOX = false;

    }

    /**
     * Creates a new <code>SettingsFormHandler</code> in which all information
     * about the applications connection to Ebay is blank. Uses the production
     * URL
     *
     * @param configFile the configuration file
     * @param trayIcon the tray icon
     * @param logo the logo
     * @param logger the logger
     */
    public SettingsFormHandler(InternetSalesTrayIcon trayIcon, Image logo, Logger logger) {
        super(logger);
        this.trayIcon = trayIcon;
        this.logo = logo;
        this.certificateId = null;
        this.appId = null;
        this.developerName = null;
        this.ruName = null;
        this.IS_SANDBOX = false;
    }

    /**
     * Constructs a new SettingFormHandler
     *
     * @param configFile the configuration file
     * @param trayIcon the tray icon
     * @param logo the logo
     * @param logger the logger
     * @param isSandBox if this is to use the sandbox side of Ebay
     */
    public SettingsFormHandler(ConfigurationFile configFile,
            InternetSalesTrayIcon trayIcon, Image logo, Logger logger,
            boolean isSandBox) {
        super(logger);
        this.configFile = configFile;
        this.trayIcon = trayIcon;
        this.logo = logo;
        this.certificateId = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getCertificateId();
        this.appId = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getAppId();
        this.developerName = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getDeveloperName();
        this.ruName = configFile.getConfigurationFileInfo().
                getEbayApplicationInfo().getRuName();
        this.IS_SANDBOX = isSandBox;

    }

    /**
     * Creates a new <code>SettingsFormHandler</code> in which all information
     * about the applications connection to Ebay is blank
     *
     * @param configFile the configuration file
     * @param trayIcon the tray icon
     * @param logo the logo
     * @param logger the logger
     * @param isSandBox if this is to use the sandbox side of Ebay
     */
    public SettingsFormHandler(InternetSalesTrayIcon trayIcon, Image logo,
            Logger logger, boolean isSandBox) {
        super(logger);
        this.trayIcon = trayIcon;
        this.logo = logo;
        this.certificateId = null;
        this.appId = null;
        this.developerName = null;
        this.ruName = null;
        this.IS_SANDBOX = isSandBox;
    }

    /**
     * Builds the {@link FullSettingsForm} and shows it
     */
    public void showSettingsForm() {
        initForm();
    }

    /**
     * Shows the configuration file wizard to help make the configuration file
     */
    public void showWizard() {
        wizard = new CreateConfigWizard(logo, getLogger());
        parentWindow = wizard.getBaseFrame();
        wizard.addEbayAccountListener((EbayAccountEvent evt) -> {
            appId = wizard.getApplicationID();
            certificateId = wizard.getCertificateID();
            ruName = wizard.getRUName();
            developerName = wizard.getDeveloperID();
            helper = new EbayLogInHelper(certificateId, appId, developerName,
                    ruName, SiteCodeType.US, logo, getLogger());
            ebayAccountHandler(evt);
        });

        int ret = wizard.showDialog();
        if (ret == CreateConfigWizard.CANCEL) {
            JOptionPane.showMessageDialog(null, "<html>Configuration file "
                    + "not configured<br>Now Exiting");
            System.exit(EXIT_CODE_CONFIG_FILE);
        }
    }

    private void initForm() {
        Runnable r = () -> {
            initFormCreation();
            initFormActionListeners();
            initFormEbayAccountEditListeners();
            form.setVisible(true);
            parentWindow = form.getBaseDialog();
        };

        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(() -> {
                r.run();
            });
        }
    }

    private void initFormCreation() {
        form = new FullSettingsForm(version,
                configFile.getConfigurationFileInfo(), logo,
                trayIcon.getTrayIcon(), getLogger());

        helper = new EbayLogInHelper(form.getBaseDialog(), certificateId, appId,
                developerName, ruName, SiteCodeType.US, logo, trayIcon, getLogger());

        form.getBaseDialog().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                helper.cancel();
            }

            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e); //To change body of generated methods, choose Tools | Templates.
                helper.cancel();
            }

        });
    }

    private void initFormActionListeners() {

    }

    private void initFormEbayAccountEditListeners() {
        form.addEbayAccountListener((EbayAccountEvent evt) -> {
            ebayAccountHandler(evt);
        });
    }

    private void ebayAccountHandler(EbayAccountEvent evt) {
        switch (evt.getAccountRequest()) {
            case EbayAccountEvent.CANCEL:
                try {
                    helper.cancel();
                    helper = null;
                    System.gc();
                    break;
                } catch (Exception e) {
                    break;
                }
            case EbayAccountEvent.REQUEST_ADD_ACCOUNT:
                buildEbayAccountFromLogIn(evt);
                break;
        }
    }

    private void buildEbayAccountFromLogIn(EbayAccountEvent e) {
        String token = null;
        helper.addEbayValueListener((EbayValueEvent e1) -> {
            switch (e1.getReturnType()) {
                case EbayValueEvent.RETURN_TYPE_SESSION_ID: {
                    if (form.isVisible()) {
                        try {
                            helper.getSecurityTokenFromLogIn();
                        } catch (Exception ex) {
                            logError(Level.SEVERE, ex.getMessage(), ex);
                        }
                    }
                    break;
                }
                case EbayValueEvent.RETURN_TYPE_TOKEN:
                    if (helper.getToken() != null
                            && !helper.getToken().trim().equalsIgnoreCase("")
                            && form.isVisible()) {
                        try {
                            helper.getAccountNameFromToken();
                        } catch (Exception ex) {
                            logError(Level.SEVERE, ex.getMessage(), ex);
                        }
                    }
                    break;
                case EbayValueEvent.RETURN_TYPE_ACCOUNT_NAME:
                    EbayAccount account = new EbayAccountV1(getLogger());
                    account.setAccountName(helper.getAccountName());
                    account.setSecurityToken(helper.getToken());
                    form.addEbayUser(account);
                    helper.setProgressFormVisible(false);
                    break;
            }
        });

        try {
            ApiContext apiContext = helper.getApiContext();
            helper.getEbaySessionID(apiContext);
        } catch (EbayConfigurationException exConfig) {
            showEbayConfigError(exConfig);
        } catch (Exception ex) {
            logError(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private void buildEbayAccountFromKey(EbayAccountEvent e) {
        /*
        String token = e.getToken();
        ApiContext apiContext = (ApiContext) e.getApiContext();
        String name = getEbayNameFromKey(apiContext, token);
        EbayAccount account = new EbayAccountV1(getLogger());
        account.setAccountName(name);
        account.setSecurityToken(token);

         */
    }

    private String getEbayNameFromKey(ApiContext apiContext, String key) {
        try {
            return EbayAccountLive.getUserID(apiContext, key);
        } catch (Exception e) {
            logError(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    private void showEbayConfigError(EbayConfigurationException e) {
        String[] options = new String[]{"  OK  "};
        JOptionPane.showOptionDialog(parentWindow, "<html>"
                + "<div style='text-align: center;'>"
                + e.getMessage()
                + "</div>"
                + "</html>",
                "Error In Ebay Configuration",
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE,
                new ImageIcon(this.logo),
                options,
                options[0]);

    }
}
