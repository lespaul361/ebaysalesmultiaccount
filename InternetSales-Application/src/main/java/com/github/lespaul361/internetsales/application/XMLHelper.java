/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

/**
 *
 * @author David Hamilton
 */
class XMLHelper {

    public static String getXMLText(String value) {
        if (value == null || value.isEmpty()) {
            return "";
        }
        return removeIllegalCharacters(value).trim();
    }

    public static String removeIllegalCharacters(String input) {
        // Ampersand—&—&amp;
        // greater-than—>—&gt;
        // less-than—<—&lt;
        // apostrophe—'—&apos;
        // quote—"—&quot;
        input = removeIllegalCharacter(input, "&", "&amp;");
        input = removeIllegalCharacter(input, ">", "&gt;");
        input = removeIllegalCharacter(input, "<", "&lt;");
        input = removeIllegalCharacter(input, "'", "&apos;");
        input = removeIllegalCharacter(input, "\"", "&quot;");
        return input;
    }

    private static String removeIllegalCharacter(String input, String character, String replacement) {
        try {
            if (input.contains(character)) {
                int start = 0;
                if (!character.equalsIgnoreCase("&")) {
                    input = input.replace(character, replacement);
                } else {
                    String[] parts = input.split(character);
                    int i = 1;
                    String ret = "";
                    ret = parts[0];
                    while (i < parts.length) {
                        try {
                            if ((i < parts.length)) {
                                if (!(parts[i].substring(0, 4).toLowerCase().equals("amp;"))) {
                                    ret = ret.concat(parts[i] + replacement);
                                } else {
                                    ret.concat(parts[i]);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace(System.out);
                        }
                        i++;
                    }
                    return ret;
                }
            }
        } catch (NullPointerException ne) {
            return "";
        } catch (Exception e) {
        }

        return input;
    }

}
