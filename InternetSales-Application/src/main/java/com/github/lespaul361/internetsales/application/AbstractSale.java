/*
 * Copyright (C) 2019 David Hamilton
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.lespaul361.internetsales.application;

import static com.github.lespaul361.internetsales.application.XMLHelper.getXMLText;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.Element;

/**
 *
 * @author David Hamilton
 */
public abstract class AbstractSale implements Sale {

    private String firstName;
    private String lastName;
    private LocalDate saleDate;
    private String orderId;
    private String address1;
    private String address2;
    private States.State state;
    private String country;
    private String city;
    private String zipCode;
    private List<Transaction> transactions;
    private String sellerId;

    @Override
    public String getSellerId() {
        return this.sellerId;
    }

    @Override
    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public LocalDate getSaleDate() {
        return this.saleDate;
    }

    @Override
    public void setSaleDate(LocalDate saleDate) {
        this.saleDate = saleDate;
    }

    @Override
    public String getOrderId() {
        return this.orderId;
    }

    @Override
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getAddress1() {
        return this.address1;
    }

    @Override
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Override
    public String getAddress2() {
        return this.address2;
    }

    @Override
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Override
    public States.State getState() {
        return this.state;
    }

    @Override
    public void setState(States.State state) {
        this.state = state;
    }

    @Override
    public String getCountry() {
        return this.country;
    }

    @Override
    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String getCity() {
        return this.city;
    }

    @Override
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String getZipCode() {
        return this.zipCode;
    }

    @Override
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public List<Transaction> getTransactions() {
        if (this.transactions == null) {
            this.transactions = new ArrayList<Transaction>();
        }

        return this.transactions;
    }

    @Override
    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public int getTransactionCount() {
        return this.getTransactions().size();
    }

    @Override
    public Element getSaleElement() {
        Element ele = new Element("Order");

        //Header
        ele.setAttribute("ID", getXMLText(getOrderId()));
        ele.setAttribute("DateTime", getSaleDate().format(
                DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        //future
        //ele.setAttribute("Platform", String.valueOf(SALE_TYPE));

        //Seller ID and Buyer Info
        Element subElement = new Element("User");
        subElement.setText(getXMLText(getSellerId()));
        ele.addContent(subElement);

        //Buyer address
        subElement = new Element("BuyerFirstName");
        subElement.setText(getXMLText(getFirstName()));
        ele.addContent(subElement);
        subElement = new Element("BuyerLastName");
        subElement.setText(getXMLText(getLastName()));
        ele.addContent(subElement);
        subElement = new Element("Address1");
        subElement.setText(getXMLText(getAddress1()));
        ele.addContent(subElement);
        subElement = new Element("Address2");
        subElement.setText(getXMLText(getAddress2()));
        ele.addContent(subElement);
        subElement = new Element("State");
        subElement.setText(getXMLText(getState().getAbbreviation()));
        ele.addContent(subElement);
        subElement = new Element("City");
        subElement.setText(getXMLText(getCity()));
        ele.addContent(subElement);
        subElement = new Element("Zipcode");
        subElement.setText(String.valueOf(getZipCode()));
        ele.addContent(subElement);
        subElement = new Element("Country");
        subElement.setText(getXMLText(getCountry()));
        ele.addContent(subElement);

        //Add Transactions
        int count = getTransactionCount();
        for (int i = 0; i < count; i++) {
            ele.addContent(getTransactions().get(i).getTransactionElement());
        }

        return ele;
    }
}
